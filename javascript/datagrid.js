/**
*Datagrid Javascript Functions
*-------------------------------
* @version 1.1
* @since 2010-04-27 Updates to allow for use with datagrid 4.4, adds coloring and other bug fixes.
* @since 2009-12-02 Creates interaction for datagrid, version 4.3
*/


var _replaceContext = false;
var _mouseOverContext = false;
var _noContext = false;
var _divContext;
var old_value = 0;

var _dgNodeListBgColor = {item1:"background-color:white;color:black;",
item2:"background-color:white;color:black;",
item3:"background-color:white;color:black;",
item4:"background-color:white;color:black;",
item5:"background-color:white;color:black;",
item6:"background-color:white;color:black;",
head1:"background-color:black;color:white;",
head2:"background-color:#333333;color:white;",
head3:"background-color:#555555;color:white;",
head4:"background-color:#777777;color:white;",
head5:"background-color:#999999;color:white;",
head6:"background-color:#BBBBBB;color:white;"}

var _dg_custom_find_label = "Custom Search";//$this->custom_find_label;
var _dg_display_rss       = true; //$this->display_rss;
var _dg_display_print     = true; //$this->display_print;
var _dg_display_csv       = true; //$this->display_csv;

//Sets whether the last column is named "Actions" and is blank, if so, adds the icons to the context menu
var _dg_has_actions       = true;//($last_column['field_name']=='' && ($last_column['field_display']=='Actions'||$last_column['field_display']=='Action') ?"true":"false");
var _dg_query_string      = "";//$query_string;
var _dg_rss_querystring   = "";//$query_string;
var dgrowcolor  = '';
var dgrowhilite = '#cecece';//'$this->datagrid_row_highlight';
var _dgControlKeyDown = false;

var _dgDefaultFilterIndexes = [];

Event.observe(document, 'keydown', function(e){
  e = e || window.event;
  //Find Which key is pressed
  if (e.keyCode) code = e.keyCode;
  else if (e.which) code = e.which;
  if(code == 17) {
    _dgControlKeyDown=true;
  }
});

Event.observe(document, 'keyup', function(e){
  e = e || window.event;
  //Find Which key is pressed
  if (e.keyCode) code = e.keyCode;
  else if (e.which) code = e.which;

  if(code == 17) _dgControlKeyDown=false;
});

function dgFilterChange(obj,hash){
  if(obj.value=='dgcustom'){
    dgShowSelectionMenu(document.getElementById('link_'+hash),hash);
  }
  else{
    dgDestroyLinkedField(hash);
    dgSelectNoneAll(hash,false);
    document.getElementById('div_'+hash).style.display='none';
    document.getElementById('link_'+hash).innerHTML = _dg_custom_find_label;
    if(_dgControlKeyDown){
      obj.form.submit();
    }
  }
}

function saveDefaultFilterIndexes(){
  var selects = $('filterDefaultsSelection').getElementsByTagName('select');
  for(var i=0, l=selects.length; i<l;i++){
    _dgDefaultFilterIndexes[i] = selects[i].selectedIndex;
  }
}

function dgResetFiltersDefaultForm(){
  var selects = $('filterDefaultsSelection').getElementsByTagName('select');
  for(var i=0, l=selects.length; i<l;i++){
    selects[i].selectedIndex = _dgDefaultFilterIndexes[i];
  }
  $('filterDefaultsSelection').style.display = 'none';
}

function dgSilentSubmit(resetFilters){
  s = document.getElementById("dgsearchform");
  if(resetFilters==true){
    r       = document.createElement("input");
    r.type  = "hidden";
    r.name  = "dgreset";
    r.value = "1";
    s.appendChild(r);
  }
  s.submit()
}


/**
*Datagrid Hilight Row - change the cells rows to highlight them
*/
function dghr(row) {
  dgrowcolor = row.style.backgroundColor;
  row.style.backgroundColor= dgrowhilite;
  tds = row.getElementsByTagName('td');
  for(i=0;i<tds.length;i++){ td = tds[i]; td.style.backgroundColor = dgrowhilite; }
}

/**
*Datagrid Unhighlight Row - revert the row's cells to the previous color
*/
function dguhr(row){
  row.style.backgroundColor= dgrowcolor;
  tds = row.getElementsByTagName('td');
  for(i=0;i<tds.length;i++){td = tds[i]; td.style.backgroundColor = dgrowcolor; }
}

function InitContext() {
  try{
    _divContext = document.getElementById('dgContextMenu');
    _divContext.onmouseover = function() { _mouseOverContext = true; };
    _divContext.onmouseout  = function() { _mouseOverContext = false; };
    document.body.onmousedown   = ContextMouseDown;
    document.body.oncontextmenu = ContextShow;
  } catch(e){alert(e.description)}
}

/**
* Call from the onMouseDown event, passing the event if standards compliant
*/
function ContextMouseDown(event) {
  if (_noContext || _mouseOverContext) return;
  if (event == null) event = window.event;
  var target = event.target != null ? event.target : event.srcElement;

  //only show the context menu if the right mouse button is pressed and
  //if the rel='ac' attribute is set on the target or the parent.
  if (event.button == 2 && target.getAttribute('rel')=='ac' || target.parentNode.getAttribute('rel')=='ac'){
    _replaceContext = true;
  }
  else if (!_mouseOverContext){
    _divContext.style.display = 'none';
  }
}

/**
* Show the contextual menu when right clicked.
*/
function ContextShow(event)
{
  if (_noContext || _mouseOverContext) return;
  if (event == null) event = window.event;

  var target = event.target != null ? event.target : event.srcElement;
  if (_replaceContext) {
    /* document.body.scrollTop does not work in IE*/
    var scrollTop = document.body.scrollTop ? document.body.scrollTop : document.documentElement.scrollTop;
    var scrollLeft = document.body.scrollLeft ? document.body.scrollLeft :document.documentElement.scrollLeft;
    _divContext.style.display = 'none';_divContext.style.left = event.clientX + scrollLeft + 'px';_divContext.style.top = event.clientY + scrollTop + 'px'; _divContext.style.display = 'block';
    _replaceContext = false;
    return false;
  }
}

/*Force the image loader to load after the form has been submitted to prevent a stalled image*/
function dgShowLoading(doit){
  if(doit){
    l=document.getElementById('dgloading');
    if(typeof(l)=='object'){l.style.display='';}
  }
  setTimeout('dgShowLoading(true)',500);
}

/*Show Context-menu Content*/
function SCC(type, obj) {
  //initialize the HTML that goes in the context menu
  var dgcontent = '';

  /* A header was clicked, so display the three links.*/
  if(type=='h') {
    dgcontent = '';
    dgcontent += '<li><a href="?'+_dg_query_string+'&dgdir=a&dgsort='+obj.id.replace('dgcol_','')+'&dgrest=1" onclick="dgShowLoading()">Sort This Column Ascending</a></li>';
    dgcontent += '<li><a href="?'+_dg_query_string+'&dgdir=d&dgsort='+obj.id.replace('dgcol_','')+'&dgrest=1" onclick="dgShowLoading()">Sort This Column Descending</a></li>';
    dgcontent += '<li><a href="#" onclick="dgShowLoading();dgSilentSubmit(true)">Remove All Sorts &amp; Filters</a></li>';
  }
  else {
    var txt = '';
    var matchTag = /<(?:.|\s)*?>/g;
    var cell = obj;
    var cidx = cell.cellIndex || 0;

    //Try to get any text that user has highlighted
    if (window.getSelection) {
      txt = window.getSelection();
    }
    else if (document.getSelection) {
      txt = document.getSelection();
    }
    else if (document.selection){
      txt = document.selection.createRange().text;
    }

    /*
    If no content was selected, then use the cell's value as the text.
    Remove the tags and replace url encode quotes.*/
    if(txt==''){
      txt = obj.innerHTML.replace(matchTag,'');
      txt = dgRemoveExcessSpaces(txt);
      txt = dgEncodeQuotes(txt);
      range = 'this value';
    }
    else {
      try{
        txt = txt;
        txt = dgRemoveExcessSpaces(txt);
        txt = dgEncodeQuotes(txt.replace(matchTag,''));
      }catch(e){}
      range = '"'+txt+'"';
    }

    //Find out which cell was clicked, then how many cells from the left it was at.

    /*If the cell index is 0, it may be reported wrong
    so count how many cells are before this.*/
    if (cidx == 0) {
      do{
        if (cell.nodeType == 1) cidx++;
        cell = cell.previousSibling;
      } while (cell);
      --cidx;
    }

    /* Set up the links that will be available for the context menu*/
    dgcontent = '';
    dgcontent += '<li><a href="#" onclick=\'dgShowLoading();document.getElementById("dgsearch").value = "'+txt+'";dgSilentSubmit(false)\'>Search for '+range+'</a></li>';

    try{
      if(document.getElementById('dgsearch').value!=''){
        dgcontent += '<li><a href="#" onclick=\'dgShowLoading();s=document.getElementById("dgsearch");s.value += "::AND::";s.value+="'+dgEncodeQuotes(txt)+'";dgSilentSubmit(false)\'>Search for '+range+' <b><i> and </i></b>'+document.getElementById('dgsearch').value.replace(/::AND::/gi,"<b><i> and </i></b>")+'</a></li>';
      }
    }catch(e){}

    dgcontent += '<li><a href="#" onclick="dgShowLoading();">Remove All Sorts &amp; Filters</a></li>';
    if(_dg_display_csv){
      dgcontent += '<li><a href="#" onclick="window.location=\'?'+_dg_query_string+'&dgcsv=1\'">Export To CSV</a></li>';
    }
    if (_dg_display_print){
      dgcontent += '<li><a href=\"?'+_dg_query_string+'&dgprint=1\" target=\'_blank\'>Printer-Friendly</a></li>';
    }
    if(_dg_display_rss){
      dgcontent += '<li><a href="?'+_dg_query_string+_dg_rss_querystring+'"&dgrss=1" target="_blank">Subscribe to RSS</a></li>';
    }

    dgcontent += '<li class="topSep"></li>';
    dgcontent += '<li><a href="#" onclick=\'dgShowLoading();window.location="?'+_dg_query_string+'&dgsort='+cidx+'&dgdir=a&dgrest=1"\'>Sort This Column Ascending</a></li>';
    dgcontent += '<li><a href="#" onclick=\'dgShowLoading();window.location="?'+_dg_query_string+'&dgsort='+cidx+'&dgdir=d&dgrest=1"\'>Sort This Column Descending</a></li>';

    if(_dg_has_actions) {
      dgcontent += '<li class="topSep" style="color:gray;font-family:verdana;font-size:10px;"></li>';

      var tr     = obj.parentNode;
      var tds    = tr.getElementsByTagName('td');
      var td     = tds[tds.length-1];
      var links  = td.getElementsByTagName('a');

      /* Loop through the icons in the last column
      Take the icon and text and add it as a display in the context menu*/
      for(i=0;i<links.length;i++) {
        var link = links[i];
        if (link.title !='') {
          var img        = link.getElementsByTagName('img')[0];
          dgcontent     += '<li><a href="'+dgEncodeQuotes(link.href)+'"><img src="'+img.src+'" border="0" />'+link.title+'</a></li>';
        }
        else{
          try{
            var img    = link.getElementsByTagName('img')[0];
            dgcontent += '<li><a href="'+dgEncodeQuotes(link.href)+'"><img src="'+img.src+'" border="0" /> (no description available)</a></li>';
          }catch(e){}
        }
      }
    }
  }

  /* The content has been set to variables. Now replace the content with what was generated */
  document.getElementById('cmenuitems').innerHTML = dgcontent;
}
/**
* Find the position of the datagrid filter
* to show the selection box below it.
*/
function dgfindPos(obj) {
  var curleft = curtop = 0;
  if (obj.offsetParent) {
    do {
      curleft += obj.offsetLeft;
      curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);
    return [curleft,curtop];
  }
}
/**
* Close any of the custom selection dialog boxes.
*/
function dgCloseDialogs(){
  divs = document.getElementById('dgSelectionDiv').getElementsByTagName('div');
  for (var i=0;i<divs.length;i++) {
    var div = divs[i];
    if (div.className == 'dgcustomselect' && div.style.display == ''){
      dgCloseSelectionMenu(div.id.substring(4));
    }
    else if(div.id =='filterDefaultsSelection'){
      div.style.display='none';
    }
  }
}


/**
* Show the custom selection menu
*/
function dgShowSelectionMenu(link,hash){
  var d = document.getElementById('div_'+hash).style;
  if(d.display==''){
    dgCloseSelectionMenu(hash);
  }
  else {
    dgCloseDialogs();
    if(dgGetCheckboxCount(hash)[0]==0){
      dgCheckboxPopulate(old_value,hash);
    }
    var s     = link.parentNode.getElementsByTagName('select')[0];
    var p     = dgfindPos(link);
    old_value = s.value;
    s.value   = 'dgcustom';
    d.display = '';
    var div = $('div_'+hash);
    div.style.display = "";
    //div.getElementsByTagName('table')[0].style.width = '100%';//(div.offsetWidth-10)+'px';

    d.left    = p[0]+'px';
    d.top     = p[1]+'px';
  }
}

/**
* Close a custom selection dialog and update its values to the filter.
*/
function dgCloseSelectionMenu(hash){
  var stat = dgGetCheckboxCount(hash);
  var select_el = document.getElementById('link_'+hash).parentNode.getElementsByTagName('select')[0];
  dgDestroyLinkedField(hash);

  /*either all the boxes or none of the boxes were selected*/
  if(
  ((stat[0]==0 || stat[0]==stat[1]) && stat[1]>1) ||
  (stat[1]==0 && dgGetCheckboxValues(hash,true)==',,,,,')
  ){
    select_el.value = 'dgall';
    dgSelectNoneAll(hash,false);
    document.getElementById('link_'+hash).innerHTML = _dg_custom_find_label;
  } else if(stat[0]==1){ /*if there was only one value, then its not a custom search.*/
    select_el.value = dgGetCheckboxValues(hash);
    dgSelectNoneAll(hash,false);
    document.getElementById('link_'+hash).innerHTML = _dg_custom_find_label;
  } else {
    document.getElementById('link_'+hash).innerHTML = _dg_custom_find_label + ' (applied)';
    var textbox   = document.createElement('input');
    textbox.name  = select_el.name;
    textbox.id    = 'temp_'+hash;
    textbox.type  = 'hidden';
    textbox.value = dgGetCheckboxValues(hash,(stat[0]==0 && stat[1]==0 ? true : false));
    select_el.parentNode.appendChild(textbox);
  }
  document.getElementById('div_'+hash).style.display='none';
}
/**
* Remove the hidden form element that was created via script.
*/
function dgDestroyLinkedField(hash){
  if(document.getElementById('temp_'+hash)){
    var input = document.getElementById('temp_'+hash);
    input.parentNode.removeChild(input);
  }
}

/**
* Show the custom selection menu
*/
function dgShowFiltersDefaultForm(){
  var d = document.getElementById('filterDefaultsSelection').style;
  if(d.display==''){
    d.display = 'none';
    //dgCloseFiltersDefaultForm(hash);
  }
  else {
    dgCloseDialogs();
    //var s     = link.parentNode.getElementsByTagName('select')[0];
    var p     = dgfindPos($('dgsearchform').getElementsByTagName('table')[0]);
    d.display = '';
    $('filterDefaultsSelection').style.display = "";
    d.left    = p[0]+'px';
    d.top     = (p[1]+35)+'px';
  }
}

function dgSaveFiltersDefault(){
  var filters = $('filterDefaultsSelection').getElementsByTagName('select');

  var str = [];
  for(var i = 0, l= filters.length;i<l;i++){
    str[i] = filters[i].name + '=' + encodeURIComponent(filters[i].value).replace(/\'/g,"%27").replace(/\"/g,"%22");
  }
  location = '?'+_dg_query_string+"&"+str.join("&");
  window.location = location;
}

/**
* Precheck one or more values in the custom selection dialog
*/
function dgCheckboxPopulate(old_value,hash){
  boxes = document.getElementById('div_'+hash).getElementsByTagName('input');
  for(var i=0;i<boxes.length;i++){
    if(boxes[i].value == old_value || old_value =='dgall'){
      boxes[i].checked = true;
    }
  }
}
/**
* Select all or none of the checkboxes in the custom selection dialog
*/
function dgSelectNoneAll(hash,all){
  boxes = document.getElementById('div_'+hash).getElementsByTagName('input');
  for(i=0;i<boxes.length;i++){
    boxes[i].checked = all;
  }
}
/**
* Get the number of checkboxes that have been checked and the total number of checkboxes.
*/
function dgGetCheckboxCount(hash){
  var count = 0;
  var box_count = 0;
  try {
    var boxes = document.getElementById('div_'+hash).getElementsByTagName('div')[0].getElementsByTagName('input');
    for(i=0;i<boxes.length;i++){
      if(boxes[i].type=='checkbox'){
        box_count ++;
      }
      if(boxes[i].checked){
        count++;
      }
    }
  }catch(e){}
  return [count,box_count];
}
/**
* Return a comma-separated list of checked values
*/
function dgGetCheckboxValues(hash,all_boxes){
  var boxes = document.getElementById('div_'+hash).getElementsByTagName('input');
  var str = '';
  for(var i=0;i<boxes.length;i++){
    if(boxes[i].checked || all_boxes==true){
      str = str+boxes[i].value+',';
    }
  }
  str = str.substring(0,str.length-1);
  return str;

}

function dgBuildCheckBoxDiv(obj){
  var l = obj.elements.length;
  var hash = obj.hash;
  var key, value, divText = "";
  var isChecked = false;
  var div, table, tbody, tr, td1, td2, chkbox;

  div   = $('div_'+hash);
  div.style.display ='none';

  wrapper = document.createElement('div');
  wrapper.style.maxHeight = '300px';
  wrapper.style.minHeight = '40px';
  wrapper.style.overflow  = 'auto';
  table = document.createElement('table');
  tr   = document.createElement('tr');
  tbody = document.createElement('tbody');
  table.style.backgroundColor = '#eee';
  table.appendChild(tbody);
  wrapper.appendChild(table);
  div.appendChild(wrapper);
  for(var i=0;i<l;i++){
    td1       = document.createElement('td');
    td2       = document.createElement('td');
    chkbox    = document.createElement('input');
    isChecked = obj.elements[i].checked;// == true ? " checked='checked'": "";
    key       = obj.elements[i].id;
    value     = obj.elements[i].value;

    if(obj.elements[i].level){
      var cls = obj.elements[i].level;
      if(_dgNodeListBgColor[cls]){
        var css = _dgNodeListBgColor[cls];
        td1.setAttribute('style',css);
        td2.setAttribute('style',css);
      }
    }

    chkbox.type    = "checkbox";
    chkbox.value   = key;
    td1.appendChild(chkbox);//td1.innerHTML += (isChecked?'yes':'no');
    td2.innerHTML = value;
    chkbox.checked = isChecked;

    tr.appendChild(td1);
    tr.appendChild(td2);

    //    divText += "<td><input type='checkbox' value='"+key+"'"+isChecked+" /></td><td>"+value+"</td>";
    // save the row if this is the 3rd element and there are more elements to follow.
    if((obj.isNodeList || (i+1)%3==0) && (i+1 !== l)){
      tbody.appendChild(tr);
      tr = document.createElement('tr');
      //divText += "</tr><tr valign='top'>";
    }
  }

  tbody.appendChild(tr);


  /*tr = document.createElement('tr');
  td = document.createElement('td');
  td.setAttribute('colSpan',6);
  td.setAttribute('width','100%');*/
  td = document.createElement('div');
  td.style.padding='3px';
  td.style.whiteSpace = 'nowrap';
  td.innerHTML +=   "<a href='javascript:dgCloseSelectionMenu(\""+hash+"\");'>Save</a> |" +
  "<a href='javascript:dgSelectNoneAll(\""+hash+"\",true);'>Select All</a> |" +
  "<a href='javascript:dgSelectNoneAll(\""+hash+"\",false);'>Select None</a> | " +
  "<a href='javascript:dgCancelSelection(\""+hash+"\");'>Cancel</a>" ;
  div.appendChild(td);
  //tbody.appendChild(tr);


  /*  divText += "</tr><tr><td colspan='100%'>"+
  "<a href='javascript:dgCloseSelectionMenu(\""+hash+"\");'>close</a> |" +
  "<a href='javascript:dgSelectNoneAll(\""+hash+"\",true);'>select all</a> |" +
  "<a href='javascript:dgSelectNoneAll(\""+hash+"\",false);'>select none</a>" +
  "</td></tr></table>";

  $("div_"+hash).innerHTML = divText;
  */
}

function dgCancelSelection(hash){
  var link = $("link_"+hash);
  link.parentNode.getElementsByTagName('select')[0].value = old_value;
  $('div_'+hash).style.display = 'none';
}

function dgEncodeQuotes(string){
  return string.replace(/\'/g,"&#039;").replace(/\"/g,"&quot;")
}

function dgRemoveExcessSpaces(string){
  return string.replace(/\s+/g,' ').replace(/^\s*|\s*$/,'');
}
