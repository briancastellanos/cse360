/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.skin      = "v2";
    config.pasteFromWordKeepsStructure = true;
    config.pasteFromWordRemoveStyle = true;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.format_p  = {element : '', attributes : {}};
    config.toolbar   = [
    ['Maximize','Source','-'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','SpellChecker','Scayt'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Image','Flash','Table','HorizontalRule','SpecialChar'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['Bold','Italic','Underline','-','Subscript','Superscript'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['TextColor','BGColor'],
    ['-'],
    ['Styles','Format','Font','FontSize']
    ] ;
};
