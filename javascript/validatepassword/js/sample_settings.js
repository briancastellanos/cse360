////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Password Validation - Copyright (c) 2010 by Inexo
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// The following sample settings are for "high security" needs; disable/change as needed for your application

var validatePasswordTitle = "Password Validation";
var validatePasswordNumber = true;
var validatePasswordLetter = true;
var validatePasswordUCLetter = true;
var validatePasswordSpecial = true;

var validatePasswordLength = true;
var validatePasswordMinLength = 8;

var validatePasswordConfirm = true;

var validatePasswordIconYes = "<font style='color: green;'>Yes</font>";
var validatePasswordIconNo = "<font style='color: red;'>No</font>";