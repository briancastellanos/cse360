////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Password Validation - Copyright (c) 2010 by Inexo
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validatePassword(pid, cid) {
  if (typeof $(pid) == 'object') {
    var iHTML = "<table>\
                   <tr><th id='validatePasswordHeader' colspan=2>" + validatePasswordTitle + "</th></tr>";

    var passwordValue = $(pid).value;

    // number check
    if (validatePasswordNumber) {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{number}</td><td id='validatePasswordItem'>number</td></tr>";

      var re = /\d/g;

      if (re.test(passwordValue)) {
        iHTML = iHTML.replace("{number}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{number}", validatePasswordIconNo);
      }
    }

    // letter check
    if (validatePasswordLetter) {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{letter}</td><td id='validatePasswordItem'>letter</td></tr>";

      var re = /[a-zA-Z]/g;

      if (re.test(passwordValue)) {
        iHTML = iHTML.replace("{letter}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{letter}", validatePasswordIconNo);
      }
    }

    // upper-case letter check
    if (validatePasswordUCLetter) {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{ucletter}</td><td id='validatePasswordItem'>upper-case letter</td></tr>";

      var re = /[A-Z]/g;

      if (re.test(passwordValue)) {
        iHTML = iHTML.replace("{ucletter}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{ucletter}", validatePasswordIconNo);
      }
    }

    // non-alphanumeric (special) character check
    if (validatePasswordSpecial) {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{special}</td><td id='validatePasswordItem'>special characters</td></tr>";

      var re = /[^a-zA-Z0-9_]/g;

      if (re.test(passwordValue)) {
        iHTML = iHTML.replace("{special}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{special}", validatePasswordIconNo);
      }
    }

    // minimum password length check
    if (validatePasswordLength && validatePasswordMinLength) {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{length}</td><td id='validatePasswordItem'>minimum length (8)</td></tr>";

      if (passwordValue.length >= validatePasswordMinLength) {
        iHTML = iHTML.replace("{length}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{length}", validatePasswordIconNo);
      }
    }

    // confirmation check (if defined)
    if (validatePasswordConfirm && typeof $(cid) == 'object') {
      iHTML = iHTML + "<tr><td id='validatePasswordIcon'>{confirm}</td><td id='validatePasswordItem'>confirmed</td></tr>";

      var confirmValue = $(cid).value;

      if (confirmValue == passwordValue) {
        iHTML = iHTML.replace("{confirm}", validatePasswordIconYes);
      }
      else {
        iHTML = iHTML.replace("{confirm}", validatePasswordIconNo);
      }
    }

    iHTML = iHTML + "</table>";
  }
  else {
    var iHTML = "<font style='color: red'>validatePassword(): Invalid object ID</font>";
  }

  $("validatePassword").innerHTML = iHTML;
}