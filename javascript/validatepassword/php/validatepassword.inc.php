<?php
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Password Validation - Copyright (c) 2010 by Inexo
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  class validatePassword {
    // password validation checks default to "high security"
    private $checkOptions = array("checkForNumber"   => true,
                                  "checkForLetter"   => true,
                                  "checkForUCLetter" => true,
                                  "checkForSpecial"  => true,
                                  "checkForLength"   => true,
                                  "minLength"        => 8);

    private $errors = array();

    public function __construct($options = array()) {
      if (count($options)) {
        $this->checkOptions = $options;
      }
    }

    /**
     * Validate password against defined password validation checks
     *
     * @param string $password - password value to check, returns TRUE if defined validation checks are passed
     * @return boolean
     */
    public function validate($password = null) {
      $ret = true;

      if ($password) {
        // number check
        if ($this->checkOptions["checkForNumber"] == true) {
          if (!preg_match("/\d/", $password)) {
            $ret = false;
            $this->errors[] = "The password must contain at least one number.";
          }
        }

        // letter check
        if ($this->checkOptions["checkForLetter"] == true) {
          if (!preg_match("/[a-zA-Z]/", $password)) {
            $ret = false;
            $this->errors[] = "The password must contain at least one letter.";
          }
        }

        // upper-case letter check
        if ($this->checkOptions["checkForUCLetter"] == true) {
          if (!preg_match("/[A-Z]/", $password)) {
            $ret = false;
            $this->errors[] = "The password must contain at least one upper-case letter.";
          }
        }

        // non-alphanumeric (special) character check
        if ($this->checkOptions["checkForSpecial"] == true) {
          if (!preg_match("/[^a-zA-Z0-9_]/", $password)) {
            $ret = false;
            $this->errors[] = "The password must contain at least one special character.";
          }
        }

        // minimum password length check
        if ($this->checkOptions["checkForLength"] == true && is_numeric($this->checkOptions["minLength"])) {
          if (!(strlen($password) >= $this->checkOptions["minLength"])) {
            $ret = false;
            $this->errors[] = "The password must contain a minimum of {$this->checkOptions["minLength"]} characters.";
          }
        }
      }

      return ($ret);
    }

    /**
     * Returns array containing errors encountered (after failure of validate() method check)
     *
     * @return array
     */
    public function get_errors() {
      return ($this->errors);
    }
  }
?>