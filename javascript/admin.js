/**
* Reads the value from a cookie
*/
function cookieVal(cookieName) {
  thisCookie=document.cookie.split("; ");
  for(i=0;i<thisCookie.length;i++){
    if (cookieName==thisCookie[i].split("=")[0]){
      return thisCookie[i].split("=")[1];
    }
  } return null;
}

var cV      = cookieVal("visible");
var visible = (cV == true || cV=='true' || isNull(cV)) ? true : false;

function toggleNavigation(open) {
  try{
    var collapse='/img/collapse.gif';
    var expand = '/img/expand.gif';

    var navigation = document.getElementById('navigation');
    var welcomemsg = document.getElementById('welcomemsg');
    var tabspace   = document.getElementById('tabspace');
    var img        = document.getElementById('toggle');

    //If the navigation pane is visible, collapse it and update cookie to make it remember to be closed.
    if(open==false || (visible==true && (open==null || open=='undefined'))){
      img.src                   = expand;
      visible                   = false;
      tabspace.style.width      = '0px';
      navigation.style.width    = '0px';
      welcomemsg.style.width    = '0px';
      tabspace.style.display    = 'none';
      navigation.style.display  = 'none';
      welcomemsg.style.display  = 'none';
    }
    // If the pane is not visible, then display it.
    else {
      img.src                  = collapse;
      visible                  = true;
      tabspace.style.display   = '';
      navigation.style.display = '';
      welcomemsg.style.display = '';
      tabspace.style.width     = '200px';
      navigation.style.width   = '200px';
      welcomemsg.style.width   = '200px';
    }
    setCookie();
  } catch (e){ }
}

/**
* Saves the visibility property to the cookie.
*/
function setCookie()
{
  expireDate = new Date;
  expireDate.setMonth(expireDate.getMonth()+6);
  document.cookie = "visible="+visible+";expires="+expireDate.toGMTString();
}

function isNull(val){
  return(val==null);
}