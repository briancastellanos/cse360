<?php
/**
 * Responds to a request for a URL. If the URL is in permalink form, checks for content. If fails, or there is no permalink,
 * checks content manager for an active page. If fails, checks for a front end page. 
 * If fails, returns a 404 error.
 **/
include_once ("./resources/init.php");

if ($_REQUEST['x_permalink'] == 'yes') {
  preg_match("/\-[0-9]+$/", $_REQUEST['url'], $matches);
  $page_id=$filter->filter_var(str_replace("-", "", $matches[0]), FILTER_VALIDATE_INT);
  $db->Query("SELECT * FROM content_page WHERE page_id='$page_id' AND page_status='active' AND (publish_date<= CURDATE() OR publish_date IS NULL) ORDER BY issue_date desc, last_update DESC");
  if ($db->affected < 1) {
    $try_uri=true;
  } else {
    $pg=$db->fetch_assoc();
    include_once "./content.php";
    exit();
  }
}

if ($_REQUEST['x_permalink'] == 'no' || $try_uri == true) {
  $db->Query("SELECT * FROM content_page WHERE page_url='" . addslashes(gpc_clean_array($_REQUEST['url'])) . "' AND page_status='active' AND (publish_date<= CURDATE() OR publish_date IS NULL) ORDER BY issue_date desc, last_update DESC");
  if ($db->affected > 0) {
    $pg=$db->fetch_assoc();
    include_once "./content.php";
    exit();
  }
}

$info       = pathinfo($_REQUEST['url']);
$path       = explode(DIRECTORY_SEPARATOR, $info['dirname']);
$location   = $path[0] == 'admin' ? array_shift($path) : "front";
$extension  = $info['extension'];
$module     = $info['dirname'] == '.' ? basename($info['basename'], "." . $info['extension']) : array_pop($path);
$file_name  = $info['dirname'] == '.' ? "index.php" : $info['basename'];

if ($module == "" && $info['extension'] == "") {
  $module=$info['basename'];
  $file_name="index.php";
}

$base_path="./$location/modules{$path_str}/$module/";
$file_path=$base_path . $file_name;
$tab=trim(strtolower($_REQUEST['tab']));
$button=trim(strtolower($_REQUEST['button']));
$action=trim(strtolower($_REQUEST['action']));
$CFG['mod']=$module;
$CFG['tab']=$tab;
$CFG['button']=$button;

if (file_exists($file_path) && !is_dir($file_path)) {
  if ($location == "admin" && $file_name == "index.php") {
    $_REQUEST['mod']=$module;
    header("location: /admin/index.php?mod=$module");
  } else {
    include ($file_path);
    $page->display_page();
  }
} else if (file_exists($file_path . ".php") && !is_dir($file_path . ".php")) {
  if ($location == "admin" && $file_name == "index.php") {
    header("location: /admin/index.php?mod=$module");
  } else {
    include ($file_path . ".php");
    $page->display_page();
  }
} else {
  header("HTTP/1.0 404 Not Found");
  include_once ("404.php");
}