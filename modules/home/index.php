<?php
/**
 * @package Framework
 * @since   2008-11-11
 * @version 1.0
 * @author  Brian Castellanos
 */

if($_SESSION["UserType"]==USER_PATIENT){
  header("location: ?mod=patients&action=visit&id={$_SESSION['UserId']}");
  exit;
}
echo "Welcome {$CLIENT_NAME} Administration.<br /><br />You may use any of the modules of the left side navigation.";
