<?php
//include("init.php");
if($_SESSION['UserAccessKey']!=''){
	$access=array(
	"user_id"=>$_SESSION['AdminId'],
	"user_access_status"=>"Logged Out",
	"user_type"=>"admin",
	"user_session_id"=>$_SESSION['UserAccessKey'],
	"user_datetime"=>"DB_TIMESTAMP",
	);
	$access['user_ip']=$_SERVER['REMOTE_ADDR'];
	$access['user_access_reason']="Admin Logout";
	$db->mk_sql($access,"user_access");
}
$_SESSION=array();
session_regenerate_id(true);
session_destroy();
header("location:?mod=login");
?>