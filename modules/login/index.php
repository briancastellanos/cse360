<?php
/**
 * @package login
 * @version 2.0
 * @author briancastellanos
 * @since 2010-08-30
 * @desc Adds administrator user type to the session, updates gui
 */
$title="BitTwiddlers Patient Management";
$page->content("BitTwiddlers Patient Management","page_title");

if($_SESSION['AdminLoggedIn']){
    header("location: ?mod=home");
}
else{
    $data = gpc_clean_array($_REQUEST['data']);
    
    if($_REQUEST['data']==''){
        login_form();
    }
    else{
        if($data['username']=='' || $data['password']=='' ){
            login_form(8);
        }
        else{
            
            $username   = $data['username'];
            if($username!==false){
                $sql="SELECT * FROM person WHERE `username`='".mysql_real_escape_string($username)."'";
                $db->Query($sql);
                
                if($db->affected==0){
                    login_form(2);
                }
                else{
                    $r=$db->fetch_assoc();

                    if($r['password']!=$data['password']){
                        login_form(4);
                    } elseif($r['status']!='active') {
                        login_form(16);
                    }
                    else{
                        $_SESSION['LoggedIn']  = true;
                        $_SESSION['UserId']    = $r['person_id'];
                        $_SESSION['UserName']  = $r['username'];
                        $_SESSION['FirstName'] = $r['first_name'];
                        $_SESSION['LastName']  = $r['last_name'];
                        $_SESSION['UserType']  = $r['type'];

                        if($_SESSION['UserDestination']!='' && stristr($_SESSION['UserDestination'],"mod=forgotpw")==false){
                            header(("location: ?{$_SESSION['UserDestination']}"));
                        }else{
                            header("location:?mod=home");
                        }
                    }
                }
            }else{
                login_form(1);
            }
        }
    }
}


function login_form($error=null){
    global $form;
    $data=gpc_clean_array($_REQUEST['data']);
    $errors=array(
    1=>"You must enter a valid username",
    2=>"The username you entered is invalid",
    4=>"The username/password combination does not match",
    8=>"You must supply both a username and password",
    16=>"You do not have permission to access this module",
    );

    if(is_integer($error)){
        if(array_key_exists($error,$errors)){
            $error=$errors[$error];
        }
        else{
            $error="(Unknown reason)";
        }
        $error = notify($error,NOTIFY_DISPLAY_ERROR,false);
    }
    
    echo "
<form action='index.php' method='post' style='padding:20px;'>
<input type='hidden' name='mod' value='login' />

<fieldset>
<legend>Admin Login</legend>
$error
<table width='400'>
<tr>
   <td>Username</td>
   <td>".$form->textbox("data[username]",$data['username'])."</td>
</tr>
<tr>
   <td>Password</td>
   <td>".$form->password("data[password]")."</td>
</tr>
<tr>
    <td></td>
    <td>".
        $form->link('?mod=forgotpw','Forgot Your Password?',array('style'=>'color:maroon;font-size:11px;text-decoration:none','tabindex'=>'4'))."
	</td>
</tr>
<tr>
	<td colspan='2' align='center'>".
    $form->submit("","Login")."
    </td>
</tr>
</table>
</fieldset>
</form>";
}

?>