<form method="post" action="">
<fieldset>
  <legend>Add/Edit Patient</legend>
  <table>
    <tr>
      <td>First Name:</td>
      <td>[!first_name!]</td>
    </tr>
    <tr>
      <td>Last Name:</td>
      <td>[!last_name!]</td>
    </tr>
    <tr><td>&nbsp;</td><td></td></tr>
    <tr>
      <td>Address 1</td>
      <td>[!address_1!]</td>
    </tr>
    <tr>
      <td>Address 2</td>
      <td>[!address_2!]</td>
    </tr>
    <tr>
      <td>City, State Zip</td>
      <td>[!city_state!]</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
      <td>Email</td>
      <td>[!email!]</td>
    </tr>
    <tr>
      <td>Password</td>
      <td>[!password!]</td>
    </tr>
    <tr>
      <td>Phone</td>
      <td>[!phone!]</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
      <td>Doctor</td>
      <td>[!doctor!]</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
      <td>Social Security Number</td>
      <td>[!ssn!]</td>
    </tr>
    <tr>
      <td>[!buttons!]</td>
    </tr>
  </table>
</fieldset>
</form>
