<?php

add_tab("Add Visit Data", "visit", "",true);

if($_SESSION['UserType']!=USER_DOCTOR){
  save_notify("You don't have permission to add new prescriptions");
}

$id      = filter_var($_REQUEST['id'],FILTER_VALIDATE_INT);
$patient = new Patient($id);
$data    = gpc_clean_array($_REQUEST['data']);

if(filter_var($_REQUEST['submitted'],FILTER_VALIDATE_BOOLEAN)){
  $prescription = array(
  "prescription_date"   => date("Y-m-d"),
  "patient_id"   => $patient->personId,
  "doctor_id"  => $_SESSION['UserId'],
  "prescription_name" =>$data['prescription_name'],
  "prescription_description" => $data['prescription_description'],
  );

  $db->mk_sql(addslashes_array($prescription), "prescription");
  
  header("location: ?mod=$module&action=visit&id=$patient->personId");
  #exit;
}


$template = new ModuleTemplate(TEMPLATE_ALLOW_EDIT,"prescription",dirname(__FILE__).DIR_TKN);
$template->content("Add New Information for: {$patient->lastName}, {$patient->firstName}","intro");
$template->content(date("m/d/Y"),"date");
$template->textbox("prescription",     "data[prescription_name]",    $data['prescription_name']);
$template->textarea("comments", "data[prescription_description]",    $data['prescription_description'], array("style"=>"width:500px;height:100px;"));
$template->submit("buttons", "", "Save");
$template->button("buttons","","Cancel",array("onclick"=>"window.location='?mod=$module&action=visit&id=$patient->personId'"));
$template->hiddenbox("buttons", "id", $patient->personId);
$template->hiddenbox("buttons", "submitted", "true");
$template->display_page();