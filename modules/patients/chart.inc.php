<?php 
$page->content('
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="/javascript/excanvas.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="/javascript/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="/javascript/plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="/javascript/plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="/javascript/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="/javascript/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="/javascript/plugins/jqplot.barRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="/javascript/jquery.jqplot.css" />
    ',"page_javascript");

$sql = "SELECT 
    concat_ws('','[',group_concat(concat('[\"',date_format(date_taken,'%m/%d/%Y'),'\",',blood_pressure,']') SEPARATOR ','),']') as blood_pressure,
    concat_ws('','[',group_concat(concat('[\"',date_format(date_taken,'%m/%d/%Y'),'\",',blood_sugar,']')    SEPARATOR ','),']')  as blood_sugar,
    concat_ws('','[',group_concat(concat('[\"',date_format(date_taken,'%m/%d/%Y'),'\",',heart_rate,']')     SEPARATOR ','),']')  as heart_rate,
    concat_ws('','[',group_concat(concat('[\"',date_format(date_taken,'%m/%d/%Y'),'\",',weight,']')         SEPARATOR ','),']')  as weight
    FROM patient_data WHERE patient_id = '$id'";

$r = $db->query_fetch_assoc($sql,false);

$jsVars = "";
$jsSets = array();
foreach($r as $field=>$value){
  $jsVars .= "var $field = {$value};\n";
  $jsSets[] = $field;
}


ob_start();
?>
<script type="text/javascript">
$(document).ready(function(){
  <?php echo $jsVars?>
 
  var plot2 = $.jqplot('chart2', [<?php echo implode(",",$jsSets)?>], {
    series:[{xaxis:'xaxis', yaxis:'yaxis',label:'Blood Pressure'},
            {xaxis:'xaxis', yaxis:'yaxis',label:'Blood Sugar'},
            {xaxis:'xaxis', yaxis:'yaxis',label:'Heart Rate'},
            {xaxis:'xaxis', yaxis:'yaxis',label:'Weight'}],
    axesDefaults: {
        tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
        tickOptions: {
          angle: 30,
        },
    },
    axes: {
      xaxis: {
        renderer: $.jqplot.CategoryAxisRenderer
      },
      yaxis: {
        autoscale:true
      }
    },
    legend: {
      show: true
     }
  });
});
</script>
<?php
$page->content(ob_get_clean(),"page_javascript");
?>

