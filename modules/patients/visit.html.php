<form>
  <fieldset>
    <legend>Patient Information</legend>
    [!edit_patient_info!]
    <table>
     <tr>
       <td>Patient Name:</td><td>[!patient_name!]</td>
       <td>Doctor Name:</td><td>[!doctor_name!]</td>
     </tr>
     <tr>
       <td>Enable Patient Data Entry</td>
       <td colspan='3'>
         [!checkboxes!]
       </td>
     </tr>
     
     <tr>
      <td colspan='4'>&nbsp;</td>
    </tr>
     
     
     <tr>
       <td>Address 1:</td>
       <td>[!address_1!]</td>
       </tr>
     <tr>
       <td>Address 2:</td>
       <td>[!address_2!]</td>
       </tr>
     <tr>
       <td>City:</td>
       <td>[!city!]</td>
       </tr>
     <tr>
       <td>State:</td>
       <td>[!state!]</td>
       </tr>
     <tr>
       <td>Zip:</td>
       <td>[!zip!]</td>
       </tr>
     <tr>
       <td>Telephone:</td>
       <td>[!phone!]</td>
       </tr>
     <tr>
       <td>Email:</td>
       <td>[!email!]</td>
       </tr>
    </table>
  </fieldset>
  <br />
  
  <fieldset>
    <legend>Patient History</legend>
    [!new_visit_button!]
    [!history!]
  </fieldset>
  
  <div id='chart2' style='width:80%;'></div>
  
  <br />
  <fieldset>
    <legend>Prescriptions</legend>
    [!new_prescription_button!]
    [!prescriptions!]
  </fieldset>  
</form>