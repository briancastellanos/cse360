<form action="?mod=patients&action=data" method="post">
<fieldset>
  <legend>Add New Data</legend>
[!intro!]<br />
  <table>
    <tr><td>Visit Date:</td><td>[!date!]</td></tr>
    <tr><td>Weight:</td>        <td>[!weight!] lbs</td></tr>
    <tr><td>Blood Pressure:</td><td>[!bp!] mm/hg</td></tr>
    <tr><td>Blood Sugar:</td>   <td>[!bs!] g/dl</td></tr>
    <tr><td>Heart Rate:</td>    <td>[!hr!] bpm</td></tr>
    <tr><td>Comments/Observations</td></tr>
    <tr>
      <td colspan=2>
        [!comments!]
      </td>
    </tr>
  </table>
  [!buttons!]
</fieldset>
</form>