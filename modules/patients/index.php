<?php

$action = $_REQUEST['action'];
if($_SESSION['UserType']==USER_PATIENT && ($action=="" || $action=="new")){
  header("location: ?mod=$module&action=visit&id={$_SESSION['UserId']}");
  exit;
}

error_reporting(0);
include_class("utility");
global $page;

$CFG['tab'] = "patient_listing";

$db=DatabaseFactory::getDb();

$page->content("Patient Management", "page_section_title");
$page->content("Patient Management", "page_title");

if ($_REQUEST['action'] == "") {
  $sql="
  SELECT person.*,patient.*,concat(doctor.first_name,' ',doctor.last_name) as doctor_name,
  ifnull(last_visit,'Not Yet Visited') as last_visit
  FROM person 
  LEFT JOIN patient ON person.person_id = patient.person_id
  LEFT JOIN person as doctor ON patient.doctor_id = doctor.person_id
  LEFT JOIN
  ( SELECT date_format(max(visit_date),'%m/%d/%Y') as last_visit, patient_id FROM patient_visit GROUP BY patient_id)
    as  patient_visit ON patient_visit.patient_id = person.person_id     
 
  WHERE person.type='patient' AND person.status='active'";
  
  $db->register_header("first_name", "First Name");
  $db->register_header("last_name", "Last Name");
  $db->register_header("doctor_name", "Doctor");
  $db->register_header("last_visit", "Last Visit Date");
  $db->register_header("","Actions","{!edit!} {!visit!}");
  
  
  $db->register_icon("edit", "?mod=$module&action=edit&id=[!person_id!]",   "Edit Patient", "/img/sm_icon/user_edit.png");
  $db->register_icon("visit", "?mod=$module&action=visit&id=[!person_id!]", "New Vist", "/img/sm_icon/door_in.png");
  
  $db->create_grid($sql);
}
if($_SESSION['UserType']==USER_PATIENT){
  add_button("View My Information", "$module.new", "$module.new&action=new");
}
else{
  add_button("Add New Patient", "$module.new", "$module.new&action=new");
}

if ($action=="new" || $action=="edit") {
  include 'patient_create_edit_form.inc.php';
}

if($action=="visit"){
  include 'patient_visit.inc.php';
}

if($action=="data"){
  include 'data.inc.php';
}

if($action=="prescription"){
  include "prescription.inc.php";
}

if($action=="chart"){
  include "chart.inc.php";
}