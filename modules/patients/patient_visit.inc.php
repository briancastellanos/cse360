<?php

$id = filter_var($_REQUEST['id'],FILTER_VALIDATE_INT);

$patient = new Patient($id);
$address = new Address($id);
$doctor = new Person($patient->doctorId);

$template = new ModuleTemplate(TEMPLATE_ALLOW_EDIT,"visit",dirname(__FILE__).DIR_TKN);
$template->content($patient->firstName." ".$patient->lastName,"patient_name");
$template->content($doctor->firstName." ".$doctor->lastName,"doctor_name");

$template->content($address->addressOne,"address_1");
$template->content($address->addressTwo,"address_2");
$template->content($address->city,"city");
$template->content($address->state,"state");
$template->content($address->zip,"zip");

$template->content($patient->emailAddress,"email");
$template->content($patient->phoneNumber,"phone");
$template->button("edit_patient_info","","Edit Patient Info",array("onclick"=>"window.location='?mod=$module&action=edit&id=$patient->personId'"));

$db->display_csv = false;
$db->display_filters = false;
$db->display_print = false;
$db->datagrid_row_background_1 = "white";
$db->datagrid_row_background_2 = "white";
$db->datagrid_row_highlight = "white";

$db->register_header("date_taken",     "Date");
$db->register_header("blood_pressure", "Blood Pressure");
$db->register_header("blood_sugar",    "Sugar");
$db->register_header("heart_rate",     "Heart Rate");
$db->register_header("weight",         "Weight");

$sql = "SELECT * FROM patient_data WHERE patient_id='$patient->personId' ORDER BY date_taken DESC";

$template->button("new_visit_button", "", "Add New Entry",array("onclick"=>"window.location='?mod=$module&action=data&id={$patient->personId}'"));
#$template->button("new_visit_button", "", "View Graphical Data",array("onclick"=>"window.location='?mod=$module&action=chart&id=$patient->personId'"));
ob_start();
$db->create_grid($sql,null,false,false);
$template->content(ob_get_clean(),"history");
include "chart.inc.php";

if($_SESSION['UserType']==USER_DOCTOR){
  $template->button("new_prescription_button","","Add A New Prescription",array("onclick"=>"window.location='?mod=$module&action=prescription&id=$patient->personId'"));
}

$db->datagrid_header_rows= array();
$db->register_header("prescription_date","Date");
$db->register_header("prescription_name","Prescription");
$db->register_header("prescription_description","Description");

$sql = "SELECT * FROM prescription WHERE patient_id='$patient->personId' ORDER BY prescription_date DESC";
ob_start();
$db->create_grid($sql,null,false,false);
$template->content(ob_get_clean(),"prescriptions");
$template->display_page();