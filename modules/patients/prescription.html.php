<form action="?mod=patients&action=prescription" method="post">
<fieldset>
<legend>Add New Data</legend>
[!intro!]<br />
<table>
<tr><td>Visit Date:</td><td>[!date!]</td></tr>
<tr><td>Prescription:</td>        <td>[!prescription!]</td></tr>
<tr><td>Comments/Observations</td></tr>
<tr>
<td colspan=2>
[!comments!]
</td>
</tr>
</table>
[!buttons!]
</fieldset>
</form>