<?php
if($_SESSION['UserType']==USER_PATIENT){
  add_tab("Edit My Information", "newpatient", "",true);
}else{
add_tab("Add New Patient", "newpatient", "",true);
}

$CFG['tab'] = "newpatient";
$person_id = filter_var($_REQUEST['id'],FILTER_VALIDATE_INT);
$patient=new Patient($person_id);
$address=new Address($person_id);

if (filter_var($_REQUEST['submitted'], FILTER_VALIDATE_BOOLEAN) === true) {
  $personData=gpc_clean_array($_REQUEST['patient']);
  $addressData=gpc_clean_array($_REQUEST['address']);
  
  foreach ( $personData as $field => $value ) {
    $patient->$field=$value;
  }
  
  $patient->type   = "patient";
  $patient->status = "active";
  $patient->userName = $patient->emailAddress;
  
  foreach ( $addressData as $field => $value ) {
    $address->$field=$value;
  }
  
  $errors=array();
  if ($patient->firstName == "") {
    $errors[]="A First Name is required";
  }
  if ($patient->firstName == "") {
    $errors[]="A Last Name is required";
  }
  if (filter_var($patient->emailAddress, FILTER_VALIDATE_EMAIL) === false) {
    $errors[]="A valid email address is required";
  }
  if (filter_var($address->zip, FILTER_VALIDATE_INT) === false) {
    $errors[]="The zip code is invalid";
  }
  if($patient->password == "" ){
    $errors = "A password must be entered";
  }
  
  $db->query("SELECT * FROM person WHERE username='$patient->emailAddress' AND person_id<>'$patient->personId'");
  
  if($db->affected>0){
    $errors[] = "The email address is already in use by another person";
  }
  
  if (count($errors)) {
    notify("Please correct the following errors:<ul><li>" . implode("</li><li>", $errors) . "</li></ul>", NOTIFY_DISPLAY_ERROR);
  } else {
    $patient->save();
    $patient->saveAddress($address);
    header("location:?mod=$module&action=visit&id=$patient->personId");
    exit;
  }
}

$template=new ModuleTemplate(TEMPLATE_ALLOW_EDIT, "edit", dirname(__FILE__) . DIR_TKN);

$template->hiddenbox("buttons", "id", $patient->personId);
$template->textbox("first_name", "patient[firstName]", $patient->firstName);
$template->textbox("last_name", "patient[lastName]", $patient->lastName);
$template->textbox("address_1", "address[addressOne]", $address->addressOne);
$template->textbox("address_2", "address[addressTwo]", $address->addressTwo);
$template->textbox("city_state", "address[city]", $address->city);
$template->select("city_state", "address[state]", $address->state, Utility::getStatesAbbrevList());
$template->textbox("city_state", "address[zip]", $address->zip);
$template->textbox("email", "patient[emailAddress]", $patient->emailAddress);
$template->password("password", "patient[password]",  $patient->password);
$template->textbox("phone", "patient[phoneNumber]", $patient->phoneNumber);

if($_SESSION['UserType']==USER_PATIENT){
 $doctors = Utility::getDoctorsList();
 $template->content($doctors[$patient->doctorId],"doctor");
}
else{ 
  $template->select("doctor", "patient[doctorId]", $patient->doctorId, Utility::getDoctorsList());
}

$template->textbox("ssn", "patient[ssn]", $patient->ssn);

$template->hiddenbox("buttons", "submitted", "true");
$template->submit("buttons", "Update");

if($patient->personId){
  $location = "?mod=$module&action=visit&id=$patient->personId";
}
else {
  $location = "?mod=$module";
}

$template->button("buttons","",  "Cancel", array("onclick"=>"window.location='$location'"));

$template->submit();
$template->display_page();