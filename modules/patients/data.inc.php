<?php

add_tab("Add Visit Data", "visit", "",true);

if($_SESSION['UserType']!=USER_DOCTOR){
  save_notify("You don't have permission to add new prescriptions");
}


$id      = filter_var($_REQUEST['id'],FILTER_VALIDATE_INT);
$patient = new Patient($id);
$data    = gpc_clean_array($_REQUEST['data']);


if(filter_var($_REQUEST['submitted'],FILTER_VALIDATE_BOOLEAN)){
  if($_SESSION['UserType']!=USER_PATIENT){
  $visit = array(
      "visit_date"   => date("Y-m-d"),
      "patient_id"   => $patient->personId,
      "provider_id"  => $_SESSION['UserId'],
      );

  $visitId = $db->mk_sql($visit,"patient_visit");
  }
  else {
   $visitId = "*FUNC*null";
  }
  
  $data["patient_id"] = $patient->personId;
  $data["visit_id"]   = $visitId;
  $data["date_taken"] = date('Y-m-d');
  $data["entered_by_id"] = $_SESSION['UserId'];

  $db->mk_sql(addslashes_array($data), "patient_data");
  
  header("location: ?mod=$module&action=visit&id=$patient->personId");
  exit;
}


$template = new ModuleTemplate(TEMPLATE_ALLOW_EDIT,"data",dirname(__FILE__).DIR_TKN);
$template->content("Add New Information for: {$patient->lastName}, {$patient->firstName}","intro");
$template->content(date("m/d/Y"),"date");
$template->textbox("weight", "data[weight]",        $data['weight']);
$template->textbox("bp",     "data[blood_pressure]",$data['blood_pressure']);
$template->textbox("bs",     "data[blood_sugar]",   $data['blood_sugar']);
$template->textbox("hr",     "data[heart_rate]",    $data['heart_rate']);
$template->textarea("comments", "comments",         $comments, array("style"=>"width:500px;height:100px;"));
$template->submit("buttons", "", "Save");
$template->button("buttons","","Cancel",array("onclick"=>"window.location='?mod=$module&action=visit&id=$patient->personId'"));
$template->hiddenbox("buttons", "id", $patient->personId);
$template->hiddenbox("buttons", "submitted", "true");
$template->display_page();