<?php
include_class("module_template");

$mt = new ModuleTemplate(TEMPLATE_ALLOW_EDIT,"admin_edit",dirname(__FILE__).DIR_TKN);
$t  = explode(".",$action);

addFormSubmitButtons();
add_tab(ucwords($t[1]." ".$t[0]),$action,$action,true);
$admin_id = filter_var($_REQUEST['admin_id'],FILTER_VALIDATE_INT);
if($action=="admin.edit" && $loaded!=true){
    $db->Query("SELECT * FROM person WHERE person_id='$admin_id'");
    if($db->affected>0){
        $admin=$db->fetch_assoc();
    }else{
        $action='admin.new';
    }
}

$mt->content("?mod=$module","form_action");
if($admin['admin_id']!=''){
    $mt->hiddenbox("hiddenbox","admin_id",$admin_id);
    $mt->hiddenbox("hiddenbox","admin[admin_id]",$admin_id);
    $mt->hiddenbox("hiddenbox","action","admin.update");
}
else{
    $mt->hiddenbox("hiddenbox","action","admin.add");
}

if ($admin['admin_confirm'] == "") $admin['admin_confirm'] = $admin['admin_password'];

if (file_exists(RSC_ROOT."$module-config.inc.php")){
  include RSC_ROOT."$module-config.inc.php";
}

$mt->textbox("username", "admin[username]",         $admin['username'],       array("size"=>30));
$mt->password("password","admin[admin_password]",   $admin['admin_password'], array("id"=>"password", "size"=>30));
$mt->password("confirm","admin[admin_confirm]",     $admin['admin_confirm']	, array("id"=>"confirm",  "size"=>30));
$mt->select("user_type","admin[user_type]",         $admin['user_type'],      array("doctor"=>"Doctor", "nurse"=>"Nurse"));
$mt->select("status","admin[admin_status]",         $admin["admin_status"],   array("active"=>"Active","inactive"=>"Inactive","deleted"=>"Deleted"));
$mt->textbox("first_name","admin[first_name]",      $admin['first_name'],     array("size"=>30));
$mt->textbox("last_name","admin[last_name]",        $admin['last_name'],      array("size"=>30));
$mt->textbox("email","admin[email_address]",        $admin['email_address'],  array("size"=>30));
$mt->content($form->submit("mode","Save").$form->button("","Cancel",array("onclick"=>"location.href='?mod=$module'")),"submit");
$mt->display_page();