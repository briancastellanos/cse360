<fieldset>
<legend>User Information</legend>
<div style='float: left; display: inline; width: 350px; height: 275px;'>
<form action='[!form_action!]' method='post' style='padding:20px;'>
[!hiddenbox!]
<table>
    <tr>
        <td>First Name:</td>
        <td>[!first_name!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	    <td>Last Name:</td>
	    <td>[!last_name!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	    <td>Email:</td>
	    <td>[!email!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	   <td>Username:</td>
	   <td>[!username!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	   <td>Password:  		</td>
	   <td>[!password!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	   <td>Confirm:  		</td>
	   <td>[!confirm!]&nbsp;<span style='color:red;'>*</span></td>
    </tr>
	<tr>
	   <td>User Type:		</td>
	   <td>[!user_type!]    </td>
    </tr>
	<tr>
	   <td>Status:			</td>
	   <td>[!status!]       </td>
    </tr>
	<tr>
	   <td></td>
	   <td>[!submit!]</td>
    </tr>
</table>
</form>
</div>
[!password_validation!]
</fieldset>
