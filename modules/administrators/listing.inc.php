<?php
$page->content ( "Doctor Listing", "page_title" );

$CFG ['tab'] = "administrators.listing";
$CFG ['button'] = "administrators.listing";

$menu_buttons = array (
    array (
        "text" => "Doctors Listing",
        "button_href" => "administrators.listing"
    ),
    array (
        "text" => "Create New Doctor",
        "button_href" => "&action=admin.new"
    )
);

$db->register_icon ( "edit", "?mod=$module&action=admin.edit&admin_id=[!person_id!]", "Edit Doctor", "/img/sm_icon/user_edit.png" );
$db->register_icon ( "delete", "javascript:if(confirm(\"Are you sure you want to delete this doctor? This cannot be undone\")){window.location=\"?mod=$module&action=admin.delete&admin_id=[!person_id!]\"}", "Delete Administrator", "/img/sm_icon/user_delete.png" );

$db->register_header ( "first_name", "First Name" );
$db->register_header ( "last_name", "Last Name" );
$db->register_header ( "email_address", "Email Address" );
$db->register_header ( "username", "Username" );
$db->register_header ( "type", "User Type" );
$db->register_header ( "", "Actions", "{!edit!} {!delete!}" );

$db->register_filter ( "Status", "admin_status", array (
    "dgall" => "All",
    "active" => "Active",
    "inactive" => "Inactive"
) );

$sql = "SELECT * FROM `person` WHERE status <> 'deleted'
    AND(type='doctor' OR type='nurse')";

retrieve_notify ();
$db->display_filters = false;
$db->create_grid ( $sql );
