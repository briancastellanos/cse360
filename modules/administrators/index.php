<?php
$page->content("Doctor Management", "page_section_title");
$user_id=$filter->filter_var($_REQUEST['admin_id'], FILTER_VALIDATE_INT);
$admin=gpc_clean_array($_REQUEST['admin'], GPC_TRIM);
$person = new Person($user_id);

// ----------------------------+
// Manage user information. |
// ----------------------------+

if ($action == 'admin.delete') {
  $db->Query("UPDATE person SET status='deleted' WHERE person_id='$user_id'");
  save_notify("The user has successfully been removed");
  header("location:?mod=$module");
  exit();
}

if ($action == 'admin.update' || $action == 'admin.add') {
  $strErr="";
  
  if ($admin['username'] == "") {
    $err=true;
    $strErr.="A username is required<br />";
  }
  
  if (strlen($admin['admin_password']) < 6) {
    $err=true;
    $strErr.="The password must be at least 6 characters long.<br />";
  }
  
  if (strlen($admin['admin_confirm']) < 6) {
    $err=true;
    $strErr.="The confirmation password must be at least 6 characters long.<br />";
  }
  
  if ($admin['admin_password'] != $admin['admin_confirm']) {
    $err=true;
    $strErr.="The password and confirmation password must match.<br />";
  }
  
  if ($admin['admin_password'] == "" || $admin['admin_confirm'] == "") {
    $err=true;
    $strErr.="Both the password and confirmation password must be entered.<br />";
  }
  
  if ($filter->filter_var($admin['email_address'], FILTER_VALIDATE_EMAIL) === false || $admin['email_address'] == '') {
    $err=true;
    $strErr.="The email address in invalid.<br />";
  }
  
  if ($admin['first_name'] == '') {
    $err=true;
    $strErr.="A first name is required.<br />";
  }
  
  if ($admin['last_name'] == '') {
    $err=true;
    $strErr.="A last name is required.<br />";
  }
  
  if ($err == true) {
    notify($strErr, NOTIFY_DISPLAY_ERROR);
    $action=($action == "admin.update" ? "admin.edit" : "admin.new");
    $loaded=true;
  } else {
    $db->Query("SELECT username, person_id FROM person WHERE username='" . addslashes($admin['username']) . "'");
    $r=$db->fetch_assoc();
    
    if ($r['user_id'] != '' && $r['user_id'] != $user_id) {
      notify("This username is already in use", NOTIFY_DISPLAY_ERROR);
      $action=($action == "admin.update" ? "admin.edit" : "admin.new");
      $loaded=true;
    } 
    else {
      $person->type         = $admin['user_type'];
      $person->firstName    = $admin['first_name'];
      $person->lastName     = $admin['last_name'];
      $person->userName     = $admin['username'];
      $person->password     = $admin['password'];
      $person->emailAddress = $admin['email_address'];
      $person->status       = "active";

      $person->save();
      
      if ($db->error == '') {
        save_notify("Successfully updated information");
        header("location:?mod=$module");
        exit();
      } else {
        notify("An error has occured: $db->error", NOTIFY_DISPLAY_ERROR);
        $action=($action == "admin.update" ? "admin.edit" : "admin.new");
        $loaded=true;
      }
    }
  }
}

if ($action == 'admin.new' || $action == 'admin.edit') {
  include "admin_edit.inc.php";
}

if ($action == 'default' || $action == '') {
  include "listing.inc.php";
}