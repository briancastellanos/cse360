<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="description" content="[!page_description!]" />

    <title>[!page_title!]</title>
    <script src='/javascript/jquery.min.js' type="text/javascript"></script>
    [!page_javascript!]
    <script src="/javascript/admin.js" type="text/javascript"></script>
    
    <script type="text/javascript">
	//Create a listener event that runs when the page finishes loading
	//From the template
	$(document).ready(function(){
	  //Collapse or show the navigation area
	  try{
	    navheight=$('#navigation').getHeight();
	    $('#content').setStyle({height:(navheight)+'px'});
	    var e =(visible==false || visible=='false' ?false:true);
	    toggleNavigation(e);
	  }catch(e){}

	  //Select the first visible form element on the page.
	  if(document.forms[0]){
	    try{
	      for(i=0;i<document.forms[0].elements.length;i++){
	        t = document.forms[0].elements[i].type;
	        if (t=='text' || t=='textarea' || t=='select-one') {
	          document.forms[0].elements[i].focus();
	          break;
	        }
	      }
	    }catch(e){}
	  }
	});

	</script>
    <link rel="stylesheet" type="text/css" href="/style/admin.css" />
    [!page_head!]
</head>

<body onload="[!page_onload!]" onunload="[!page_unload!]"  class="soria">
	<table style="width:100%;" cellspacing="0" cellpadding="0">
		<tr>
			<td id='top' colspan="3" style='position:relative;'>
				<div style="width:200px;float:left;">
				    <!-- [!logo!] &nbsp; -->
				</div>
				<div style="float:left;font-size:18px;font-weight:bold;color:#6c6c6c;font-family:arial;">
					[!page_section_title!]
				</div>
				<div style="float:right;display:inline;margin-right:7%;" class="links">
					<span style="margin-right:20px;font-weight:bold;color:#7c7c7c">
					   [!client_name!]
					</span>
					<a href='/'>[!client_name!]</a> | <a href='/'> Home</a> | <a href='?mod=logout'>Logout</a>
				</div>
				<div style="clear:both;"></div>
				<br />
			</td>
		</tr>
		<tr>
			<td colspan="100%">&nbsp;</td>
		</tr>
		<tr valign="top">
			<td id="tabspace" style="width:200px;"></td>
			<td></td>
			<td id="tab_nav" style="padding-bottom:5px;padding-top:15px;">
			 <!--Module Tabs Go Here-->
			 <ul id='tab_ul'>
			     [!page_tabs!]
			     <li></li>
			 </ul>
			</td>
		</tr>
		<tr>
			<td valign="bottom" style="font-weight:normal;color:#0575a4;font-size:11px;width:200px;" id='welcomemsg'>
			 [!welcome_message!]
			</td>
			<td class='spacer'></td>
			<td id='buttons'>
				<!--Module Buttons Go Here-->
				<ul>
					[!page_buttons!]
					<li></li>
				</ul>
			</td>
		</tr>
		<tr valign="top">
			<td id='navigation' width="200">
				[!page_navigation!]
			</td>
			<td class='spacer' valign="middle" onclick='toggleNavigation()' id='toggleTd' onmouseover='this.style.backgroundColor="Gray"' onmouseout='this.style.backgroundColor="#F4F5EB"' style='cursor:pointer;' title="Toggle Navigation Panel">
			<img id='toggle' src='/img/collapse.gif' />
			</td>
			<td id='content' width='100%'>
			[!page_content!]
			&nbsp;
			</td>
		</tr>
	</table>
	</body>
</html>