<?php
/**
 * @version 1.0
 * @since   2010-07-10
 * @author  Brian Castellanos
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name='description' content='[!page_description!]' />
<title>[!page_title!]</title>
[!page_javascript!]
<script src='/javascript/prototype.js' type="text/javascript"></script>
[!page_head!]
<style type="text/css">
body{
	font-family:arial;
	font-size:12px;
	height:100%;
}
</style>
</head>
<body onload="[!page_onload!]" onunload="[!page_unload!]">
    [!page_content!]
</body>
</html>