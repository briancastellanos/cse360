<?php
DEFINE("NOTIFY_DISPLAY_SUCCESS"    ,1);
DEFINE("NOTIFY_DISPLAY_ERROR"      ,2);
DEFINE("NOTIFY_DISPLAY_WARNING"    ,4);
DEFINE("NOTIFY_DISPLAY_INFORMATION",8);

DEFINE("GPC_STRIP_SLASHES"         ,0);
DEFINE("GPC_ADD_SLASHES"           ,1);
DEFINE("GPC_LTRIM"                 ,2);
DEFINE("GPC_RTRIM"                 ,3);
DEFINE("GPC_TRIM"                  ,4);

$__DIRECTORY_FILES__ = array();
set_exception_handler('userExceptionHandler');
set_error_handler('userErrorHandler',E_USER_ERROR&E_USER_NOTICE&E_USER_WARNING);


function userErrorHandler($error_level,$error_message,$error_file,$error_line,$error_context){
  echo $error_message;
  return true;
}
function userExceptionHandler($exception){
  echo "<h2>An Error Has Occured</h2>";
  if(strip_tags($exception->getMessage())==$exception->getMessage){
    notify($exception->getMessage,NOTIFY_DISPLAY_ERROR);
  }
  else{
    echo $exception->getMessage();
  }
  echo "<hr />";
  echo "Occured at ".$exception->getFile().' line '.$exception->getLine()."<br />Backtrace:<br />";
  echo nl2br($exception->getTraceAsString());
}


/**
 * Includes a file from the classes directory
 *
 * @param string $class
 */
function include_class($class){
  require_once(CLS_ROOT."class.$class.php");
}

/**
 * Uses MySQL Query to Encode a string.
 * @param string $string
 * @return string
 */
function encrypt($string){
  global $db;
  $db->Query("SELECT CAST(AES_ENCRYPT('$string','".CRYPTKEY."') AS CHAR) as `text`",false);
  $text=$db->fetch_result("text");
  $db->free_result();
  return  $text;
}

/**
 * Uses MySQL to Decode a string
 * @param string $string
 * @return string
 */
function decrypt($string){
  global $db;
  $db->Query("SELECT CAST(AES_DECRYPT('$string','".CRYPTKEY."') AS CHAR) as `text`",false);
  $text = $db->fetch_result("text");
  $db->free_result();
  return  $text;
}

/**
 * Save a notification message to the session
 *
 * @param string $message
 * @param integer $alert_type
 */
function save_notify($message,$alert_type=NOTIFY_DISPLAY_INFORMATION){
  $_SESSION['notificationMessage'] = $message;
  $_SESSION['notificationType']    = $alert_type;
}

/**
 * Retrieve the notification message from the string and display it.
 *
 */
function retrieve_notify(){
  if($_SESSION['notificationMessage']!=''){
    notify($_SESSION['notificationMessage'],$_SESSION['notificationType']);
    $_SESSION["notificationMessage"] = null;
    $_SESSION["notificationType"]    = null;
  }
}

/**
 * Used to dereference variables
 * @param string $value
 * @return string $value
 */
function getValue($value){
  return $value;
}

/**
 * Display a notification dialog box
 *
 * @param string $message
 * @param integer $alert_type
 */
function notify($message,$alert_type=NOTIFY_DISPLAY_INFORMATION,$display_alert=true){
  $text =  "	<table style='width:95%;background-color:#FBF8AD;margin:20px;padding: 10px 0px;border:1px solid gray;'>
			<tr valign='top'><td width='25'>
			<img src='/img/sm_icon/";
  switch($alert_type){
    case 4:
      $text .= "error.png";
      break;
    case 2:
      $text .= "exclamation.png";
      break;
    case 1:
    case 8:
    default:
      $text .= "comment.gif";
      break;
  }
  $text .= "' alt=''/>
					</td><td>
					$message</td>
					</tr>
					</table>";
  if ($display_alert) {
    echo $text;
  } else {
    return $text;
  }
}

/**
 * Recursively apply the htmlentities() function to each element of an array
 *
 * @param array $r
 * @return array
 */
function htmlentities_array($r){
  if(!is_array($r))return false;
  foreach($r as $field=>$value){
    $r[$field]=htmlentities($value);
  }
  return $r;
}

/**
 * Recursively apply the html_entity_decode() funciton to each element of an array
 *
 * @param array $r
 * @return array
 */
function html_entity_decode_array($r){
  if(!is_array($r))return false;
  foreach($r as $field=>$value){
    $r[$field]=html_entity_decode($value);
  }
  return $r;
}

/**
 * Recursively add slashes to each element of an array
 *
 * @param array $r
 * @return array
 */
function addslashes_array($r){
  foreach($r as $key=>$value){
    if(is_array($value)){
      $a[$key]=addslashes_array($value);
    }
    else{
      $a[$key]=addslashes($value);
    }
  }
  return $a;
}

/**
 * Recursively strip slashes from each element of an array
 *
 * @param array $r
 * @return array
 */
function stripslashes_array($r){
  foreach($r as $key=>$value){
    if(is_array($value)){
      $a[$key]=stripslashes_array($value);
    }
    else{
      $a[$key]=stripslashes($value);
    }
  }
  return $a;
}

/**
 * This function is still in beta. It will work with single level folders ,but i haven't worked out hte bugs yet for
 * nested folders.
 * It should pull return all the filenames from any folder and its subfolders that you specify.
 *
 * @param string $directory
 * @param string $directory
 * @return mixed
*/
function fetch_files($directory,$extension=null){
  global $__DIRECTORY_FILES__;
  $parent=&$__DIRECTORY_FILES__;
  if(substr($directory,-1,-1)!=DIR_TKN){
    $directory.=DIR_TKN;
  }
  if ($handle = opendir($directory)) {
    while(false!==($file=readdir($handle))) {
      if ($file == '.' || $file == '..') continue;
      elseif(is_dir($directory.$file)){
        continue;
        $level_up=str_replace(DIR_TKN.$file,"",basename(dirname($directory.$file)));
        echo $level_up;
        $parent[$level_up][$file]=array();
        $parent=&$parent[$level_up][$file];
        fetch_files($directory.$file,$extension);
      }
      elseif(is_file($directory.$file)){
        $curr_dir=basename(dirname($directory.$file));
        $parent[$curr_dir]['files'][]=basename($file,$extension);
      }
    }
    closedir($handle);
  }
}

/**
 * Replaces single slashes with its html equivelant
 *
 * @param string $string
 * @return string
 */
function encodequote($string){
  return str_replace("'","&#39;",$string);
}

/**
 * Checks the get_magicquotes_gpc() to see if magicquotes is set, then returns the array or string with stripslashes or addslashes called recursively New version uses GPC_ constants to handle more options.
 * @since 2007-08-21
 * @version 2.0
 * @var array $array
 * @var integer $options one of GPC_XXXX variables, either singular or in array (GPC_XXX,GPC_XXX)
 * @return  mixed
*/
function gpc_clean_array($array,$options=null){
  if($options==""){
    $options=array();
    $options[]=GPC_STRIP_SLASHES;
  }elseif(!is_array($options) && $options!=""){
    $options=array();
    $options[]=$options;
  }

  if(is_array($array)){
    foreach($array as $id=>$value){
      $array[$id]=gpc_clean_array($value,$options);
    }
    return $array;
  }
  else{
    if(get_magic_quotes_gpc()){
      if(!in_array(GPC_ADD_SLASHES,$options)){
        $string=stripslashes($array);
      }else{
        $string=$array;
      }
    }else{
      if(!in_array(GPC_STRIP_SLASHES,$options)){
        $string= $array;
      }else{
        $string=addslashes($array);
      }
    }

    if(in_array(GPC_LTRIM,$options)){
      $string=ltrim($string);
    }elseif(in_array(GPC_RTRIM,$options)){
      $string=rtrim($string);
    }elseif(in_array(GPC_TRIM,$options)){
      $string=trim($string);
    }
    return $string;
  }
}

/**
 * Register a new button to use in the navigation
 *
 * @param string $display_text
 * @param string $button_name Any Unique identifier for the tab.
 * @param string $href
 * @param string $make_active
 */
function add_button($display_text, $button_name, $href, $make_active=false){
  global $menu_buttons;
  global $CFG;
  $menu_buttons[] = array("text" =>$display_text, "button_name" => $tab_name,"button_href"=> $href);
  if($make_active){
    $CFG['button'] = $button_name;
  }
}

/**
 * Register a tab to use in the navigation. Only shows on the page that it is registered on.
 *
 * @param string $display_text
 * @param string $tab_name Any Unique identifier for the tab.
 * @param string $href
 * @param string $make_active
 */
function add_tab($display_text, $tab_name, $href, $make_active=false){
  global $menu_tabs;
  global $CFG;
  $menu_tabs[] = array("text" =>$display_text, "tab_name" => $tab_name,"tab_href"=> $href);

  if($make_active){
    $CFG['tab'] = $tab_name;
  }
}

function addFormSubmitButtons(){
  global $menu_buttons;
  global $module;
  $menu_buttons = array(
  array("text"=>"Save","button_href"=>"javascript:confirm_close=false;document.forms[0].submit()"),
  array("text"=>"Cancel","button_href"=>"&action="),
  );
}

/**
 * Generates a random GUID as a string in the format "{nnnnnnnn-nnnn-nnnn-nnnn-nnnnnnnnnnnn}"
 * @return string
 */
function gen_guid(){
  $charid = strtoupper(md5(uniqid(rand(), true)));

  $hyphen = chr(45); // "-"

  $uuid = chr(123) // "{"
  .substr($charid, 0, 8).$hyphen
  .substr($charid, 8, 4).$hyphen
  .substr($charid,12, 4).$hyphen
  .substr($charid,16, 4).$hyphen
  .substr($charid,20,12)
  .chr(125); // "}"

  return $uuid;
}

/**
 * Iterates through the output buffers and deletes them.
 *
 */
function clear_ob(){
  while (ob_get_length()>0){ob_clean();}
}
include(dirname(__FILE__).DIR_TKN."custom_functions.inc.php");