<?php
/**
 * This file contains class names that will be displayed in the class versions list.
 *
 * Current as of 9/27/2011
 *
 */
$last_updated = "2011-09-27";
$classes = array(
"analytics"       => array("name"=>"Analytics",     "version"=>"1.0",   "date"=>"2010-04-30"),
"captcha"         => array("name"=>"Captcha",       "version"=>"1.1",   "date"=>"2009-07-01"),
"db"              => array("name"=>"Database",      "version"=>"4.4.3", "date"=>"2011-08-29",),
"filters"         => array("name"=>"Filters",       "version"=>"1.1",   "date"=>"2009-07-01"),
"form"            => array("name"=>"Form",          "version"=>"1.5",   "date"=>"2010-04-27"),
"image"           => array("name"=>"Image",         "version"=>"2.0",   "date"=>"2008-11-11"),
"layout"          => array("name"=>"layout",        "version"=>"1.0",   "date"=>"2010-08-18"),
"menu"            => array("name"=>"Menu",          "version"=>"2.4.1", "date"=>"2011-11-09"),
"module_template" => array("name"=>"ModuleTemplate","version"=>"1.0",   "date"=>"2009-07-02"),
"PackageManager"  => array("name"=>"PackageManager","version"=>"1.5.1", "date"=>"2011-09-27"),
"page"            => array("name"=>"page",          "version"=>"1.0",   "date"=>"2009-07-01")
);