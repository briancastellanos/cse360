<?php
class Person {
  protected $personId;
  protected $firstName;
  protected $lastName;
  protected $userName;
  protected $password;
  protected $emailAddress;
  protected $phoneNumber;
  protected $type;
  protected $status;
  
  public function __get($what) {
    return $this->$what;
  }
  public function __set($what, $value) {
    $this->$what=$value;
  }
  public function __construct($id=null) {
    if ($id >0) {
      /**
       *
       * @param $db Database          
       */
      $db=DatabaseFactory::getDb();
      if ($db instanceof Database) {
        $data=$db->query_fetch_assoc("SELECT * FROM person WHERE person_id='$id'", false);
        
        if ($data == false) {
          throw new Exception("User $id not found: $db->sql $db->error");
        } else {
          
          $this->firstName     = $data['first_name'];
          $this->lastName      = $data['last_name'];
          $this->emailAddress  = $data['email_address'];
          $this->phoneNumber   = $data['phone_number'];
          $this->type          = $data['type'];
          $this->status        = $data['status'];
          $this->personId      = $data['person_id'];
          $this->userName      = $data['username'];
          $this->password      = $data['password'];
        }
      }
    }
  }
  public function save() {
    $db=DatabaseFactory::getDb();
    $data=array(
    "first_name"     => $this->firstName,
    "last_name"      => $this->lastName,
    "email_address"  => $this->emailAddress,
    "phone_number"   => $this->phoneNumber,
    "type"           => $this->type,
    "status"         => $this->status,
    "username"       => $this->userName,
    "password"       => $this->password    
    );
    
    if ($this->personId ==0) {
      $data['created_on']="DB_TIMESTAMP";
      $method="insert";
    } else {
      $data['person_id']=$this->personId;
      $method='update';
    }
    
    $tmp = $db->mk_sql(addslashes_array($data), "person", $method, "person_id");
    
    if($method=="insert"){
      $this->personId = $tmp;
    }
    return $this->personId;
  }
  
  /**
   *
   * @param Address $address          
   */
  public function saveAddress(&$address) {
    $db=DatabaseFactory::getDb();
    
    if ($this->personId <=0 ) {
      $this->save();
    }
    
    $address->personId=$this->personId;
    $address->save();
  }
}
class Patient extends Person {
  private $doctorId;
  private $insuranceCarrier;
  private $insuranceId;
  private $ssn;
  private $dob;
  
  public function __construct($id=null) {
    parent::__construct($id);

    if ($this->personId != null) {
      $db=DatabaseFactory::getDb();
      $data=$db->query_fetch_assoc("SELECT * FROM patient WHERE person_id='$id'", false);
      $this->insuranceCarrier=$data["insurance_carrier"];
      $this->insuranceId=$data["policy_number"];
      $this->ssn=$data["ssn"];
      $this->doctorId=$data["doctor_id"];
    }
  }
  
  public function save() {
    $db = DatabaseFactory::getDb();
    $originalId=$this->personId;
    parent::save();
    
    $data=array(
      "person_id"          => $this->personId,
      "insurance_carrier"  => $this->insuranceCarrier,
      "policy_number"      => $this->policyNumber,
      "ssn"                => $this->ssn,
      "doctor_id"          => $this->doctorId,
    );
    
    if ($originalId == false) {
      $db->mk_sql(addslashes_array($data), "patient", "insert");
    } else {
      $db->mk_sql(addslashes_array($data), "patient", "update", "person_id");
    }
    
    return $this->patientId;
  }
  
  public function __get($what) {
    return $this->$what;
  }
  public function __set($what, $value) {
    $this->$what=$value;
  }
}

class Presciption {

}

class PatientVisit {

}

class PatientData {
  private $visitDate;
}
