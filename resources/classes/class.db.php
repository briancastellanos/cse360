<?php
define("DATAGRID_SORT_ASC"                ,0);
define("DATAGRID_SORT_DESC"               ,1);
define("DATAGRID_SEARCH_CONDITION_WHERE"  ,0);
define("DATAGRID_SEARCH_CONDITION_HAVING" ,1);

define("DATAGRID_FILTER_MULTIPLE"         ,0);
define("DATAGRID_FILTER_SINGLUAR"         ,1);
define("DATAGRID_FILTER_SINGULAR"         ,1);
define("DATAGRID_FILTER_DATE_RANGE"       ,2);
define("DATAGRID_FILTER_DATE_RANGE_FUTURE",5);
define("DATAGRID_FILTER_IN_GROUP"         ,3);
define("DATAGRID_FILTER_NODE_LIST"        ,4);

class DatabaseFactory {
  
  /**
   * @param String $username
   * @param String $password
   * @param String $host
   * @return Database
   */
  public static function getDb($username=null,$password=null,$host=null){
    static  $database;
    if($database==null){
     $database =  new Database($username, $password, $host);
    }
    return $database;
  }
}

/**
 * Creates database connection and allows datagrids to be created.
 *
 * @author Brian Castellanos
 * @version 4.5
 * @since 2011-11-16 v4.5     Adds error notification on sql errors, improves logging functions.
 * @since 2011-08-29 v4.4.3   Adds DATAGRID_FILTER_DATE_RANGE_FUTURE
 * @since 2011-06-22 v4.4.2   Fixes bug caused from using uniqid for temporary table names
 * @since 2011-05-31 v4.4.1   Fixes pagination error caused from changing filter submit method.
 * @since 2011-04-27 v4.4     Alllows default filter values to be set (requires `datagrid_filter_default`), allows new filter type NODE_LIST to be used
 * @since 2011-04-19 v4.3.1   Bug Fixes for datagrid updates from version 4.3
 * @since 2010-12-02 v4.3     Moves datagrid resources into separate components, datagrid.js and datagrid.css
 * @since 2010-03-05 v4.2.3.1 Fixed issue with sorting when /*orderby* / token not included
 * @since 2010-03-02 v4.2.3   Added support for multiple ORDER BYs with no token.
 * @since 2010-02-03 v4.2.2   Added ability to turn off webslices
 * @since 2010-01-25 v4.2.1   Added argument to register_icon to allow delete confirmation to be created dynamically
 * @since 2010-01-06 v4.2     Added ability to Subscribe as RSS feed - allows faster access to changing information
 * @since 2010-01-06 v4.1     Added ability to hide searchbox, control width, hide highlight feature, fix bug for displaying context menu from temporary table,
 * Adds ability to display a printable format.
 * @since 2009-12-31 v4.0.1.3 Added fixed width to text search box - 15 characters.
 * @since 2009-12-29 v4.0.1.2 Added notification in failure to create temporary tables on datagrids.
 * @since 2009-12-28 v4.0.1.1 Allow option groups in filters, allows date selection filters to have a default other than "today"
 * @since 2009-12-22 v4.0.0.0 Organized methods and functions together. Reapplied various bug fixes.
 * @since 2009-11-05 v3.4.0.0 Added ability to relate filters together.
 * @since 2009-09-11 Removed the display of developer debugging info when a certain condition was met
 * @since 2009-08-31 Added tokens, for MySQL replacements of special tokens
 * @since 2009-08-20 Added argument for new window in register_icon
 * @since 2009-07-01
 * @since 2009-06-25 v3.3 update to version 3.3, updated PHPDoc, allows use of dgqsval in querystring to reset filters and set values to those in querystring.
 */

class Database {

  const ICON_EDIT_RECORD                 = "/img/sm_icon/application_edit.png";
  const ICON_LINK_RECORD                 = "/img/sm_icon/link.png";
  const ICON_DELETE_RECORD               = "/img/sm_icon/application_delete.png";

  ###########################
  #DATABASE CLASS PROPERTIES#
  ###########################
  /**
   * The version number of the current datagrid class
   * @since 4.4 2011-04-27
   * @var string
   */
  public $version                        =    "4.5";
  /**
   * The name of the current active database
   * @var string
   */
  public $dbname                         =    null;
  /**
   * An array containing result sets of queries which have been executed
   * @var array
   */
  public $result                         =    array();
  /**
   * A flag that says wheather to display debug output
   * @var boolean
   */
  public $debug                          =    false;
  /**
   * The mysql connection resource handle used to execute queries
   * @var resource
   */
  public $mysql                          =    null;
  /**
   * Contains the mysql_affected_rows for the last query run
   *
   * @var integer
   */
  public $affected                       =    -1;
  /**
   * Contains the current database name
   *
   * @var string
   */
  public $dbcurrent                      =    null;
  /**
   * Contains true/false depending on the connection status to the MySQL server
   *
   * @var boolean
   */
  public $connected                      =    null;
  /**
   * Additional MySQL resource created to handle logging on a separate connection.
   * @var object
   */
  private $spare_resource                =    null;
  /**
   * Contains the mysql_error value for the last query run
   *
   * @var string
   */
  public $error;
  /**
   * Contains the mysql_error_no for the last query run
   *
   * @var int
   */
  public $error_no;
  /**
   * Contains the last SQL statement run
   *
   * @var string
   */
  public $sql;

  ###########################
  #DATAGRID CLASS PROPERTIES#
  ###########################

  ##
  ## 1. - Datagrid Internal Properties
  ## These are still set to public so they can be overriden, but they don't
  ## Need to be explicitly configured
  ##
  /**
     * The sort direction for the datagrid, a is ascending
     *
     * @var string
     */
  public $datagrid_sort_direction        =    "a";
  /**
     * The field that the datagrid is being sorted on.
     *
     * @var string
     */
  public $datagrid_sort_field            =    null;
  /**
     * The page number of the currently displayed datagrid
     *
     * @var integer
     */
  public $datagrid_page_number           =    1;

  /**
     * A string containing the output of the conditions generated from the filters.
     *
     * @var string
     */
  public $datagrid_filter_conditions     =    null;
  /**
     * The text that was entered via the Search textbox
     *
     * @var string
     */
  public $search_filter_criteria         =    null;

  /**
     * Sets whether or not to show the search box
     *
     * @var boolean
     */
  public $display_searchbox              =    true;

  /**
     * A counter for generating row ids.
     *
     * @var int
     */
  public $datagrid_row_number            =    1;

  ## 2. - Record Display Configuration
  ## Sets configuration related to data in the grid

  /**
     * Sets how many records per page should be displayed;
     * @var int
     */
  public $datagrid_rpp                   =    20;
  /**
     * A message that is displayed when there are no records in a result set
     *
     * @var string
     */
  public $datagrid_message_norecords     =    "There are no records to display";



  /**
   * Sets whether to display the "Print Page" button at the bottom of the grid.
   *
   * @var boolean
   */
  public $display_print                  = true;
  /**
   * Sets whether or not to display the RSS button
   *
   * @var boolean
   */
  public $display_rss                    = false;

  /**
   * An array of Links that will be added to the footer as buttons, with keys href and values display text.
   *
   * @var array
   */
  public $footer_buttons                 = array();


  ## 3. - Highlighting
  ## Sets configuration options related to highlighting
  ##
  /**
   * If sets to true, matching text in the datagrid is highlighted
   *
   * @var boolean
   */
  public $display_highlight_checkbox      =    true;

  /**
   * Sets whether the default state of the checkbox should be set.
   *
   * @var boolean
   */
  public $datagrid_highlight_default_on  = true;

  /**
   * If set to true, when rows mouse over, the row color changes to a different color.
   *
   * @var boolean
   */
  public $datagrid_highlight_rows        =    true;
  /**
   * A css name or hex value for the color code used to highlight rows
   *
   * @var string
   */
  public $datagrid_row_highlight         =    "#cecece";
  /**
   * Sets the default sort order as descending for numeric and date columns.
   *
   * @var boolean
   */
  public $datagrid_reverse_sort_non_text = true;
  /**
   * a type of DATAGRID_SEARCH_LOCATION_WHERE or DATAGRID_SEARCH_LOCATION_HAVING
   * that corresponds to the WHERE or HAVING section of the query
   *
   * @var integer
   */
  public $datagrid_search_condition_location = DATAGRID_SEARCH_CONDITION_WHERE;
  /**
   * An array containing the fields to be used in CSV export
   *
   * @var array
   */
  public $datagrid_csv_field_map         =    array();
  /**
   * An array containing additional field/label values that will be inserted in the CSV export
   * @var array
   */
  public $datagrid_csv_additional_fields =    array();
  /**
   * Contains the total number of records in the resultset of the last-run query
   * @var integer
   */
  public $datagrid_total_records		   =    null;
  /**
   * The number of records on the current page.
   * @var integer
   */
  public $datagrid_page_records		   =    null;
  /**
   * Specifies the form method to use when submitting datagrid filters.
   * @var string
   */
  public $datagrid_filters_form_method		   =	"GET";
  /**
   * A mapped array containing field names and values to be used for the datagrid
   * @var array
   */
  public $datagrid_icons                 =    false;
  /**
   * A mapped array containing field names and values to be used for the datagrid
   * @var array
   */
  public $datagrid_formatted_icons       =    false;
  /**
   * Sets whether to display filters or not
   * @var boolean
   */
  public $display_filters                =    true;
  /**
   * Maps the columns to include in the text seach (all with a specified field_name if null)
   * or a string containing the field names.
   *
   * @var mixed
   */
  public $datagrid_searchbox_fields      =    null;
  /**
   * Create a title row with specified text
   *
   * @var string
   */
  public $datagrid_title_text		       =	null;
  /**
   * Sets the color of the title, default is black
   *
   * @var string
   */
  public $datagrid_title_color		   =    "#FFFFFF";
  /**
   * Sets the background color of the title row
   *
   * @var string
   */
  public $datagrid_title_background	   = 	"#004a80";	#background-color
  /**
   * Sets the font-family to use for the title row
   *
   * @var string
   */
  public $datagrid_title_family		   = 	"verdana";	#font-family
  /**
   * Sets the "font-weight" attribute for the title row - default is "bold"
   *
   * @var string
   */
  public $datagrid_title_weight		   =	"bold";		#font-weight
  /**
   * Sets the font-size propert for the title row - default is 12px;
   *
   * @var string
   */
  public $datagrid_title_size			   =	"12px";		#text-size
  /**
   * Sets the font color of the header row - default is black
   *
   * @var string
   */
  public $datagrid_header_color		   =	"#000000";	#font-color
  /**
   * Sets the background color for the header row
   *
   * @var string
   */
  public $datagrid_header_background	   = 	"#FEFCE8";	#background-color
  /**
   * Sets the font-family property for the header row - deafult is verdana
   *
   * @var string
   */
  public $datagrid_header_family		   = 	"verdana";	#font-family
  /**
   * Sets the font-weight property for the header row - default is "bold"
   *
   * @var string
   */
  public $datagrid_header_weight		   =	"bold";		#font-weight
  /**
   * Sets the font-size property for the header row - default is 11px
   *
   * @var mixed
   */
  public $datagrid_header_size		   = 	"11px";		#font-size
  /**
   * Sets the "border" property of the table
   *
   * @var boolean
   */
  public $datagrid_table_border		   = 	true;
  /**
   * Sets the "cellpadding" property of the table
   *
   * @var int
   */
  public $datagrid_table_padding		   = 	2;
  /**
   * Sets the "cellspacing" property of the table
   *
   * @var int
   */
  public $datagrid_table_spacing		   = 	0;
  /**
   * Sets the "style" property of the main table;
   *
   * @var string
   */
  public $datagrid_table_style		   =  	null;
  /**
   * Sets the datagrid "width" propery, if one is provided.
   * @var mixed
   */
  public $datagrid_table_width		   =    null;
  /**
   * Sets the style attribute for row 1
   * @var string
   */
  public $datagrid_row_style_1           =    null;
  /**
   * Sets the style attribute for row 2
   * @var string
   */
  public $datagrid_row_style_2           =    null;
  /**
   * Sets the text color for the datagrid rows - default is black
   *
   * @var string
   */
  public $datagrid_row_color			   =    "#000000";	#font-color
  /**
   * Sets the background color for the alternating rows
   *
   * @var string
   */
  public $datagrid_row_background_1	   =    "#FFFFFF";	#Sets the alternating row color.
  /**
   * Sets the background color for the alternating rows
   *
   * @var string
   */
  public $datagrid_row_background_2	   =	"#EBF2FF";  #Sets the alternating row color
  /**
   * Sets the font family to be used for the datagrid rows - default is verdana
   *
   * @var string
   */
  public $datagrid_row_family		       = 	"verdana";	#font-family
  /**
   * Sets the font-weight property for the datagrid rows - default is "normal"
   *
   * @var string
   */
  public $datagrid_row_weight		       =	"normal";	#font-weight
  /**
   * Sets the font-size property for the datagrid rows - default is 11px
   *
   * @var mixed
   */
  public $datagrid_row_size			   = 	"11px";		#font-size
  /**
   * Sets the vertical alignment of the text in the datagrid row
   *
   * @var string
   * @since v4.5 2011-12-14
   */
  public $datagrid_row_valign       = "top";
  /**
   * Sets the token to use in the template if the page object is being used (for xhtml compliance)
   *
   * @var string
   */
  public $header_location			       =	"page_javascript";
  /**
   * An array containing registered datagrid filter information
   *
   * @var array
   */
  public $filters;
  /**
   * An array containing entries that will each be created as a cell
   * after the filters.
   **/
  public $filters_extra_cells;

  /**
   * Sets weather the datagrid should paginate results - default is true
   *
   * @var bool
   */
  public $datagrid_paginate;
  /**
   * Sets weather the datagrid should allow the headers to be sortable - default is true
   *
   * @var bool
   */
  public $datagrid_sortable;
  /**
   * Sets the headers/columns to be used in the datagrid
   * @var array
   **/
  public $datagrid_header_rows = array();
  /**
   * The current page number of the datagrid
   *
   * @var integer
   */
  public $dgpage;
  /**
   * The SQL statement that was used to generate the datagrid
   *
   * @var string
   */
  public $datagrid_sql;
  /**
   * A cleaned version of the query string with datagrid variables removed.
   *
   * @var string
   */
  public $query_string;
  /**
   * Contains fieldnames and their types in the result set executed in the query - used for sorting
   *
   * @var array
   */
  public $resultset_fields;
  /**
   * An array containing additional field/label values that will be inserted in the CSV export
   * @var array
   */
  public $datagrid_csv_fields;
  /**
   * Sets whether to display csv export or not
   * @var boolean
   */
  public $display_csv                     = true;
  /**
   * Controls the inheritance and relationships between filters
   * @var array
   */
  public $relation_stack                  = array();
  /**
   * Sets the Title above the global search box.
   *
   * @var string
   */
  public $datagrid_searchbox_label        = "Search";
  /**
   * Sets the subtitle of the search box below each filter
   *
   * @var string
   */
  public $custom_find_label               = "Custom Search";
  /**
   * Sets the 'Size' attribute of the the search box
   *
   * @var integer
   */
  public $datagrid_searchbox_size         = 15;
  /**
   * Sets the text that should be printed before the table is displayed on a printer friendly view
   * @var string
   */
  public $datagrid_printable_summary_text;
  /**
     * A string that is generated that contains uriencoded values for the filters so they can be retrived correctly
     *
     * @var string
     */
  public $rss_query_string = '';
  /**
     * The name of the field in the resultset that should be used as the title for each record.
     *
     * @var string
     */
  public $rss_title_field;
  /**
   * Corresponding alias name that is used as "{!token_name!}" to create a link for the record
   *
   * @var string
   */
  public $rss_link_icon;
  /**
   * The name of the field in the resultset that should be used as the date the record was created or updated
   *
   * @var string
   */
  public $rss_published_field;
  /**
   * The name of the field in the resultset that should be used as the description of the record
   *
   * @var string
   */
  public $rss_description_field;
  /**
   * Sets the field in the dataset to be used as the category
   *
   * @var string
   */
  public $rss_category_field;
  /**
   * Sets the title for the RSS feed.
   *
   * @var string
   */
  public $rss_feed_title;


  ##################
  #DATABASE METHODS#
  ##################
  #1.0 - Initalization
  /**
   * Instantiate a new Database object
   *
   * @param string $username  The username that should be used to connect to the database
   * @param string $password  The password that should be used to connect to the database
   * @param string $host      The name of the server that should be connected
   * @return object
   */
  public function __construct($username,$password,$host='localhost') {
    include_class("datagrid_filters");
    $this->connect($username,$password,$host);
    if($this->mysql==null || $this->mysql==false){
      $this->connected=false;
      $this->error_no=mysql_errno();
      $this->error=mysql_error();
      return false;
    }
    $this->connected = true;
  }
  /**
     * Establish a new MySQL connection
     *
     * @param string $username The username that should be used to connect to the database
     * @param string $password The password that should be used to connect to the database
     * @param string $host     The name of the server that should be connected
     * @return boolean Returns True of the connection was successful
     */
  public function connect($username,$password,$host='localhost'){
    $this->spare_resource = @mysql_connect($host, $username, $password     );
    $this->mysql          = @mysql_connect($host, $username, $password,true);

    if ($this->mysql == null || $this->mysql == false) {
      $this->connected = false;
      $this->error_no  = mysql_errno();
      $this->error     = mysql_error();
      return false;
    }else{
      $this->username  = $username;
      $this->password  = $password;
      $this->host      = $host;
      return true;
    }
  }
  /**
   * Disconnect from the current MySQL server
   *
   * @return boolean Returns true is the disconnection was okay
   */
  public function disconnect(){
    if($this->mysql==null || $this->mysql==false){
      return false;
    }else{
      if (@mysql_close($this->mysql)){
        $this->connected = false;
        return true;
      }
    }
  }
  /**
   * Remove all stored result sets from the Database object
   *
   */
  public function dump() {
    $this->affected = -1;
    while($res = array_pop($this->result)) {
      mysql_free_result($res);
    }
    $this->result = array();
  }
  /**
   * Select a new database on the MySQL server
   *
   * @param string $database The name of the database to switch to
   * @return bool
   */
  public function selectDB($database) {
    $switch = @mysql_select_db($database, $this->mysql);
    if($switch){
      $this->dbcurrent = $database;
      return true;
    }else{
      $this->error    = mysql_error();
      $this->error_no = mysql_errno();
      return false;
    }
  }
  /**
   * Ping the current MySQL server
   *
   * @return mixed
   */
  public function ping(){
    if($this->mysql==''){
      return false;
    }else{
      return @mysql_ping($this->mysql);
    }
  }

  #2.0 - MySQL Result Handling
  /**
   * Fetch a row as an associative array
   *
   * @param object $resource A result resource from a mysql_* function
   * @return mixed
   */
  public function fetch_assoc($resource = null) {
    if($this->result == null && $resource == null) {
      return false;
    }
    else{
      $res = ($resource == null ? $this->result[count($this->result)-1] : $resource );
      if($res == null) {
        return false;
      }
      else {
        return mysql_fetch_assoc($res);
      }
    }
  }
  /**
   * Fetches a result set as an numeric and associative array
   *
   * @param object $resource A result resource from a mysql_* function
   * @return mixed
   */
  public function fetch_array($resource = null) {
    if($this->result == null && $resource == null)
    return false;
    $res = ($resource == null ? $this->result[count($this->result)-1] : $resource);
    if($res == null) {
      return false;
    }
    return mysql_fetch_array($res);
  }
  /**
   * Gets result data from only one field in a result set
   *
   * @param string $index The field name from the result set
   * @param integer $row  The row number to return
   * @param object $resource A result resource from a mysql_* function
   * @return mixed
   */
  public function fetch_result($index = null, $row = null, $resource = null) {
    if(count($this->result)==0 && $resource == null){
      return false;
    }
    else{
      $res = ($resource == null ? $this->result[count($this->result)-1] : $resource);
      if(gettype($res)!='resource') {
        return false;
      }else{
        return @mysql_result($res, $row, $index);
      }
    }
  }
  /**
     * Fetch a result row as an enumerated array
     *
     * @param object $resource A result resource from a mysql_* function
     * @return mixed
     */
  public function fetch_row($resource = null) {
    if($this->result == null && $resource == null)
    return false;
    $res = ($resource == null ? $this->result[count($this->result)-1] : $resource);
    if($res == null) {
      return false;
    }
    return mysql_fetch_row($res);
  }
  /**
   * Frees the last resource set from the result set array
   *
   */
  public function free_result() {
    $res = array_pop($this->result);
    if(gettype($res)=='resource')	mysql_free_result($res);
  }
  /**
   * Send a SQL query to MySQL
   *
   * @param string $sql A formatted MySQL Query
   * @param boolean $free_resource Set to true to delete the previous result stored in the result stack
   * @return mixed On error, returns false. For queries with Result Sets, returns a result, for others, returns true.
   */
  public function query($sql, $free_resource = true,$trigger_warnings=true) {
    $this->sql=$sql;
    $result = mysql_query($sql, $this->mysql);
    if($result===false){
      $this->error=mysql_error($this->mysql);
      $this->error_no=mysql_error($this->mysql);
      $this->affected=0;

      if($trigger_warnings){
        $bt = debug_backtrace();

        if(!stristr($bt[0]['file'],"class.db.php")){
          $bt = $bt[0];
        }else{
          $bt = $bt[1];
        }

        $file     = str_replace(DOC_ROOT,"",$bt['file']);
        $line     = $bt['line'];
        $function = $bt['function'];

        // $err_msg = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "," - Syntax error near <b>",$this->error."</b>");
        //$substr = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","",$this->error);
        //$err_str = "<b>An error was encountered while executing the SQL</b><br />$err_msg<hr /><pre style='text-size:6px;font-family:courier;'>".str_replace($substr,"<b>$substr</b>",$sql)."</pre>";

        if(strlen($sql)>900){
          preg_match("/at line ([0-9]+)/",$this->error,$matches);
          $error_line  = intval($matches[1]);
          $sql_lines   = explode("\n",htmlentities($sql));
          $start_line  = max(0,$error_line-3);
          $end_line    = min($error_line+3 , count($sql_lines));
          if($error_line==0){
            $sql = substr($sql,0,800);
          }
          else{
            $error_lines = array();
            for($line_number=$start_line; $line_number<$end_line; $line_number++){
              $error_lines[] =  ($line_number==$error_line ? "<div style='background-color:#9BE38D'>":"")."{$line_number}.\t{$sql_lines[$line_number]}\n".($line_number==$error_line ? "</div>":"");
            }
            $sql = implode($error_lines);
          }
        }

        $err_msg = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","Syntax error near <b>",$this->error."</b>");
        $err_str = "The Database::{$function}() call failed on line {$line} in {$file} because: {$err_msg}<br /><pre>{$sql}</pre>";
        trigger_error($err_str,E_USER_WARNING);
      }
    }

    if($this->mysql!=''){
      $this->affected = mysql_affected_rows($this->mysql);
    }

    if($free_resource === true){
      $this->free_result();
    }
    if(gettype($result)=='resource'){
      array_push($this->result, $result);
    }

    $this->error    = mysql_error($this->mysql);
    $this->error_no = mysql_error($this->mysql);
    return $result;
  }

  #3.0 - Listing Functions
  /**
     * List available databases on the current MySQL server
     *
     * @return mixed
     */
  public function list_dbs(){
    $db_list = mysql_list_dbs($this->mysql);
    if(!$db_list){
      $this->error=mysql_error($this->mysql);
      $this->error_no=mysql_errno($this->mysql);
      return false;
    }
    while ($row=$this->fetch_assoc($db_list)) {
      $list[$row['Database']]=$row['Database'];
    }
    return $list;
  }
  /**
     * List field details for a table on the current database or a different database
     *
     * @param string $table The name of the table to get information for
     * @param string $database  The name of the database the table is located on
     * @return mixed
     */
  public function list_fields($table,$database=null){
    if($database!=null){
      $this->selectDB($database);
    }
    $sql="SHOW COLUMNS FROM `$table`";
    $result=$this->query($sql,false);
    if($result!==false){
      while($r=$this->fetch_assoc()){
        $fields[$r['Field']]=$r;
      }
      return $fields;
      $this->free_result();
    }
    else{
      return false;
    }
  }
  /**
     * List table information for the current database
     *
     * @param string $database The name of the database to get table information for
     * @return mixed
     */
  public function list_tables($database){
    if($this->dbcurrent!=''){
      $current_db=$this->dbcurrent;
    }
    $this->selectDB($database);
    $sql="SHOW TABLES FROM $database";
    $result=$this->query($sql,false);
    if($current_db!=''){
      $this->selectDB($current_db);
    }
    if($result!==false){
      while($r=$this->fetch_row()){
        $tables[]=$r[0];
      }
      $this->free_result();
      return $tables;
    }
    else{
      return false;
    }
  }
  /**
     * List the enumeration values for a field in a table on the current database of a different database
     *
     * @param string $table The name of the table to get information for
     * @param string $field  The name of the field to get enum data for
     * @param boolean $auto_index If true uses numeric keys for enum values, if false, uses the enum value as a key
     * @param string $database
     * @return mixed
     **/
  public function list_enum($table,$field,$auto_index=false,$database=null){
    if($database!=''){
      if($this->dbcurrent!=''){
        $current_db=$this->dbcurrent;
      }
      $this->selectDB($database);
    }
    $sql="SHOW COLUMNS FROM $table WHERE field='$field'";
    $result=$this->query($sql,false);
    if($current_db!=''){
      $this->selectDB($current_db);
    }
    if($result!==false){
      $r=$this->fetch_assoc();
      $this->free_result();
      if(substr($r['Type'],0,4)=='enum'){
        foreach(explode("','",substr($r['Type'],6,-2)) as $v){
          if($auto_index){
            $values[]=$v;
          }
          else{
            $values[$v]=$v;
          }
        }
        return $values;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }
  /**
     * List the fields and the field type contained from a SQL query
     *
     * @param string $sql A formatted MySQL Query to execute
     * @return mixed
     * @since 01-25-2010
     * @author  Brian C.
     */
  public function list_fields_sql($sql){
    $result=$this->query($sql,false);
    if($result==false){
      return false;
    }
    else{
      while ($i < mysql_num_fields($result)) {
        $r = mysql_fetch_field($result, $i);
        foreach($r as $key=>$val){
          $field[$key]=$val;
        }
        $fields[]=$field;
        $i++;
      }
      $this->free_result();
      return $fields;
    }
  }

  #4.0 - Custom Functions
  /**
     * Compare the the values in compare against original and return a string telling
     * what the old value was and what the new value is.
     *
     * @param array $original The source array to be used
     * @param array $compare The array to compare
     * @return string
     */
  public function array_compare($original,$compare){
    if(!is_array($original)){
      $original = array();
    }
    if(!is_array($compare)){
      $compare = array();
    }

    foreach($compare as $key=>$value){
      if($original[$key]!=$value && key_exists($key,$original)){
        $text .= "'$key' changed from '{$original[$key]}' to '{$value}'\n";
      }
    }

    if($text == ''){
      $text = "There were no changes made";
    }
    return $text;
  }

  /**
 * Returns the tree for a node specified by id and its descendants, or all nodes if no  id is provided.
 * By default uses the type table, but can be used with other tables that have a similar structure.
 *
 * @param string $id
 * @param boolean $active
 * @param string $field
 * @param string $table
 * @return array
 */
  public function fetch_node_tree($id=null, $active=true, $field = "guid", $parent_field = "parent_guid", $table = "type"){
    //If a guid was provided
    if($id!=null){
      $array = $this->query_fetch_assoc("SELECT * FROM $table WHERE $field='$id'",false);
      $having = " HAVING node.lft BETWEEN {$array['lft']} AND {$array['rgt']}";
    }

    if($active == true){
      $where = "AND node.status = 'active'";
    }

    $sql = "SELECT node.$field, node.$parent_field,
node.status,
count(parent.$field) as level,
((node.rgt-node.lft-1)/2) as children,
concat(if(node.rgt-node.lft-1=0,'level','head'),count(parent.name)) as `class`,
concat('BETWEEN ',node.lft,' AND ',node.rgt) as filter_value,
node.name,
CONCAT(REPEAT('&nbsp;&nbsp;&nbsp;&nbsp;', (COUNT(parent.name) - 1)), node.name) AS formatted_name,
node.lft, node.rgt
FROM `$table` as `node`, `$table` as  `parent`
WHERE node.lft BETWEEN  parent.lft AND  parent.rgt {$where}
GROUP BY node.$field {$having}
ORDER BY node.lft";
    $this->query($sql);
    $array = array();
    while($r=$this->fetch_assoc()){
      $array[$r[$field]] = $r;
    }
    return $array;
  }



/**
 * Returns the tree for a node from the type table specified by a guid. $include_parent will include the seeding GUID, otherwise it will only include the children.
 *
 * @param string $guid
 * @param boolean $include_parent
 * @param boolean $only_active
 * @since 4.5 2011-12-05
 * @return array
 */
  public function fetch_guid_tree($guid=null, $include_parent=false, $only_active = true,$add_please_select = true){
    //If a guid was provided
    $array = array();
    if($guid != null){
      $parent = $this->query_fetch_assoc("SELECT * FROM type WHERE guid='$guid'",false);

      if($include_parent==true){
      $having = " HAVING node.lft BETWEEN {$parent['lft']} AND {$parent['rgt']}";
      }
      else{
      $having = " HAVING node.lft > {$parent['lft']} AND node.lft < {$parent['rgt']}";
      }

      if($only_active == true){
        $where = "AND node.status = 'active'";
      }

      $sql = "
      SELECT node.guid, node.parent_guid, node.status, node.name,node.lft, node.rgt,
      count(parent.guid) as level,
      ((node.rgt-node.lft-1)/2) as children,
      CONCAT(if(node.rgt-node.lft-1=0,'level','head'),count(parent.name)) as `class`,
      CONCAT('BETWEEN ',node.lft,' AND ',node.rgt) as filter_value
      FROM `type` as `node`, `type` as  `parent`
      WHERE node.lft BETWEEN  parent.lft AND  parent.rgt {$where}
      GROUP BY node.guid {$having}
      ORDER BY node.lft";
      $this->query($sql);

      while($r=$this->fetch_assoc()){
        if(count($array)==0){
          $starting_level = $r['level'];
        }
        $r['level']          = $r['level'] - ($starting_level-1);
        $r['class']          = (($r['rgt']-$r['lft']-1==0) ? 'level' : 'head').$r['level'];
        $r['formatted_name'] = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;",max($r['level']-$starting_level-1,0)).$r['name'];
        $array[$r["guid"]] = $r;
      }
    }
    if($add_please_select){
      $array = array(""=>"Please Select")+ $array;
    }
    return $array;
  }










  /**
   * Create an array containing name-value pairs from a SQL query
   *
   * @param string $sql A formatted MySQL Query
   * @param string $key The field name from the result set that should be used to populate the array keys
   * @param string $value The field name from the result set that should be used to populate the array values
   * @return mixed
   */
  public function create_array_values($sql,$key,$value){
    $records = array();
    $this->query($sql,false);
    while($r=$this->fetch_assoc()){
      $records[$r[$key]]=$r[$value];
    }
    $this->free_result();
    return $records;
  }

  /**
   * Build a SQL query from an array and send to MySQL
   *
   * @param array $r  An array containing fieldname value pairs
   * @param string $table  The table name the update should be written to
   * @param string $mode  One of insert, update, or delete
   * @param string $key  If update or delete, the key name of $r that is the primary key
   * @param boolean $log_entry  If true, logs changes and modifications to the audit_log
   * @param object $mysql_resource Which Mysql_connect resource to use
   * @return mixed
   */
  public function mk_sql($r,$table,$mode="insert",$key=null,$log_entry=true,$mysql_resource = null){
    $mode = strtolower($mode);
    if($r==null){$r=$_REQUEST['update'];}

    if($mode=='update') {
      foreach($r as $field=>$value){
        if(strtoupper($value)=="DB_TIMESTAMP"){
          $set .= "`$field`=NOW(), ";
        }
        elseif(substr($value,0,6)=="*FUNC*"){
          $set.="`$field`=".str_replace("*FUNC*","",$value).", ";
        }
        else{
          $set .= "`$field`='$value', ";
        }
      }

      $set = rtrim($set,", ");

      if($key!=''){
        $where="WHERE `$key`='{$r[$key]}'";
        $o_sql = "SELECT * FROM `$table` $where";
        $old_values = $this->query_fetch_assoc($o_sql,false);
        $log_text = $this->array_compare($old_values,$r);
        $record_key = $r[$key];
      }

      $sql="UPDATE `$table` SET $set $where";
    }
    elseif($mode=="delete"){
      if($key!=''){
        $where="WHERE `$key`='{$r[$key]}'";
        $record_key = $r['key'];
      }
      $sql="DELETE FROM `$table` $where";
    }
    else{
      //Insert a new record
      foreach($r as $key=>$value){
        $fields.="`$key`, ";
        if(strtoupper($value)=="DB_TIMESTAMP"){
          $values.="NOW(),";
        }
        elseif(substr($value,0,6)=="*FUNC*"){
          $values.=str_replace("*FUNC*","",$value).", ";
        }
        else{
          $values.="'$value',";
        }
      }

      $fields=trim($fields,", ");
      $values=trim($values,", ");

      $sql="INSERT INTO `$table` ($fields) VALUES ($values)";
    }

    $this->sql=$sql;

    if($mysql_resource!=null){
      if(!mysql_select_db($this->dbcurrent,$mysql_resource)){
        echo "Couldn't change database to $this->dbcurrent";
      }

      $result = mysql_query($sql, $mysql_resource);
      if($result===false){
        $this->error=mysql_error($mysql_resource);
        $this->error_no=mysql_error($mysql_resource);
        $this->affected=0;
      }
      else{
        $this->affected = mysql_affected_rows($mysql_resource);
        array_push($this->result, $result);
        $this->error    = mysql_error($mysql_resource);
        $this->error_no = mysql_error($mysql_resource);
      }
    }
    else{
      $this->query($sql,false);
    }

    if($this->error_no==0){
      $this->free_result();
      if($mode=="insert"){
        $id = mysql_insert_id();
        $record_key  = $id;
        $return_value = $id;
      }
      else{
        $return_value = $this->affected;
      }

      /*
      if($log_entry && $_SESSION['AdminId']!=''){
        $log_array = array(
        "admin_id"      => $_SESSION['AdminId'],
        "activity_type" => $mode,
        "target_table"  => $table,
        "record_key"    => $record_key,
        "log_note"      => $log_text."\n\nURL: ".$_SERVER['REQUEST_URI']."?".$_SERVER['QUERY_STRING']."\n\nPost Data\n".print_r($_REQUEST,true),
        "sql"           => $this->logSql ? $sql : "",
        "audit_date"    => "DB_TIMESTAMP"
        );

        $old_sql = $this->sql;
        $this->mk_sql(addslashes_array($log_array),"audit_log","insert",null,false,$this->spare_resource);
        $this->audit_sql = $this->sql;
        $this->sql = $old_sql;
        
        
        unset($mode);
        unset($table);
        unset($record_key);
        unset($sql);
        unset($log_text);
        unset($con2);
      }*/

      return $return_value;
    }
    else{
      return false;
    }
  }
  /**
   * Executes a query, and returns the result set as a multi-dimensional array with
   * result rows contained as elements. Optionally returns a single result row as an array instead
   * of a multi-dimensional array.
   *
   * @param string $sql A formatted MySQL Query
   * @param boolean $multi_dimensional_if_single If only one record is returned, should the single result be in a multi-dimensional array (array[0]=array[field]=>value)
   * @return mixed
   */
  public function query_fetch_assoc($sql,$multi_dimensional_if_single=true){
    $array = array();
    $this->query($sql,false);
    if($this->affected>0){
      if($this->affected>1 || $multi_dimensional_if_single){
        while($r=$this->fetch_assoc()){
          $array[] = $r;
        }
      }
      else{
        $array = $this->fetch_assoc();
      }
      $this->free_result();
      return $array;
    }
    else{
      $this->free_result();
      return false;
    }
  }

  /**
   * Runs a query, and returns a field from the result.
   *
   * @param string  $sql
   * @param string  $index
   * @param integer $row_number
   * @return mixed
   */
  public function query_fetch_result($sql,$index,$row=0){
    $res = $this->query($sql);
    if($res){
      return $this->fetch_result($index,$row,$res);
    }
    else{
      return false;
    }
  }


  ##################
  #DATAGRID METHODS#
  ##################
  /**
   * Register a new column to use in the datagrid table
   *
   * @param string $field_name The name of the field in the result set the column should be mapped to
   * @param string $field_display The text that should be used as a column title
   * @param string $field_custom Any special text that should be used as as the cell contents - can contain tokens [!token!] or {!icon!}
   * @param boolean $field_eval Should the contents of $field_custom be passed to eval()
   * @param array $extra  An array containing extra configurable parameters
   */
  public function register_header($field_name,$field_display,$field_custom=null,$field_eval=false,$extra=null){
    $header = array(
    "field_name"    => $field_name,
    "field_display" => $field_display,
    "field_custom"  => $field_custom,
    "field_eval"    => $field_eval,
    "field_nowrap"  => false,
    );

    if(is_array($extra)){
      foreach($extra as $key=>$value){
        $header['nowrap'] = $value;
        $header[$key]=$value;
      }
    }
    if(!is_array($this->datagrid_header_rows)){
      $this->datagrid_header_rows=array();
    }

    $this->datagrid_header_rows[]=$header;
  }

  /**
   * Register an icon to use in the datagrid referenced by $icon_alias
   *
   * @param string $icon_alias An alias used to refer back to the icon
   * @param string $icon_href The URI to link to, can be http:// or javascript:
   * @param string $legend_text Text that should be displayed in the grid key
   * @param string $icon_src The absolute path to the image location
   * @param boolean $new_page If true, the icon opens to a new page
   * @param boolean $is_delete If true, a javascript delete confirmation message is added to the icon.
   */
  public function register_icon($icon_alias,$icon_href,$legend_text,$icon_src,$new_page=false,$is_delete=false){
    $icon=array(
    "icon_name"      => $icon_alias,
    "icon_href"      => $icon_href,
    "icon_display"   => $legend_text,
    "icon_src"       => $icon_src,
    "icon_new_page"  => $new_page,
    "icon_is_delete" => $is_delete
    );

    if(!is_array($this->datagrid_icons)){
      $this->datagrid_icons=array();
    }
    $this->datagrid_icons[]=$icon;
  }
  /**
   * Create a new dropdown box to use in filtering the result set.
   *
   * @param string $text  Text that should display above the dropdown
   * @param mixed $column_position Either a fieldname from the result set or the column number (starting at 0 from the left)
   * @param mixed $options A OptionsList object or array of options that should be used to populate the dropdown
   * @param mixed $default_value_if_null If no value is selected, what the default selection should be. Either a string value or a integer of the key position.
   * @return array
   */
  public function register_filter($text,$column_position, $options, $default_value_if_null=null, $filter_type = DATAGRID_FILTER_MULTIPLE){
    //Establish the field name first
    if(is_integer($column_position)){
      $field_name = $this->datagrid_header_rows[$column_position]['field_name'];
    }elseif($column_position==''){
      $field_name ="";
    }else{
      $field_name = $column_position;
    }

    if($options instanceof OptionsList || $options instanceof OptionsNodeList){
      $options_obj = $options;
      $options = $options->options;
    }

    //Create the filter
    if($filter_type == DATAGRID_FILTER_DATE_RANGE){
      $filter = new DatagridListFilter($field_name,"NO_FILTER_COMPARATIVE",FILTER_CONJUNCTION_AND);
      $filter->comparison = 15;
      $filter->quote_value = false;
      $filter->add_slashes = false;

      $day_secs = (24*60*60);

      $labels = array(
      "All",
      "Today",
      "Yesterday",
      "This Week",
      "Last Week",
      "This Month",
      "Last Month",
      "This Year",
      );

      $values  =
      array (
      "dgall",
      "BETWEEN '".DATE('Y-m-d')." 00:00:00' AND '".DATE('Y-m-d')." 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', time()-$day_secs)." 00:00:00' AND '".DATE('Y-m-d', time()-$day_secs)." 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', time()-(date('w')*$day_secs))." 00:00:00' AND '".DATE('Y-m-d',time()+((6-date('w'))*$day_secs))." 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', mktime(0, 0, 0, date('m'), date('d')-7-date('w'), date('Y')))." 00:00:00' AND '".DATE('Y-m-d', mktime(0, 0, 0, date('m'), date('d')-(date('w')+1), date('Y')))." 23:59:59'",
      "BETWEEN '".DATE('Y-m-')."01' AND '".DATE('Y-m-t')." 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', mktime(0, 0, 0, date('m')-1, 1, date('Y')))."' AND '".DATE('Y-m-d', time()-((date('d'))*$day_secs))." 23:59:59'",
      "BETWEEN '".DATE('Y')."-01-01 00:00:00' AND '".DATE('Y')."-12-31 23:59:59'",
      );

      $options = array_combine($values,$labels);

      $keys = array_keys($options); array_shift($keys);
      $default_value_if_null = ($default_value_if_null==null ? array_shift($keys) : $default_value_if_null);

    }
    elseif($filter_type == DATAGRID_FILTER_DATE_RANGE_FUTURE){
      $filter = new DatagridListFilter($field_name,"NO_FILTER_COMPARATIVE",FILTER_CONJUNCTION_AND);
      $filter->comparison = 15;
      $filter->quote_value = false;
      $filter->add_slashes = false;

      $day_secs = (24*60*60);

      $labels = array(
      "All",
      "Today",
      "Tomorrow",
      "This Week",
      "Next Week",
      "This Month",
      "Next Month",
      "This Year",
      );


      $today       = date('Y-m-d');
      $tomorrow    = date('Y-m-d',strtotime("next day"));
      $bom_current = date('Y-m-01');
      $eom_current = date('Y-m-t');
      $bom_next    = date('Y-m-01',strtotime("next month"));
      $eom_next    = date('Y-m-t',strtotime("next month"));

      $values  =
      array (
      "dgall",
      "BETWEEN '{$today} 00:00:00' AND '{$today} 23:59:59'",
      "BETWEEN '{$tomorrow} 00:00:00' AND '{$tomorrow} 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', time()-(date('w')*$day_secs))." 00:00:00' AND '".DATE('Y-m-d',time()+((6-date('w'))*$day_secs))." 23:59:59'",
      "BETWEEN '".DATE('Y-m-d', mktime(0, 0, 0, date('m'), date('d')+7-date('w'), date('Y')))." 00:00:00' AND '".DATE('Y-m-d', mktime(0, 0, 0, date('m'), date('d')+7+(6-date('w')), date('Y')))." 23:59:59'",
      "BETWEEN '".DATE('Y-m-')."01' AND '".DATE('Y-m-t')." 23:59:59'",
      "BETWEEN '".DATE('Y-m-01 00:00:00', strtotime("+1 month"))."' AND '".DATE('Y-m-t', strtotime("+1 month"))." 23:59:59'",
      "BETWEEN '".DATE('Y')."-01-01 00:00:00' AND '".DATE('Y')."-12-31 23:59:59'",
      );

      $options = array_combine($values,$labels);

      $keys = array_keys($options); array_shift($keys);
      $default_value_if_null = ($default_value_if_null==null ? array_shift($keys) : $default_value_if_null);

    }
    elseif($filter_type == DATAGRID_FILTER_IN_GROUP) {
      $filter = new DatagridListFilter($field_name,"NO_FILTER_COMPARATIVE",FILTER_CONJUNCTION_AND);
      $filter->comparison  = 15;
      $filter->quote_value = false;
      $filter->add_slashes = false;
    }
    elseif($filter_type == DATAGRID_FILTER_NODE_LIST) {
      $filter = new DatagridListFilter($field_name,"NO_FILTER_COMPARATIVE",FILTER_CONJUNCTION_AND);
      $filter->comparison  = 15;
      $filter->quote_value = false;
      $filter->add_slashes = false;
    }
    else {
      $filter = new DatagridListFilter($field_name,FILTER_COMPARITIVE_EQUAL_TO,FILTER_CONJUNCTION_AND);
    }

    //Set the filter value to null if reset button was clicked.
    if(isset($_REQUEST['dgreset']) || isset($_REQUEST['dgqsval'])){
      $filter->value = null;
      $_SESSION['dgfilter'][$filter->db_hash] = null;
    }

    if($filter->value==''){
      $setDefaultValue = true;
    }

    if ($filter->value=='' && $default_value_if_null!==null) {
      $this->set_default_value($filter,$options,$default_value_if_null);
    }

    $_SESSION['dgfilter'][$filter->db_hash] = $filter->value;
    $index = count($this->filters)+1;

    $this->relation_stack[$index] = 0;
    $new_filter = array (
    "index"             => $index,
    "filter"            => $filter,
    "options"           => $options,
    "option_obj"        => $options_obj,
    "display_text"      => $text,
    "filter_type"       => $filter_type,
    "default"           => $default_value_if_null,
    "attributes"         => array(),
    "set_default_value" => ($setDefaultValue==true)
    );

    $this->filters[] = $new_filter;
    $this->relation_stack[$index] = 0;
    return $new_filter;
  }

  /**
   * Add a hyperlink to the bottom of the datagrid
   *
   * @param string $link The URI of the link
   * @param string $display The text that should be displayed
   * @param bool $new_window When the link is cliked, should it open in a new window
   */
  public function register_link($link,$display,$new_window=false){
    $this->footer_buttons[] = array($link,$display,$new_window);
  }

  /**
   * Defines a relationship so that options belonging to the parent filter are displayed. Array is returned from register_filter()
   *
   * @param array $parent The filter array that will modify the child filters options list
   * @param array $child The options of this filter will change to only be elements from which belong to parent array returned from register_filter()
   */
  public function register_parent_filter($filter,$parent){
    $this->relation_stack[$filter['index']] = $parent['index'];
  }

  /**
   * Executes a the create datagrid query and stores the unpaginated resultset to a temporary table
   *
   * @param $sql string A formatted MySQL Query
   * @return string
   */
  public function save_grid_to_temp($sql){
    //Generate a sql statement with filters, sorts, and search.
    $this->create_grid($sql,null,true,false,false);

    $temp_table_name = uniqid("temp_");
    $this->query("CREATE TEMPORARY TABLE `$temp_table_name` $this->datagrid_sql",true,false);

    if($this->error){
      $err_msg = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "," - Syntax error near <b>",$this->error."</b>");
      $substr = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","",$this->error);
      ob_start();
      notify("<b>There was an error when creating the temporary table in the Datagrid</b><br />
            {$err_msg}<hr />Query:<pre style='text-size:6px;font-family:courier;'>".str_replace($substr,"<b>$substr</b>",$this->sql)."</pre>",NOTIFY_DISPLAY_ERROR);

      $this->saved_err_message = ob_get_clean();
    }
    else{
      $this->saved_err_message = null;
    }
    $this->grid_temp_table    = $temp_table_name;
    $this->grid_temp_sortable = false;
    $this->datagrid_sql;
    return $this->grid_temp_table;
  }

  /**
   * Retrieves the results from the datagrid temporary table and builds the datagrid from the results.
   *
   */
  public function retrieve_grid_from_temp(){
    $this->build_filters_box();
    $this->filters_originally_displayed = $this->display_filters;
    $this->display_filters      = false;
    $this->called_from_retrieve = true;
    $this->create_grid("SELECT * FROM `$this->grid_temp_table`");
    $this->called_from_retrieve = false;
  }

  /**
   * Executes a query and builds a graphical datagrid, reverse compatible with old codeset, the $sql/$headers arugments can be reversed
   *
   * @param string $sql A formatted MySQL Query
   * @param mixed $headers Deprecated - an array of unformatted header arrays
   * @param bool $sortable Sets whether to allow the columns to be clicked
   * @param bool $paginate Sets wheter to display the pagination links
   * @param bool $execute_query Sets whether the query should be exectuted and grid built, or just alter the query
   * @return bool
   */
  public function create_grid($sql,$headers=null,$sortable=true,$paginate=true,$execute_query=true){
    global $form;
    $USE_FORM = $form instanceof Form ? true : false;
    if(!is_array($headers) && !is_array($this->datagrid_header_rows) && is_array($sql)){
      $tmp_sql = $headers;
      $headers = $sql;
      $sql     = $tmp_sql;
      unset($tmp_sql);
      foreach($headers as $header){
        $this->datagrid_header_rows[]=$header;
      }
    }
    else if ($headers!=null && is_array($headers)) {
      foreach($headers as $header){
        $this->datagrid_header_rows[]=$header;
      }
    }

    $this->datagrid_sortable = $sortable;
    $this->datagrid_paginate = $paginate;

    //If the reset flag has been set, set it to null, if the value has been passed, use it,
    //other wise load it from the stored values.
    $gv = $this->fetch_grid_values();

    if (isset($_GET['dgdefault'])) {
      $values = gpc_clean_array($_GET['dgdefault']);
      $this->save_filter_defaults($values);
    }

    $fd = $this->fetch_filter_defaults();
    $this->filter_defaults = $fd;
    //If either of the search field buttons have been clicked, reset the variables
    $dguseqs       = isset($_REQUEST['dgqsval'])        ? true : false; //If true, reset all and set filter values to those in query string.
    $dgreset       = isset($_REQUEST['dgreset'])        ? true : false; //If true, reset everything to default values.
    $dgresort      = isset($_REQUEST['dgrest'])         ? true : false; //If true, resets storting
    $dgsubmit      = isset($_REQUEST['dgfiltersubmit']) ? true : false; //if true, the form has been resbumitted.

    $dgrpp         = $dgreset                           ? null : (isset($_REQUEST['dgrpp'])  ? $_REQUEST['dgrpp']  : $gv['dgrpp'] );
    $dgsort        = $dguseqs || $dgreset || $dgsubmit  ? null : (isset($_REQUEST['dgsort']) ? $_REQUEST['dgsort'] : $gv['dgsort']);
    $dgdir         = $dguseqs || $dgreset || $dgsubmit  ? null : (isset($_REQUEST['dgdir'])  ? ($_REQUEST['dgdir']=='d' ? 'd' : 'a') :$gv['dgdir']);
    $dgpage        = $dguseqs || $dgreset || $dgsubmit || $dgresort ? 0 : (isset($_REQUEST['dgpage']) ? ($_REQUEST['dgpage']>1 ? floor($_REQUEST['dgpage'])-1:0) : $gv['dgpage']);


    $query_string  = gpc_clean_array($_GET);//$_SERVER['QUERY_STRING'];
    if(is_array($query_string)){
      unset($query_string['dgfiltersubmit']);
      unset($query_string['dghilite']);
      unset($query_string['dgdefault']);
      unset($query_string['dgfilter']);
      unset($query_string['dgrpp']);
      unset($query_string['dgsort']);
      unset($query_string['dgrest']);
      unset($query_string['dgreset']);
      unset($query_string['dgqsval']);
      unset($query_string['dgrss']);
      unset($query_string['dgprint']);
      unset($query_string['dgcsv']);
      unset($query_string['search']);

      $parts = array();
      foreach ($query_string as $parameter=>$value){
        $parts[] = $this->encodeQuerystringParameter($parameter,$value,false);
      }
      $query_string = implode("&",$parts);
    }

    if(in_array($dgrpp,array(20,50,100,250,500,1000,2000))){
      $this->datagrid_rpp = $dgrpp;
    }

    include_class("datagrid_filters");

    //Load the filters regardless if one was setup or not, this will force the textbox to show.
    if($this->display_filters==true){

      if(is_array($this->filters)){
        foreach($this->filters as $position=>$f){
          if($f['set_default_value']==true || isset($_REQUEST['dgdefault'])){
            if($fd[$f['filter']->db_hash]!=''){
              $default = $fd[$f['filter']->db_hash];
              $this->set_default_value($f['filter'],$f['options'],$default);
            }
          }
        }
      }

      /**
       * @since 2009-12-08 @briancastellanos
       * this adds logic see if the dgwhere token is missing and the "HAVING" attribute.
       * If it is, then the default location for the statement conditions will be the at the end.
       */
      if( stristr($sql,"/*dgwhere*/")===false && stristr($sql," having ")===false){
        $no_where_token = true;
        $this->datagrid_search_condition_location = DATAGRID_SEARCH_CONDITION_HAVING;
      }

      $this->load_filters($dguseqs);

      //Loop through the filters, resetting the values as needed
      if($dguseqs==true){
        foreach($this->filters as $filter_position=>$f){
          $value     = trim($_GET['dgfilter'][$f['filter']->db_hash],",");

          if($value==''){
            $value = trim($_GET['dgfilter'][$filter_position],",");
          }

          $isTextFilter = $f['filter'] instanceof DatagridTextFilter;

          if($value==''){
            if($isTextFilter){
              $this->set_value($f['filter'],"");
            }
            else{
              //check if a default has been saved. if so, use the saved default, if not use the programmer-defined default.
              if($fd[$f['filter']->db_hash]!=''){
                $default = $fd[$f['filter']->db_hash];
              }
              else{
                $default = $f['default'];
              }
              $this->set_default_value($f['filter'],$f['options'],$default);
            }
          }
          else{
            $this->set_value($f['filter'],$value);
          }
        }
      }

      $old_sql = $this->sql;
      $this->modify_child_filters();
      $this->sql = $old_sql;

      $filters_copy = $this->filters;
      if($this->display_searchbox){
        $first_array  = array_shift($filters_copy);
        $first_filter = $first_array['filter'];

        //Create a FilterLevel that will be used to nest the conditions.
        $parent_level     = new DatagridFilterLevel($first_array['filter']);
        $cached_values    = array($first_filter->db_hash => $first_filter->value);
        $filter_count++;
        $rss_query_string[] = "dgfilter%5B{$first_filter->db_hash}%5D=".urlencode($first_filter->value);
        //$rss_query_string[] = "dgf%5B$filter_count%5D=".urlencode($first_filter->value);
      }
      else{
        $has_filter_level = false;
        $cached_values    = array();
      }

      while ($array = array_shift($filters_copy)) {
        $filter   = $array['filter'];
        $db_field = $this->cleanFilterField($filter->db_field_name);
        $filter->setDbField($db_field);
        $filter->db_field_name = $db_field;

        /**
        * @since  2010-01-06 bricast - If the search box is not displayed, then the parent
        * level hasnt' been set up. So this will set up the filter level so it can generate the
        * condition
        */
        if($this->display_searchbox == false && $has_filter_level==false){
          $parent_level  = new DatagridFilterLevel($filter);
          $has_filter_level = true;
        }


        //Save this value so it can be retrived incase its manipulated.
        $cached_values[$filter->db_hash] = $filter->value;
        $filter_count++;
        // $rss_query_string[] = "dgfilter%5B{$filter->db_hash}%5D=".urlencode($filter->value);
        $rss_query_string[] = "dgf%5B{$filter_count}%5D=".urlencode($filter->value);

        if($filter->value == 'dgall'){
          $filter->value = null;
          $filter->dgall = true;
        }
        else if(
        (
        in_array($array['filter_type'],array(DATAGRID_FILTER_MULTIPLE,DATAGRID_FILTER_DATE_RANGE))
        &&
        stristr($filter->value,",")!==false
        )
        ||
        $array['filter_type']==DATAGRID_FILTER_IN_GROUP
        ||
        $array['filter_type'] == DATAGRID_FILTER_NODE_LIST
        ) {
          $filter->comparison = -1;
          $filter->quote_value = false;
          $filter->add_slashes = false;

          if($array['filter_type']==DATAGRID_FILTER_MULTIPLE || $array['filter_type']==DATAGRID_FILTER_IN_GROUP){
            if($array['filter_type']==DATAGRID_FILTER_IN_GROUP){
              $filter_value = $filter->value;
              foreach(explode(",",$filter_value) as $company_id){
                $field_value[] = "(FIND_IN_SET('$company_id',$filter->db_field_name))";
              }
              $filter->value = " IN ('$filter_value') OR ".implode(" OR ",$field_value);
            }else{
              $filter->value = "IN (".trim($filter->value,",").")";
            }
          }
          else if($array['filter_type']==DATAGRID_FILTER_DATE_RANGE){
            $parts = explode(",",$filter->value);

            $parts[0] = is_numeric($parts[0]) && $parts[0]<=12   && $parts[0] >=1    ? $parts[0] : date('m');
            $parts[1] = is_numeric($parts[1]) && $parts[1]<=31   && $parts[1] >=1    ? $parts[1] : date('d');
            $parts[2] = is_numeric($parts[2]) && $parts[2]<=2100 && $parts[2] >=1900 ? $parts[2] : date('Y');

            $parts[3] = is_numeric($parts[3]) && $parts[3]<=12   && $parts[3] >=1    ? $parts[3] : date('m');
            $parts[4] = is_numeric($parts[4]) && $parts[4]<=31   && $parts[4] >=1    ? $parts[4] : date('d');
            $parts[5] = is_numeric($parts[5]) && $parts[5]<=2100 && $parts[5] >=1900 ? $parts[5] : date('Y');
            $filter->value = "BETWEEN '{$parts[2]}-{$parts[0]}-{$parts[1]} 00:00:00' AND '{$parts[5]}-{$parts[3]}-{$parts[4]} 23:59:59'";

            $cached_values[$filter->db_hash] = implode(",",$parts);
          }
          //if the filter has one or more values that have been selected as custom from a
          //node list filter, then split the values. Each one should be a guid.
          //Since the lft and rgt values have already been queried and stored as part of the filter display,
          //there is no need to requery it. Instead check the index and getch the value back out.
          else if ($array['filter_type']==DATAGRID_FILTER_NODE_LIST){
            $filter_value = $filter->value;
            $betweens = array();

            foreach(explode(",",$filter_value) as $guid){
              if(array_key_exists($guid,$array['options'])){
                $betweens[] = "({$filter->db_field_name} BETWEEN {$array['options'][$guid]['lft']} AND {$array['options'][$guid]['rgt']})";
              }
            }
            //Here we say we want all records where the lft field is between the lft and rgt values of the selected
            //guid values. This means: (lft between 1 and 10 OR lft is between 20 and 25 OR lft is between 30 and 37) for example
            $filter->value = implode(" OR ",$betweens);
            $filter->db_field_name = null;
          }
        }

        if($filter->enabled===false){continue;}
        else {
          new DatagridFilterLevel($filter);
        }

      }

      //The condition has been generated
      $this->datagrid_filter_conditions = $parent_level->buildStatement();

      //Restore the values from the cache so they can be displayed in the
      //Filters correctly.
      foreach($this->filters as $f){
        $f['filter']->value = $cached_values[$f['filter']->db_hash];
      }

      $this->rss_query_string = "&amp;".implode("&amp;",$rss_query_string);
    }

    if ($this->datagrid_filter_conditions !='' ) {
      $conditions = trim($this->datagrid_filter_conditions, ' AND');

      if ($no_where_token) {
        /**
         * @since  2009-12-23 @brianc - If no tokens exists, but an order by does, split the query into two parts and inject the having.
         */
        if(stristr($sql,"ORDER BY")!==false){
          $order_pos = stripos($sql,"ORDER BY");
          $split[0] = substr($sql,0,$order_pos);
          $split[1] = substr($sql,$order_pos);
          $sql = $split[0]." HAVING {$conditions} ".$split[1];
        }else{
          $sql = "{$sql} HAVING {$conditions}";
        }

        if (stripos($sql, '/*dgorder*/')!==false) {
          $sql = str_ireplace('/*dgorder*/', '', $sql).' /*dgorder*/';
        }
        if (stripos($sql, '/*dglimit*/')!==false) {
          $sql = str_ireplace('/*dglimit*/', '', $sql).' /*dglimit*/';
        }
      }
      else {
        //First assign dgwhere, then check if it is false..
        if ( ($dgwhere = stripos($sql, '/*dgwhere*/')) !==false ) {
          //First assign dghaving then check if it is false
          if ( ($dghaving = stripos($sql, 'HAVING ', intval($dgwhere))) !==false) {
            $before = substr($sql, 0, $dghaving);
            $after  = substr($sql,   ($dghaving+7));
            $sql    = str_ireplace('/*dgwhere*/', " HAVING 1=1 AND {$conditions} AND {$after}", $before);
          } else {
            if(stristr($sql,"HAVING 1=1")){
              $sql    = str_ireplace('/*dgwhere*/', " AND {$conditions}", $sql);
            }else{
              $sql    = str_ireplace('/*dgwhere*/', " HAVING 1=1 AND {$conditions}", $sql);
            }
          }
        }
      }
    }

    if($this->datagrid_paginate==true){
      if (stripos($sql, '/*dglimit*/')!==false) {
        $sum_sql = str_ireplace('/*dglimit*/', '', $sql);
        $this->query($sum_sql,false,false);
        if($this->affected>0){
          $this->datagrid_total_records=$this->affected;
          $this->free_result();
        }
        else{
          $this->datagrid_total_records=0;
        }

        if (empty($_REQUEST['dgcsv']) && empty($_REQUEST['dgrss'])) {
          $offset  = intval($dgpage) * intval($this->datagrid_rpp);
          $perpage = intval($this->datagrid_rpp);
          $sql     = str_ireplace('/*dglimit*/', " LIMIT {$offset}, {$perpage}", $sql);
        }
        else {
          $sql = str_ireplace('/*dglimit*/', '', $sql);
        }
      }
      else {
        $pattern="/\s*LIMIT\s+\(?\s*[0-9]*\s*\,?\s*[0-9]*\s*\)?/i";
        (!isset($_REQUEST['dgcsv']) && !isset($_REQUEST['dgrss']) ? $replace=" LIMIT ".($dgpage*$this->datagrid_rpp).",".($this->datagrid_rpp) : "");
        $sum_sql=(preg_replace($pattern,"",$sql));
        $this->query($sum_sql,false,false);
        if($this->affected>0){
          $this->datagrid_total_records=$this->affected;
          $this->free_result();
        }else{
          $this->datagrid_total_records=0;
        }

        $new_sql=(preg_replace($pattern,$replace,$sql));
        if($new_sql==$sql){
          $new_sql.=$replace;
        }
        $sql=$new_sql;
      }
    }

    /*Build the links for auto sorting, if necessary*/
    if($this->datagrid_sortable===true && $this->called_from_retrieve !== true && $dgsort!=null ){
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/"  , "", $query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/", "", $query_string);
      $clean_query_string = rtrim($query_string,"&");
      $query_string       = rtrim($query_string,"&")."&";

      if($this->datagrid_header_rows[$dgsort]['field_name']!=''){
        $this->datagrid_sort_direction = $dgdir=='d' ? DATAGRID_SORT_DESC : DATAGRID_SORT_ASC;
        $this->datagrid_sort_field     = $dgsort;
        $fields     = explode(".",str_replace("`","",$this->datagrid_header_rows[$dgsort]['field_name']));
        $field_name = count($fields>1) ? $this->datagrid_header_rows[$dgsort]['field_name'] : "`{$this->datagrid_header_rows[$dgsort]['field_name']}`";
        $replace    = " ORDER BY $field_name ".($dgdir=='d'?"DESC":"ASC")." ";

        //If the modified query contains the dgorder token, then use it to place the ORDER BY
        //condition in the appropriate location
        if (stripos($sql, '/*dgorder*/')!==false) {
          $sql = str_ireplace('/*dgorder*/', $replace, $sql);
          if (stripos($sql, '/*dglimit*/')!==false) {
            $sql = str_ireplace('/*dglimit*/', '', $sql).' /*dglimit*/';
          }
        }

        //If theres no token, then there may be zero or more ORDER BY conditions
        //Find all instances of the ORDER BY conditions and only replace the last instance.
        else {
          $sql_parts     = preg_split("/\s+limit\s+/i",$sql);
          $pattern       = "/\s*ORDER\s+BY\s+\`?[\`a-zA-Z-0-9_\-\.]*\`?\s*(ASC|DESC)?/i";

          preg_match_all($pattern,$sql_parts[0],$matches);
          $order_by      = $matches[0][count($matches[0])-1];
          $last_order_by = strripos($sql_parts[0],$order_by);
          $sql           = substr($sql_parts[0],0,$last_order_by).str_replace($order_by,$replace,substr($sql_parts[0],$last_order_by));

          ;
          if($sql==$sql_parts[0]){
            $sql  = $sql_parts[0].$replace;
          }
          if( $sql_parts[1]!='' ){
            $sql .= " LIMIT ".$sql_parts[1];
          }
        }
      }
    }

    $sql = str_ireplace(array('/*dgwhere*/','/*dgorder*/','/*dglimit*/'), '', $sql);

    if($add_to_end){$sql.=$having_condition;}
    $this->query_string            = $query_string;
    $this->datagrid_sql            = $sql;
    $this->dgpage                  = $dgpage;

    if($_REQUEST['dghilite']){
      $dghilite = true;
    }
    elseif( isset($_REQUEST['dgfiltersubmit']) && !isset($_REQUEST['dghilite'])){
      if($this->display_highlight_checkbox){
        $dghilite = false;
      }
      else{
        $dghilite = $this->datagrid_highlight_default_on;
      }
    }
    else{
      $dghilite = $this->datagrid_highlight_default_on;
    }

    $this->save_grid_values(array(
    "dgrpp"    => $this->datagrid_rpp,
    "dgpage"   => $dgpage,
    "dgsort"   => $dgsort,
    "dgdir"    => $dgdir,
    "dghilite" => $dghilite)
    );

    if($execute_query){
      if(isset($_REQUEST['dgcsv'])){
        $this->create_csv($sql);
      }
      if(isset($_REQUEST['dgprint'])){
        $this->create_printable($sql);
      }
      if(isset($_REQUEST['dgrss'])){
        $this->create_rss($sql);
      }
      else{
        //ob_start();
        global $page;
        $rs = $this->query($sql,true,false);
        if($rs!==false){
          for($i=0;$i<mysql_num_fields($rs);$i++){
            switch (mysql_field_type($rs,$i)) {
              case "int":
              case "real":
                $field_type = "number";
                break;
              case "date":
              case "time":
              case "datetime":
                $field_type = "date";
                break;
              default:
                $field_type = "text";
            }
            $this->resultset_fields[mysql_field_name($rs,$i)] = $field_type;
          }
        }

        $this->datagrid_page_records = $this->affected;
        $this->create_header();
        if($this->affected<=0){
          echo "<tr><td colspan=\"100%\" align=\"center\">$this->datagrid_message_norecords</td></tr>";
        }
        else{
          while ($r=$this->fetch_assoc()) {
            $this->create_row($r);
            $this->datagrid_row_number++;
          }
        }
        if ($this->datagrid_paginate==true) {
          $this->create_footer();
        }

        $this->create_icons();
        echo "</table>";
        if($this->error){
          if($this->saved_err_message!=''){
            echo $this->saved_err_message;
          }else{
            $err_msg   = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","Syntax error near <b>",$this->error."</b>");
            $substr    = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","",$this->error);

            preg_match("/at line ([0-9]+)/",$err_msg,$matches);
            $error_line  = intval($matches[1]);
            $line_number = 1;

            foreach(explode("\n",htmlentities(str_replace($substr,"<b>$substr</b>",trim($sql)))) as $sql_line){
              $sql_lines[] =  ($line_number==$error_line ? "<div style='background-color:#9BE38D'>":"")."{$line_number}.\t{$sql_line}\n".($line_number==$error_line ? "</div>":"");
              $line_number++;
            }

            notify("<b>There was an error in creating the Datagrid</b><br /> $err_msg <hr />
            </td>
            <tr>
              <td colspan='2'><pre style='text-size:6px;font-family:courier;background-color:#ececec'>".implode($sql_lines)."</pre>",NOTIFY_DISPLAY_ERROR);
          }
        }
      }
    }
    else{
      $this->query($sql,false);
      $this->datagrid_page_records = $this->affected;
      if($this->affected>0){
        $this->free_result();
      }
      return $sql;
    }
  }
  /**
     * Builds a graphical datagrid from a result set that has already been
     * generated.
     *
     * @param array $rows
     * @param boolean $sortable
     * @param boolean $paginate
     * @return bool
     */
  public function create_grid_from_rows($rows, $sortable = true, $paginate = true){
    global $form;

    if ($form instanceof Form) {
      $USE_FORM = true;
    }

    $this->datagrid_sortable      = (intval($sortable)) ? true : false;
    $this->datagrid_paginate      = (intval($paginate)) ? true : false;
    $this->datagrid_total_records = count($rows);
    $query_string                 = strval($_SERVER['QUERY_STRING']);
    $dgsort                       = strval($_REQUEST['dgsort']);
    $dgdir                        = strval($_REQUEST['dgdir']);
    $dgpage                       = max(intval($_REQUEST['dgpage'])-1, 0);

    if ($this->datagrid_paginate!=false) {
      $offset                       = intval($dgpage * $this->datagrid_rpp);
      $length                       = intval($this->datagrid_rpp);
      $rows                         = array_slice($rows, $offset, $length, true);
    }

    /**
         * Build the links for auto sorting, if necessary
         */
    if ($this->datagrid_sortable!=false) {
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/", "", $query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/", "", $query_string);
      $clean_query_string = rtrim($query_string, "&");
      $query_string       = rtrim($query_string, "&") . "&";
      $sort_field         = (($sort_field=strval($this->datagrid_header_rows[$dgsort]['field_name']))!='') ? $sort_field : '';

      if (strlen($sort_field)) {
        $sorted   = array();
        $unsorted = $rows;
        $rows     = array();
        foreach ($unsorted as $index => $row) {
          $sorted[$index] = $row[$sort_field];
        }
        $sort_flags = (is_numeric(current($sorted)))
        ? SORT_NUMERIC
        : SORT_STRING;
        if ($dgdir!='d') {
          asort($sorted, $sort_flags);
        } else {
          arsort($sorted, $sort_flags);
        }
        foreach (array_keys($sorted) as $index) {
          $rows[$index] = $unsorted[$index];
        }
      }
    }

    $this->query_string = $query_string;
    $this->dgpage       = $dgpage;
    $this->datagrid_sql = null;

    $this->create_header();
    if (count($rows)==0) {
      echo "<tr><td colspan='100%' align='center'>There are no records to display</td></tr>";
    } else {
      $this->datagrid_page_records = count($rows);
      foreach ($rows as $row) {
        $this->create_row($row);
      }
    }
    if ($this->datagrid_paginate!=false) {
      $this->create_footer();
    }
    $this->create_icons();
    echo "</table>";
    return true;
  }

  ################################
  #SUPPORT FUNCTIONS FOR DATAGRID#
  ################################
  #1.0 Filters Support
  /**
     * Creates a nested realtionship structure that can be iterated and modified
     *
     * @param integer $parent_id The Registered position of the Parent Filter
     * @return mixed
     */
  public function find_child_filters($parent_id){
    $stack = null;
    $stack_copy = $this->relation_stack;
    asort($stack_copy);
    foreach($stack_copy as $key=>$value){
      if($value==$parent_id){
        unset($stack_copy[$key]);
        reset($stack_copy);
        $stack[$key] = $this->find_child_filters($key);
      }
    }
    return $stack;
  }
  /**
     * Loop through modfied array
     *
     * @param integer $parent_id The Parent Filter's registered position
     */
  public function modify_child_filters($parent_id=0,$stack=null){
    if($parent_id==0){
      $stack = $this->find_child_filters(0);
    }

    if($this->filters[$parent_id-1]){
      $parent_value = $this->filters[$parent_id-1]['filter']->value;
    } else {
      $parent_value = null;
    }

    $parent_filter = $this->filters[$parent_id-1];
    if(is_array($stack)){
      foreach($stack as $id=>$children){
        $filter        = &$this->filters[$id-1];
        if($parent_value!=null && $parent_value!="dgall"){
          if($filter['option_obj'] instanceof OptionsList && $parent_filter['option_obj'] instanceof OptionsList) {
            if($filter['filter']->filter_type!=DATAGRID_FILTER_DATE_RANGE){
              $options    = $filter['option_obj'];
              $field_name = $options->foreign_key_name==null ? $parent_filter['option_obj']->keys : $options->foreign_key_name;
              $split = explode(".",str_replace("`","",$field_name));
              $field_name = $split[count($split)-1];

              $filter_value = $parent_value;
              foreach(explode(",",$filter_value) as $fk){
                $field_value[] = "(FIND_IN_SET('$fk',$field_name))";
              }

              $where_clause = " ( $field_name IN ('$filter_value') OR ".implode(" OR ",$field_value).")";

              if(stristr($options->sql,"WHERE")){
                $sql = str_ireplace("WHERE","WHERE $where_clause AND",$options->sql);
              }
              elseif(stristr($options->sql,"ORDER BY")){
                $sql = str_ireplace("ORDER BY ","WHERE $where_clause ORDER BY",$options->sql);
              }
              else{
                $sql = $options->sql."WHERE $where_clause";
              }

              $options = $this->create_array_values($sql,$options->keys,$options->values);

              // If the number of filtered options is 0 or 1, then force that option to be selected
              // and get rid of the option to select mulitple.
              if(count($options)<2){
                $filter['filter_type'] = DATAGRID_FILTER_SINGLUAR;
                if(count($options)==0){
                  $options = array("dgall"=>"(No Applicable Options)");
                }
                $v = array_keys($options);
                $filter['filter']->dgall = false;
                $filter['filter']->value = $v[0];
              }
              else{
                $k = array_keys($options);
                $filter['filter']->dgall = false;

                // If the option that was seclected previous (such as dgall, etc)
                // is not in the new list of options, the make the
                // new value be the delimited list of all the values.
                if(!in_array($filter['filter']->value,$k)){
                  $new_value = implode(",",$k);
                  $filter['filter']->value            = $new_value;
                  $filter['filter']->allSelected      = true;
                  $filter['filter']->filteredOnParent = true;
                }
                $options = array("dgall"=>"All") + $options;
              }

              $filter['display_text'].=" (filtered)";
              $filter['options'] = $options;
            }
          }
        }

        //If there are any children, then make them do the same.
        if(is_array($children) ){
          $this->modify_child_filters($id,$children);
        }
      }
    }
  }
  /**
     * Reset the default value of $filter
     *
     * @param object $filter A DatagridFilter object
     * @param array $options An array of options
     * @param mixed $default The value that should be set as default
     */
  public function set_default_value(&$filter,$options,$default){
    if(is_integer($default)){
      $keys  = array_keys($options);
      $value = $keys[$default];
      $this->set_value($filter,$value);
    }else{
      $this->set_value($filter,$default);
    }
  }
  /**
     * Change the value of a filter and update its stored value to the session data.
     *
     * @param object $filter A DatagridFilter object
     * @param mixed $value
     */
  public function set_value(&$filter,$value){
    $filter->value = $value;
    $_SESSION['dgfilter'][$filter->db_hash]=$filter->value;
  }
  /**
     * Allows the textbox filter's fields to be set by either an array containing
     * column numbers, or a string containing the field names
     *
     * @param mixed $criteria
     */
  public function map_search_fields($criteria){
    $this->datagrid_searchbox_fields = $criteria;
  }
  /**
     * Removes the table dot syntax and cleans the back ticks if
     *
     * @param string $field_name
     * @return string
     */
  public function cleanFilterField($field_name){
    if($this->datagrid_search_condition_location==DATAGRID_SEARCH_CONDITION_HAVING){
      $split = explode(".",str_replace("`","",$field_name));
      $field_name = $split[count($split)-1];
    }
    return $field_name;
  }
  /**
     * Create filter objects from columns mapped in the grid
     * If use querystring value is set to true, then any values not mapped in the querystring
     * will be reset.
     *
     * @param boolean $use_querystring_value
     */
  public function load_filters($use_query_string_values){
    //Create the search textbox filter.
    //Setup the first search box to fields.
    if($this->datagrid_searchbox_fields == null){
      foreach($this->datagrid_header_rows as $id=>$header){
        if($header['field_name']!=''){
          $field_name = $this->cleanFilterField($header['field_name']);
          $field_names[] = $field_name;
          $columns_searchable[$id] = $header['field_display'];
        }
      }
    }
    else if(is_array($this->datagrid_searchbox_fields)) {
      foreach ($this->datagrid_searchbox_fields as $idOrFieldName) {
        if (is_int($idOrFieldName)) {
          if($this->datagrid_header_rows[$idOrFieldName]['field_name']!=''){
            if($this->datagrid_search_condition_location==DATAGRID_SEARCH_CONDITION_HAVING){
              $field_name = $this->datagrid_header_rows[$idOrFieldName]['field_name'];
              $split = explode(".",str_replace("`","",$field_name));
              $field_name = $split[count($split)-1];
            }
            else{
              $field_name = $this->datagrid_header_rows[$idOrFieldName]['field_name'];
            }

            $field_names[] = $field_name;
            $columns_searchable[$idOrFieldName] = $this->datagrid_header_rows[$idOrFieldName]['field_display'];
          }
        }
        else {
          if($this->datagrid_search_condition_location==DATAGRID_SEARCH_CONDITION_HAVING){
            $field_name = $idOrFieldName;
            $split = explode(".",str_replace("`","",$field_name));
            $field_name = $split[count($split)-1];
          }
          else{
            $field_name = $this->datagrid_header_rows[$idOrFieldName]['field_name'];
          }

          $field_names[] = $field_name;
          $fields_searchable[] = ucwords(str_replace("_"," ",$field_name));
        }
      }
    }
    else{
      $db_field = $this->datagrid_searchbox_fields;
    }

    $this->datagrid_searchable_columns = $columns_searchable;
    $this->datagrid_searchable_fields  = $fields_searchable;

    if(is_array($field_names)){
      $db_field = "LOWER(CAST(CONCAT_WS(' ',' ',".implode(",",$field_names).") AS CHAR))";
    }
    else{
      $this->display_searchbox = false;
    }

    if ($this->display_searchbox){
      $find_filter = new DatagridTextFilter($db_field,FILTER_COMPARITIVE_LIKE,FILTER_CONJUNCTION_AND);
      $find_filter->db_field_name = $db_field;
      $find_filter->setDbField($db_field);
      $find_filter->db_field_name=$db_field;

      //Added this in to remove trailing spaces from the conditions and remove duplicate spaces
      if(stristr($find_filter->value,"::AND::")){
        $to_clean = explode("::AND::",$find_filter->value);
      }else{
        $to_clean[] = $find_filter->value;
      }

      foreach($to_clean as $str){
        $str = html_entity_decode(str_replace("&nbsp;"," ",htmlentities($str)));
        $str = preg_replace("/\s+/"," ",$str);
        $cleaned[] = trim($str);
      }

      $find_filter->value = implode("::AND::",$cleaned);
      $find_filter_array = array("filter"=>$find_filter,"options"=>array("class"=>"searchbox","id"=>"dgsearch","size"=>$this->datagrid_searchbox_size),"display_text"=>ucwords($this->datagrid_searchbox_label),"filter_type"=>DATAGRID_FILTER_SINGLUAR);

      if(isset($_REQUEST['dgreset']) || $use_querystring_value==true){
        $find_filter->value = null;
        $_SESSION['dgfilter'][$find_filter->db_hash] = null;
      }

      if(is_array($this->filters)){
        arsort($this->filters);
        $this->filters[] = $find_filter_array;
        $this->filters = array_reverse($this->filters,true);
      }
      else{
        $this->filters = array();
        $this->filters[] = $find_filter_array;
      }
      $this->search_filter_criteria = $find_filter->value;
    }
  }

  #2.0 Datagrid Configuration Storage
  /**
     * Returns a unique hash of all the field_names used in the datagrid.
     * Used to create a hash value that is used to save the hash values.
     */
  public function fetch_grid_hash(){
    foreach($this->datagrid_header_rows as $row){
      if($row['field_name']!=''){
        $field_name_string.=$row['field_name'];
      }
    }
    return md5($field_name_string);
  }



  public function save_filter_defaults($values){
    $hash = $this->fetch_grid_hash();
    if($_SESSION['AdminId']!='') {
      $admin_id = $_SESSION['AdminId'];
      $old_values = $this->create_array_values("SELECT * FROM datagrid_filter_defaults WHERE admin_id='$admin_id' AND datagrid_hash='$hash'", "filter_hash","id");
      if(is_array($values)){
        foreach($values as $filter_hash=>$value){
          if(array_key_exists($filter_hash,$old_values)){
            $this->mk_sql(addslashes_array(array("id"=>$old_values[$filter_hash],"filter_value"=>$value)),"datagrid_filter_defaults","update","id",false);
          }
          else{
            $this->mk_sql(addslashes_array(array("filter_hash"=>$filter_hash,"datagrid_hash"=>$hash,"admin_id"=>$admin_id,"filter_value"=>$value)),"datagrid_filter_defaults","insert","id",false);
          }
        }
      }
      unset($_SESSION['defaults'][$hash]);
    }
  }

  public function fetch_filter_defaults(){
    $hash     = $this->fetch_grid_hash();
    if($_SESSION['AdminId']!=''){
      if($_SESSION['defaults'][$hash]){
        return $_SESSION['defaults'][$hash];
      }
      else{
        $admin_id = $_SESSION['AdminId'];
        $values = $this->create_array_values("SELECT * FROM datagrid_filter_defaults WHERE admin_id='$admin_id' AND datagrid_hash='$hash'", "filter_hash","filter_value");
        $_SESSION['defaults'][$hash] = $values;
        return $values;
      }
    }
    else{
      return false;
    }
  }

  /**
     * Get the hash values for the current datagrid loaded in the page
     *
     * @return array
     */
  public function fetch_grid_values(){
    $hash = $this->fetch_grid_hash();
    return $_SESSION['datagrids'][$hash];
  }
  /**
     * Save the values from the filters to the session data.
     *
     * @param array $values
     */
  public function save_grid_values($values){
    $hash=$this->fetch_grid_hash();
    if($hash!=''){
      $_SESSION['datagrids'][$hash]=$values;
    }
  }

  #3.0 Formatting and Output
  /**
 * Creates the grey box with filters above the datagrid
 *
 */
  public function build_filters_box() {
    global $form;
    $USE_FORM = $form instanceof Form ? true : false;
    $fd = $this->filter_defaults;

    if(is_array($this->filters)){
      $gv                     = $this->fetch_grid_values();
      $query_string           = $this->query_string;
      $query_string           = preg_replace("/dgdir\=[a-z]*\&?/"   ,"",$query_string);
      $query_string           = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
      $query_string           = preg_replace("/dgrpp\=[0-9]*\&?/"   ,"",$query_string);
      $query_string           = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $query_string           = preg_replace("/dgqsval\=[0-9]*\&?/" ,"",$query_string);



      if(is_numeric($gv['dgsort'])){
        $query_string      .= "&dgsort={$gv['dgsort']}&dgdir={$gv['dgdir']}";
      }

      $query_string          .= "&dgrpp={$gv['dgrpp']}";
      $query_string           = preg_replace("/\&+/","&",$query_string);

      if($this->datagrid_filters_form_method=='GET'){
        $values  = gpc_clean_array($_GET);//$_SERVER['QUERY_STRING'];
        if(is_array($values)){
          unset($values['dgdefault']);
          unset($values['dgfilter']);
          unset($values['dgrpp']);
          unset($values['dgsort']);
          unset($values['dgrest']);
          unset($values['dgreset']);
          unset($values['dgqsval']);
          unset($values['dgrss']);
          unset($values['dgprint']);
          unset($values['dgcsv']);
          unset($values['dghilite']);
          unset($values['dgfiltersubmit']);

          if(is_numeric($gv['dgsort'])){
            $values['dgsort'] = $gv['dgsort'];
            $values['dgdir']  = $gv['dgdir'];
          }
          $values['dgrpp'] = $gv['dgrpp'];

          $parts = array();
          foreach ($values as $parameter=>$value){
            $parts[] = $this->encodeQuerystringParameter($parameter,$value,true);
          }
          $hidden_boxes = implode($parts);
        }
      }
      else{
        $hidden_boxes = "";
      }


      $this->form_url         = $this->filters_form_url==null  ? $_SERVER['PHP_SELF']."?".$query_string : $this->filters_form_url;
      $this->form_height      = 35;

      //Determine if the "Custom Search" label will need to be displayed. If so, make the form taller so that it will fit.
      if(is_array($this->filters)){
        foreach($this->filters as $f){
          if(in_array($f['filter_type'], array(DATAGRID_FILTER_SINGLUAR,DATAGRID_FILTER_DATE_RANGE,DATAGRID_FILTER_IN_GROUP,DATAGRID_FILTER_MULTIPLE))){
            $this->form_height=50;
            break;
          }else if(count($f['options'])>2){
            $this->form_height=50;
            break;
          }
        }
      }

      echo "<div style='width:$this->datagrid_table_width;'>";
      echo "<form class='dg_filter_form' method='$this->datagrid_filters_form_method' action='$this->form_url' onsubmit='dgShowLoading();try{dgCloseDialogs();}catch(e){}' style='height:{$this->form_height}px;' id='dgsearchform'>";
      echo $hidden_boxes;
      echo "<span style='float:left;text-align:left;color:#4A4A4A;font-weight:bold;display:none;' id='dgloading'>".
      "Reloading Data ".(file_exists(DOC_ROOT."img".DIR_TKN."dgloading.gif")?"<br /><img src='/img/dgloading.gif' />":"").
      "</span>";

      echo "<table style='margin:5px;margin-top:0px;border-collapse:collapse;' align='right' border='0' cellspacing='0' >".
      "<tr valign='top' align='left'>";

      $temp_value = null;

      //Iterate through the filters and build a cell for each one
      foreach($this->filters as $f){
        $attributes = array('style'=>'padding:1px;');

        if ($f['filter']->dgall==true){ $f['filter']->value='dgall'; }
        if ($f['filter']->allSelected && $f['filter']->filteredOnParent) { $f['filter']->value = 'dgall';}


        //If the filter is a select box that allows multiple values to be picked, then it is a configurable filter
        if ($f['filter_type']!=DATAGRID_FILTER_SINGLUAR && ($f['filter'] instanceof DatagridListFilter) && count($f['options'])>1) {
          if(stristr($f['filter']->value,",")){
            $temp_value = $f['filter']->value;
            $f['filter']->value = "dgcustom";
          }
          else if($f['filter_type']!=DATAGRID_FILTER_DATE_RANGE){
            $temp_value = $f['filter']->value;
          }
          else{
            $temp_value = null;
          }

          if($f['option_obj'] instanceof OptionsNodeList ){
            $f['options']['dgcustom']=array("guid"=>"dgcustom","name"=>"Custom","level"=>1);
          }
          else{
            $f['options']['dgcustom']="Custom";
          }

          $attributes["onchange"] = "dgFilterChange(this,'{$f['filter']->db_hash}');";

          if (stristr($temp_value,",")) {
            $default_text = $this->custom_find_label." (applied)";//'custom search (applied)';
          }
          else {
            $default_text = $this->custom_find_label;//'custom search';
          }
          $showCustomLink = true;
        }
        else{
          $showCustomLink = false;
        }

        /**CREATE THE FILTERS CELL WITH LABEL**/
        echo "
<td style='padding-top:0px;position:relative;'>
  <span style='white-space:nowrap'>{$f['display_text']}</span><br />
  ".$f['filter']->createFilter($f['options'],$attributes,$f['option_obj'] instanceof OptionsNodeList);

        //If this is a filter with a custom link that needs to be displayed, then build the additional text.
        if($showCustomLink){
          echo "<a href='#' id='link_{$f['filter']->db_hash}' onclick='dgShowSelectionMenu(this,\"{$f['filter']->db_hash}\")' style='display:block;font-size:11px;text-decoration:none;white-space:nowrap;'>$default_text</a>";

          //The filter won't have the default value if it has been customized. In that case, build a hidden box to store the value.
          if ($default_text!=$this->custom_find_label) {
            echo "<input type='hidden' name='dgfilter[{$f['filter']->db_hash}]' id='temp_{$f['filter']->db_hash}' ".(DatagridListFilter::concat_attributes(array("value"=>$temp_value)))." />";
          }
        }
        echo "</td>";
        /**FILTER CELL FINISHED BEING CREATED**/

        unset($f['options']['dgcustom']);
        //SAVE OPTIONS TO ALLOW DEFAULT VALUE TO BE SET
        if($f['filter'] instanceof DatagridListFilter && count($f['options'])>1){
          $hasFiltersDefined = true;
          //discard the 'custom' option, but keep the 'All' option for now.
          $configurableFilters[] = array('hash'=>$f['filter']->db_hash,'options'=>$f['options'],'text'=>$f['display_text'],'isNodeList' => $f['option_obj'] instanceof OptionsNodeList);
          unset($f['options']['dgall']);
        }

        /**CREATE THE CUSTOM SELECTION BOX**/
        if ($f['filter_type']!=DATAGRID_FILTER_SINGLUAR && $f['filter'] instanceof DatagridListFilter) {
          $x = 0;

          if (count($f['options'])>0) {
            $attributes = array(
            "class" => "dgcustomselect",
            "id"    => "div_{$f['filter']->db_hash}",
            );

            if ($f['filter_type']==DATAGRID_FILTER_MULTIPLE || $f['filter_type'] == DATAGRID_FILTER_IN_GROUP || $f['filter_type']==DATAGRID_FILTER_NODE_LIST) {
              $selected_options = explode(",",$temp_value);
              //Iterate though filter's options. If the option itself is an array, then its an OPTGROUP,
              //so iterate through it
              $json = array();
              foreach ($f['options'] as $key=>$value) {
                if(!is_array($value)){
                  if($key!="dgall" && $key!="dgcustom"){
                    $value = htmlentities(str_replace(array("\n","\r"),"",$value),ENT_QUOTES);
                    $checked = in_array($key,$selected_options) ? "true" : "false";
                    $json[] = "{id:\"$key\",value:\"$value\",checked:$checked}";
                    $x++;
                  }
                }
                elseif(array_key_exists("guid",$value)){
                  $val = str_repeat('&nbsp;',($value['level']-1)*4).htmlentities(str_replace(array("\n","\r"),"",$value['name']),ENT_QUOTES);
                  $checked = in_array($key,$selected_options) ? "true" : "false";
                  $json[] = "{id:\"$key\",value:\"$val\",checked:$checked,level:\"{$value['class']}\"}";
                  $x++;
                }
                else{
                  foreach($value as $skey=>$svalue){
                    if($skey!="dgall" && $skey!="dgcustom"){
                      $svalue = htmlentities($svalue,ENT_QUOTES);
                      $checked = in_array($skey,$selected_options) ? "true" : "false";
                      $json[] = "{id:\"$skey\",value:\"$svalue\",checked:$checked}";
                      $x++;
                    }
                  }
                }
              }
              //End filter options
              $custom_div_text .= "<div ".DatagridListFilter::concat_attributes($attributes)."></div>";
              $json = "{hash:\"{$f['filter']->db_hash}\", elements:[".implode(",\n",$json)."],isNodeList:".($f['filter_type']==DATAGRID_FILTER_NODE_LIST?"true":"false")."}";
              $javascript_blocks .= "<script type='text/javascript'>dgBuildCheckBoxDiv({$json});</script>\n";
            }
            else if($f['filter_type']==DATAGRID_FILTER_DATE_RANGE){
              $split = explode(",",$temp_value);
              $custom_div_text .= "
<div ".DatagridListFilter::concat_attributes($attributes).">
  <div style='padding:10px;'>
    <span style='font-weight:bold;'>
    Enter a custom date range</span><br /><br />

    Start Date<br />
    <input type='text' size='2' value='{$split[0]}' />/<input type='text' size='2' value='{$split[1]}' />/<input type='text' size='4' value='{$split[2]}' /><br />
    <span style='color:gray;'>mm/dd/yyyy</span><br /><br />


    End Date<br />
    <input type='text' size='2' value='{$split[3]}' />/<input type='text' size='2' value='{$split[4]}' />/<input type='text' size='4' value='{$split[5]}' /><br />
    <span style='color:gray;'>mm/dd/yyyy</span><br />
    <a href='javascript:dgCloseSelectionMenu(\"{$f['filter']->db_hash}\")'>Save</a> |
    <a href='javascript:dgCancelSelection(\"{$f['filter']->db_hash}\");'>Cancel</a>
  </div>
</div>";
            }
            else if ($f['filter_type']==DATAGRID_FILTER_DATE_TIME){
              $split = explode(",",$temp_value);
              $custom_div_text .= "
<div ".DatagridListFilter::concat_attributes($attributes).">
  <div style='padding:10px;'>
    <span style='font-weight:bold;'>Enter a custom date range</span><br /><br />
    Start Date<br />
    <input type='text' size='2' value='{$split[0]}' />/ <input type='text' size='2' value='{$split[1]}' />/ <input type='text' size='4' value='{$split[2]}' />  <input type='text' size='2' value='{$split[3]}' /> <input type='text' size='2' value='{$split[4]}' />
    <br />
    <span style='color:gray;'>mm/dd/yyyy</span><br /><br />

    End Date<br />
    <input type='text' size='2' value='{$split[5]}' />/ <input type='text' size='2' value='{$split[6]}' />/ <input type='text' size='4' value='{$split[7]}' /><input type='text' size='2' value='{$split[7]}' /> <input type='text' size='2' value='{$split[8]}' />
    <br />
    <span style='color:gray;'>mm/dd/yyyy</span><br />

    <a href='javascript:dgCloseSelectionMenu(\"{$f['filter']->db_hash}\")'>Save</a> |
    <a href='javascript:dgCancelSelection(\"{$f['filter']->db_hash}\");'>Cancel</a>
  </div>
</div>";
            }
          }
          $f['filter']->value = $temp_value;
        }
      }
      /**FINISHED ITERATING THROUGH FILTERS***/



      if(is_array($this->filters_extra_cells)){
        foreach($this->filters_extra_cells as $cell){
          echo "<td style='padding-top:0px;position:relative;'>$cell</td>";
        }
      }

      //After the filters' cells have been created, then create the last two cells for submit buttons.
      if($this->display_highlight_checkbox){
        echo "<td style='padding-top:0px;white-space:nowrap'>
            Hi-Lite<br />
            <input type='checkbox' name='dghilite' ".($gv['dghilite'] ? "checked='checked'" : "")." />
            </td>";
      }

      //If there are filters that are defined and are able to have default values set, then create a div that will be used to

      if($hasFiltersDefined){
        $custom_div_text .= "<div id='filterDefaultsSelection' style='display:none;padding:5px;'>
        Here you can define the default values that the filters will use when the form is first loaded or is reset.<br />
        <table><tr>";
        foreach($configurableFilters as $filter){
          if($filter['isNodeList']){
            $custom_div_text .= "<td>{$filter['text']}<br />".$form->type_select("dgdefault[{$filter['hash']}]",($fd[$filter['hash']]!=''?$fd[$filter['hash']] : $filter['default']),$filter['options'])."</td>";
          }else{
            $custom_div_text .= "<td>{$filter['text']}<br />".$form->select("dgdefault[{$filter['hash']}]",($fd[$filter['hash']]!=''?$fd[$filter['hash']] : $filter['default']),$filter['options'])."</td>";
          }
        }

        $custom_div_text .= "</tr></table>
        <a href='javascript:dgSaveFiltersDefault();'>Save</a> |
        <a href='javascript:dgResetFiltersDefaultForm();'>Cancel</a>
        </div>";
      }
      echo "
<td valign='top' style='padding-top:12px;'>
  <span style='white-space:nowrap'>
    <input type=\"submit\" value=\"Apply\" class=\"button\" name='dgfiltersubmit' style='border:1px solid gray;' />&nbsp;
    <input type=\"submit\" value=\"Reset\" class=\"button\" name='dgreset' style='border:1px solid gray;' />
    ".($hasFiltersDefined ? "<a href='#' style='display:block;margin-top:4px;text-align:right;font-size:11px;text-decoration:none;' onclick='dgShowFiltersDefaultForm();'>Set Filter Defaults</a>" :"")."
  </span>
</td>
</tr>
</table>
</form>
            </div>
            <div id='dgSelectionDiv'>$custom_div_text</div>
            $javascript_blocks
            <script type='text/javascript'>
            $$('.dgcustomselect').each(function(div){\$(div).style.display='none';});
            ".($hasFiltersDefined?"saveDefaultFilterIndexes();":"")."
            </script>
            ";
    }
  }
  /**
 * Creates the grey box with filters above the datagrid
 *
 */
  public function build_printable_filters_box() {
    if(is_array($this->filters)){
      $gv                     = $this->fetch_grid_values();
      $query_string           = $this->query_string;
      $query_string           = preg_replace("/dgdir\=[a-z]*\&?/"   ,"",$query_string);
      $query_string           = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
      $query_string           = preg_replace("/dgrpp\=[0-9]*\&?/"   ,"",$query_string);
      $query_string           = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $query_string           = preg_replace("/dgqsval\=[0-9]*\&?/" ,"",$query_string);

      if(is_numeric($gv['dgsort'])){
        $query_string      .= "&dgsort={$gv['dgsort']}&dgdir={$gv['dgdir']}";
      }

      $query_string          .= "&dgrpp={$gv['dgrpp']}";
      $query_string           = preg_replace("/\&+/","&",$query_string);
      $this->form_url         = $this->filters_form_url==null  ? "?".$query_string : $this->filters_form_url;
      $this->form_height      = 35;
      $this->form_height=50;

      $temp_value = null;
      foreach($this->filters as $f){
        if($f['filter']->dgall==true || $f['filter']->value==null){
          continue;
        }

        if($f['filter']->allSelected && $f['filter']->filteredOnParent) { $f['filter']->value = 'dgall';}

        if($f['filter_type']!=DATAGRID_FILTER_SINGLUAR && ($f['filter'] instanceof DatagridListFilter)) {
          if(stristr($f['filter']->value,",")){
            $temp_value = $f['filter']->value;
            $f['filter']->value = "dgcustom";
          }
          else{
            $temp_value = null;
          }
        }else{
          $temp_value = null;
        }

        //Create a cell with a label and a filter
        if($temp_value==null){
          if($f['filter'] instanceof DatagridTextFilter ){
            $search_text  .=  "<li><b>Keywords</b>  \"<i>{$f['filter']->value}&nbsp;</i>\"</li>";
          }else{
            $narrowed_text .=  "<li><b>{$f['display_text']}</b>: \"<i>{$f['options'][$f['filter']->value]}&nbsp;</i>\"</li>";
          }
        }else{

          if($f['filter_type']==DATAGRID_FILTER_MULTIPLE || $f['filter_type'] == DATAGRID_FILTER_IN_GROUP){
            $selected_options = explode(",",$temp_value);
            foreach ($f['options'] as $key=>$value){
              if(!is_array($value)){
                if($key!="dgall" && $key!="dgcustom"){
                  if(in_array($key,$selected_options))
                  $custom_selected[] = $value;
                }
              }
              else{
                foreach($value as $skey=>$svalue){
                  if(in_array($skey,$selected_options))
                  $custom_selected[] = $svalue;
                }
              }
            }
            $narrowed_text .=  "<li><b>{$f['display_text']} </b>:<i>".implode("</i> and <i>",$custom_selected." ")."&nbsp;</i></li>";
          }
          else if($f['filter_type']==DATAGRID_FILTER_DATE_RANGE){
            $split = explode(",",$temp_value);
            $narrowed_text .=   "<li><b>{$f['display_text']} <b>: <i>".$split[0]."/".$split[1]."/".$split[2]." to ".$split[3]."/".$split[4]."/".$split[5]." </i>";
          }
        }
      }
    }


    if($search_text !='' || $narrowed_text!=''){
      $printable_filter_box =
      "
    <table style='margin-bottom:5px;font-family:arial;font-size:12px;width:$this->datagrid_table_width;border:1px solid #cecece' align='left' border='0' cellspacing='0' >
     <tr><td colspan='100%' style='padding:5px;background-color:#acacac;color:white;font-family:arial;font-weight:bold;font-size:14px;'>Search Criteria</td></tr>
    <tr><td><table><tr valign='top'>".
    ($search_text!=''?"
    <td style='padding:5px;padding-right:75px;'>
    <span style='font-weight:bold;font-size:14px;'>Original Search Criteria:</span>
    <ul>$search_text</ul>
    </td>":"").
    ($narrowed_text!=''?"
    <td style='padding:5px;'>
    <span style='font-weight:bold;font-size:14px;'>Narrowed Search Criteria:</span>
    <ul>$narrowed_text</ul>
    </td>":"")."
    </tr></table></td></tr>
    </table>";
    $this->printable_search_text = $search_text;
    $this->printable_narrowed_text = $narrowed_text;

    echo $printable_filter_box;
    }
  }

  /**
     * Create the header row for a datagrid with CSS styling
     *
     */
  public function create_header(){
    global $page;
    global $form;

    $USE_FORM = $form instanceof Form ? true : false;

    $gv       = $this->fetch_grid_values();
    $dgsort   = $gv['dgsort'];
    $dgdir    = $gv['dgdir'];
    $dghilite = $gv['dghilite'];


    $txt="
            <style type=\"text/css\">".preg_replace("/\s+/"," ","
            .entry-title {
            display:none;
            }

            .dg_tbl_title {
                background-color:{$this->datagrid_title_background};
                color:{$this->datagrid_title_color};
                font-family:{$this->datagrid_title_family};
                font-size:{$this->datagrid_title_size};
                font-weight:{$this->datagrid_title_weight};
            }
            .dg_tbl_header, .dgth {
                background-color:{$this->datagrid_header_background};
                color:{$this->datagrid_header_color};
                font-family:{$this->datagrid_header_family};
                font-size:{$this->datagrid_header_size};
                font-weight:{$this->datagrid_header_weight};
            }
            .dg_tbl_header a, .dg_tbl_header a:visited, .dg_tbl_header a:link,.dg_tbl_header a:active,
            .dgth a, .dgth a:visited, .dgth a:link,.dgth a:active {
                color:{$this->datagrid_header_color};
                text-decoration:none;
            }
            .dg_tbl_header a:hover {
                color:{$this->datagrid_header_color};
                text-decoration:underline;
            }
            .dg_tbl_row, .dgtr {
                background-color:{$this->datagrid_row_background};
                color:{$this->datagrid_row_color};
                font-family:{$this->datagrid_row_family};
                font-size:{$this->datagrid_row_size};
                font-weight:{$this->datagrid_row_weight};
            }
            .dg_tbl_header .pagination, .dgth .pagination{
                padding:2px;
            }
            .dg_tbl_header .pagination ul, .dgth .pagination ul{
                font-size:12px;
                margin:0;
                padding:0;
                text-align:left;
            }
            .dg_tbl_header .pagination li, .dgth .pagination li{
                display:inline;
                list-style-type:none;
                margin:2px;
                padding-bottom:1px;
            }
            .dg_tbl_header .pagination a, .dg_tbl_header .pagination a:visited,
            .dgth .pagination a, .dgth .pagination a:visited{
                border:1px solid $this->datagrid_title_background;
                color:$this->datagrid_title_background;
                padding:0 5px;
                text-decoration: none;
            }
            .dg_tbl_header .pagination a:hover, .dg_tbl_header .pagination a:active,
            .dgth .pagination a:hover, .dgth .pagination a:active{
                background-color:lightyellow;
                border:1px solid $this->datagrid_title_background;
                color:#000;
            }
            .dg_tbl_header .pagination li.currentpage,.dgth .pagination li.currentpage {
                background-color:$this->datagrid_title_background;
                border:1px solid navy;
                color:$this->datagrid_title_color;
                font-weight:bold;
                padding:0 5px;
            }
            .dg_tbl_header .pagination li.disablepage,.dgth .pagination li.disablepage {
                border:1px solid #929292;
                color:#929292;
                padding:0 5px;
            }
            .dg_tbl_header .pagination li.nextpage,.dgth .pagination li.nextpage{
                font-weight:bold;
            }
            .dg_tbl_header .toppagination,.dgth .toppagination{
                padding:2px;
            }
            .dg_tbl_header .toppagination ul,.dgth .toppagination ul{
                font-size:12px;
                margin:0;
                padding:0;
                text-align: left;
            }
            .dg_tbl_header .toppagination li,.dgth .toppagination li{
                display:inline;
                list-style-type:none;
                margin:2px;
                padding-bottom:1px;
            }
            .toppagination a, .toppagination a:visited, .toppagination a:active, .toppagination a:link{
                color:$this->datagrid_title_background;
                padding:0 3px;
                text-decoration:none;
            }
            .toppagination a:hover{
                color:#000;
            }
            * html .dg_tbl_header .pagination li.currentpage, * html .dg_tbl_header .pagination li.disablepage,
            * html .dgth .pagination li.currentpage, * html .dgth .pagination li.disablepage{
                /*IE 6 and below. Adjust non linked LIs slightly to account for bugs*/
                margin-right: 5px;
                padding-right: 0;
            }

            .dg_filter_form {
                background-color:#eee;
                border:1px solid gray;
                margin:5px;
                margin-bottom:10px;
                margin-top:3px;
                padding:5px;
                position:relative;
                text-align:right;
            }
            .cmenu {
                margin:0;
                padding:0.3em;
                list-style-type:none;
                background-color:#white;
            }
            .cmenu li a,.cmenu li a:visited,.cmenu li a:link,.cmenu li a:active{
                text-decoration:none;
                color:black;
                padding:3px;
                display:block;
                font-family:verdana;
                font-size:10px;
            }
            .cmenu li a:hover {
                background-color:#316ac5;
                color:white;
            }
            .cmenu hr {
                border: 0; border-bottom: 1px solid grey; margin: 3px 0px 3px 0px; width: 10em;
            }
            .cmenu .topSep {
                border-top: 1px solid gray; margin-top:5px;
            }
            </style>
            <script type='text/javascript'>
            var dgrowcolor  = '';
            var dgrowhilite = '$this->datagrid_row_highlight';
            </script>");

    if($page instanceof Page ){
      $page->content($txt,$this->header_location);
    }else{
      echo $txt;
    }

    $query_string           = $this->query_string;
    $query_string           = preg_replace("/dgdir\=[a-z]*\&?/"   ,"",$query_string);
    $query_string           = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
    $query_string           = preg_replace("/dgrpp\=[0-9]*\&?/"   ,"",$query_string);
    $query_string           = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
    $query_string           = preg_replace("/dgqsval\=[0-9]*\&?/" ,"",$query_string);

    if(is_numeric($gv['dgsort'])){
      $query_string      .= "&dgsort={$gv['dgsort']}&dgdir={$gv['dgdir']}";
    }

    $query_string          .= "&dgrpp={$gv['dgrpp']}";
    $query_string           = preg_replace("/\&+/","&",$query_string);

    if($this->datagrid_sortable===true){
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgpage\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/","" ,$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgqsval\=[0-9]*\&?/" ,"" ,$query_string);
      $clean_query_string = preg_replace("/&+/"                 ,"&",$query_string."&");
      $query_string       = preg_replace("/&+/"                 ,"&","{$query_string}");
    }

    $last_column = $this->datagrid_header_rows[count($this->datagrid_header_rows)-1];

    echo "
<div id='dgContextMenu' style='border: 1px solid gray;display: none; background-color:white; position: absolute;z-index:100;'><ul class='cmenu' id='cmenuitems'></ul></div>
<script type='text/javascript'>
_dg_custom_find_label = \"$this->custom_find_label\";
_dg_display_rss       = \"$this->display_rss\";
_dg_display_print     = \"$this->display_print\";
_dg_display_csv       = \"$this->display_csv\";
_dg_has_actions       = ".($last_column['field_name']=='' && ($last_column['field_display']=='Actions'||$last_column['field_display']=='Action') ?"true":"false")."
_dg_query_string      = \"$query_string\";
_dg_rss_querystring   = \"$this->rss_query_string\";

InitContext();
</script>";

    if($this->display_filters){
      $this->build_filters_box();
    }

    echo "
    <table class='.gg_table' border='".($this->datagrid_table_border==true?"1":"0")."' cellspacing='{$this->datagrid_table_spacing}' cellpadding='{$this->datagrid_table_padding}' ".($this->datagrid_table_width!=''?"width='{$this->datagrid_table_width}' ":"")."style='border-collapse:collapse;{$this->datagrid_table_style}' bordercolor='#cccccc' bordercolorlight='#cccccc' bordercolordark='#cccccc'>";
    if($this->datagrid_title_text!=null){
      echo "\t\t<tr><td class='dg_tbl_title' colspan='100%'>{$this->datagrid_title_text}</td></tr>";
    }
    if ($this->datagrid_top_paginate) {
      $this->create_top_pagination();
    }

    $query_string = $this->query_string;// $_SERVER['QUERY_STRING'];

    /*Build the links for auto sorting, if necessary*/
    if($this->datagrid_sortable===true){
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/","",$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $clean_query_string = preg_replace("/&+/","&",$query_string."&");
      $query_string       = preg_replace("/&+/","&","$query_string");
    }

    $this->datagrid_icons = $this->reformat_icons();

    echo "\t\t<tr style='background-color:{$this->datagrid_header_background}'>\n";

    //Create the columns for the datagrid
    foreach($this->datagrid_header_rows as $id=>$data) {
      if($colsToSkip>0){
        $colsToSkip--;
        continue;
      }
      if (!is_array($data)) {
        $data['field_display'] = $data;
      }

      if ($this->datagrid_sortable) {

        if($dgdir=='a' && $id==$dgsort){
          $sort_direction = "d";
        }
        else if($data['sort_reverse']==true && $id!=$dgsort){
          $sort_direction = "d";

        }
        else if ($this->datagrid_reverse_sort_non_text && $id!=$dgsort && $this->resultset_fields[$data['field_name']]!=='text'){
          $sort_direction = "d";
        }else {
          $sort_direction = "a";
        }

        $query_string = $clean_query_string."dgrest=1&dgsort=$id&dgdir=$sort_direction";
      }
      echo "\n<td ";

      $i=1;
      while(is_array($this->datagrid_header_rows[($id+$i)])){
        if($this->datagrid_header_rows[$id+$i]['field_display']==''){
          $i++;
        }
        else{
          break;
        }
      }

      if($i>1){
        echo "colspan='".($i)."' ";
        $colsToSkip = $i-1;
      }

      if(($this->display_filters==true || $this->filters_originally_displayed==true) && !isset($_REQUEST['dgprint']) && $data['field_name']!='') {
        echo " onmousedown='SCC(\"h\",this);' rel='ac' ";
      }

      echo "id='dgcol_$id' class='dgth'".($data['field_header_style']!=''?" style='{$data['field_header_style']}' ":($data['field_style']!='' && $data['field_exclude_header']!=true?" style='{$data['field_style']}'":"")).">\n\t";

      //If the field is allowed to be sortable, and it contains a field value.
      if($data['field_allow_sort']!==false && $data['field_name']!='' && $this->datagrid_sortable===true){
        $aHref = $query_string;
        $aDisp = "";

        if ((int)$id===(int)$dgsort) {
          if ($dgdir=='d') {
            $aDisp="&darr;";
          }
          elseif($dgdir=='a'){
            $aDisp="&uarr;";
          }
        }

        $aDisp .= $data['field_display'];
        echo "<a href='?$aHref' ".(($this->display_filters==true || $this->filters_originally_displayed==true) && $data['field_name']!='' && !isset($_REQUEST['dgprint'])?" onmousedown='SCC(\"h\",this);' rel='ac' ":"").">$aDisp</a>";
      }
      else{
        echo $data['field_display'];
      }
      echo "\n</td>";
    }
    echo "</tr>\n";
  }
  /**
    * Creates a printer friendly version of the header
    *
    */
  public function create_printable_header(){
    global $page;
    global $form;

    $USE_FORM = $form instanceof Form ? true : false;

    $gv       = $this->fetch_grid_values();
    $dgsort   = $gv['dgsort'];
    $dgdir    = $gv['dgdir'];
    $dghilite = $gv['dghilite'];


    $txt="
            <style type=\"text/css\">".preg_replace("/\s+/"," ","
            .dg_tbl_title {
                background-color:{$this->datagrid_title_background};
                color:{$this->datagrid_title_color};
                font-family:{$this->datagrid_title_family};
                font-size:{$this->datagrid_title_size};
                font-weight:{$this->datagrid_title_weight};
            }
            .dg_tbl_header, .dgth {
                background-color:{$this->datagrid_header_background};
                color:{$this->datagrid_header_color};
                font-family:{$this->datagrid_header_family};
                font-size:{$this->datagrid_header_size};
                font-weight:{$this->datagrid_header_weight};
            }
            .dg_tbl_header a, .dg_tbl_header a:visited, .dg_tbl_header a:link,.dg_tbl_header a:active,
            .dgth a, .dgth a:visited, .dgth a:link,.dgth a:active {
                color:{$this->datagrid_header_color};
                text-decoration:none;
            }
            .dg_tbl_header a:hover {
                color:{$this->datagrid_header_color};
                text-decoration:underline;
            }
            .dg_tbl_row, .dgtr {
                background-color:{$this->datagrid_row_background};
                color:{$this->datagrid_row_color};
                font-family:{$this->datagrid_row_family};
                font-size:{$this->datagrid_row_size};
                font-weight:{$this->datagrid_row_weight};
            }
            .dg_tbl_header .pagination, .dgth .pagination{
                padding:2px;
            }
            .dg_tbl_header .pagination ul, .dgth .pagination ul{
                font-size:12px;
                margin:0;
                padding:0;
                text-align:left;
            }
            .dg_tbl_header .pagination li, .dgth .pagination li{
                display:inline;
                list-style-type:none;
                margin:2px;
                padding-bottom:1px;
            }
            .dg_tbl_header .pagination a, .dg_tbl_header .pagination a:visited,
            .dgth .pagination a, .dgth .pagination a:visited{
                border:1px solid $this->datagrid_title_background;
                color:$this->datagrid_title_background;
                padding:0 5px;
                text-decoration: none;
            }
            .dg_tbl_header .pagination a:hover, .dg_tbl_header .pagination a:active,
            .dgth .pagination a:hover, .dgth .pagination a:active{
                background-color:lightyellow;
                border:1px solid $this->datagrid_title_background;
                color:#000;
            }
            .dg_tbl_header .pagination li.currentpage,.dgth .pagination li.currentpage {
                background-color:$this->datagrid_title_background;
                border:1px solid black;
                color:$this->datagrid_title_color;
                font-weight:bold;
                padding:0 5px;
            }
            .dg_tbl_header .pagination li.disablepage,.dgth .pagination li.disablepage {
                border:1px solid #929292;
                color:#929292;
                padding:0 5px;
            }
            .dg_tbl_header .pagination li.nextpage,.dgth .pagination li.nextpage{
                font-weight:bold;
            }
            .dg_tbl_header .toppagination,.dgth .toppagination{
                padding:2px;
            }
            .dg_tbl_header .toppagination ul,.dgth .toppagination ul{
                font-size:12px;
                margin:0;
                padding:0;
                text-align: left;
            }
            .dg_tbl_header .toppagination li,.dgth .toppagination li{
                display:inline;
                list-style-type:none;
                margin:2px;
                padding-bottom:1px;
            }
            .toppagination a, .toppagination a:visited, .toppagination a:active, .toppagination a:link{
                color:$this->datagrid_title_background;
                padding:0 3px;
                text-decoration:none;
            }
            .toppagination a:hover{
                color:#000;
            }
            * html .dg_tbl_header .pagination li.currentpage, * html .dg_tbl_header .pagination li.disablepage,
            * html .dgth .pagination li.currentpage, * html .dgth .pagination li.disablepage{
                /*IE 6 and below. Adjust non linked LIs slightly to account for bugs*/
                margin-right: 5px;
                padding-right: 0;
            }

            .dg_filter_form {
                background-color:#eee;
                border:1px solid gray;
                margin:5px;
                margin-bottom:10px;
                margin-top:3px;
                padding:5px;
                position:relative;
                text-align:right;
            }</style>");

    if($page instanceof Page ){
      $page->content($txt,$this->header_location);
    }else{
      echo $txt;
    }

    $query_string           = $this->query_string;
    $query_string           = preg_replace("/dgdir\=[a-z]*\&?/"   ,"",$query_string);
    $query_string           = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
    $query_string           = preg_replace("/dgrpp\=[0-9]*\&?/"   ,"",$query_string);
    $query_string           = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
    $query_string           = preg_replace("/dgqsval\=[0-9]*\&?/" ,"",$query_string);

    if(is_numeric($gv['dgsort'])){
      $query_string      .= "&dgsort={$gv['dgsort']}&dgdir={$gv['dgdir']}";
    }

    $query_string          .= "&dgrpp={$gv['dgrpp']}";
    $query_string           = preg_replace("/\&+/","&",$query_string);

    if($this->datagrid_sortable===true){
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgpage\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/","" ,$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"" ,$query_string);
      $query_string       = preg_replace("/dgqsval\=[0-9]*\&?/" ,"" ,$query_string);
      $clean_query_string = preg_replace("/&+/"                 ,"&",$query_string."&");
      $query_string       = preg_replace("/&+/"                 ,"&","$query_string");
    }

    $last_column = $this->datagrid_header_rows[count($this->datagrid_header_rows)-1];

    if($this->display_filters || $this->filters_originally_displayed){
      $this->build_printable_filters_box();
    }
    echo "<!--BEGIN WEB SLICE CONTENT -->";
    echo "<div class='entry-content'>";
    echo "<table class='.gg_table' border='".($this->datagrid_table_border==true?"1":"0")."' cellspacing='{$this->datagrid_table_spacing}' cellpadding='{$this->datagrid_table_padding}' ".($this->datagrid_table_width!=''?"width='{$this->datagrid_table_width}' ":"")."style='border-collapse:collapse;{$this->datagrid_table_style}' bordercolor='#cccccc' bordercolorlight='#cccccc' bordercolordark='#cccccc'>";
    if ($this->datagrid_top_paginate) {
      $this->create_top_pagination();
    }
    if($this->datagrid_title_text!=null){
      echo "<tr><td class='dg_tbl_title' colspan='100%'>{$this->datagrid_title_text}</td></tr>";
    }

    $query_string = $_SERVER['QUERY_STRING'];

    /*Build the links for auto sorting, if necessary*/
    if($this->datagrid_sortable===true){
      $query_string       = preg_replace("/dgsort\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgpage\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgdir\=[a-zA-Z]*\&?/","",$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $query_string       = preg_replace("/dgrest\=[0-9]*\&?/"  ,"",$query_string);
      $clean_query_string = preg_replace("/&+/","&",$query_string."&");
      $query_string       = preg_replace("/&+/","&","$query_string");
    }

    $this->datagrid_icons = $this->reformat_icons();

    echo "<tr style='background-color:{$this->datagrid_header_background}'>";

    //Create the columns for the datagrid
    foreach($this->datagrid_header_rows as $id=>$data) {

      if($colsToSkip>0){
        $colsToSkip--;
        continue;
      }

      if (!is_array($data)) {
        $data['field_display'] = $data;
      }

      if ($this->datagrid_sortable) {
        $query_string = $clean_query_string."dgrest=1&dgsort=$id&dgdir=".($dgdir=='a' && $id==$dgsort ? 'd' : 'a');
      }
      echo "<td ";

      $i=1;
      while(is_array($this->datagrid_header_rows[($id+$i)])){
        if($this->datagrid_header_rows[$id+$i]['field_display']==''){
          $i++;
        }
        else{
          break;
        }
      }

      if($i>1){
        echo "colspan='".($i)."' ";
        $colsToSkip = $i-1;
      }

      echo (($this->display_filters==true || $this->filters_originally_displayed==true) && $data['field_name']!='' && !isset($_REQUEST['dgprint'])?" onmousedown='SCC(\"h\",this);' rel='ac' ":"")."id='dgcol_$id' class='dgth'".($data['field_header_style']!=''?" style='{$data['field_header_style']}' ":($data['field_style']!='' && $data['field_exclude_header']!=true?" style='{$data['field_style']}'":"")).">";

      //If the field is allowed to be sortable, and it contains a field value.
      if($data['field_allow_sort']!==false && $data['field_name']!='' && $this->datagrid_sortable===true){
        $aHref = $query_string;
        $aDisp = "";

        if ((int)$id===(int)$dgsort) {
          if ($dgdir=='d') {
            $aDisp="&darr;";
          }
          elseif($dgdir=='a'){
            $aDisp="&uarr;";
          }
        }

        $aDisp .= $data['field_display'];
        echo "<a href='#'>$aDisp</a>";
      }
      else{
        echo $data['field_display'];
      }
      echo "</td>";
    }
    echo "</tr>";
  }


  /**
    * Create a row in a datagrid table
    *
    * @param $r array
    **/
  public function create_row($r){
    $cols_to_skip = 0;
    $cols_skipped = 0;
    $already_skipped = false;
    $gv  = $this->fetch_grid_values();

    //If the icons have not been placed into the arrays and there are icons registered
    if($this->datagrid_formatted_icons==false && $this->datagrid_icons!=false){
      $this->reformat_icons();
    }

    echo "\n<tr ".($this->datagrid_highlight_rows==true?"onmouseover='dghr(this)' onmouseout='dguhr(this)'":"")." style=\"background-color:".($this->count%2==0?$this->datagrid_row_background_1:$this->datagrid_row_background_2).";".
    ($this->count%2==0?$this->datagrid_row_style_1:$this->datagrid_row_style_2)."\" id='dr_{$this->datagrid_row_number}'".($this->datagrid_row_valign<>''?" valign='{$this->datagrid_row_valign}'":"").">";

    foreach($this->datagrid_header_rows as $id=>$data){
      //This was added mainly for support for the Live Editor, which allows certain rows to have a colspan and other rows to not.
      if ($cols_skipped < $cols_to_skip && $cols_to_skip > 0) {
        $cols_skipped++;
      }
      else {
        $fields     = explode(".",str_replace("`","",$data['field_name']));
        $field_name = $fields[count($fields)-1];
        $value      = $r[$field_name];


        $td_attr    = array(
        "class" => "dgtr",
        "style" => $data['field_style']!='' ? $data['field_style'] : "",
        );

        if($r['colspan']!='' && $already_skipped==false){
          $td_attr["colspan"] = $r['colspan'];
        }

        if(($this->display_filters==true || $this->filters_originally_displayed==true) && !isset($_REQUEST['dgprint'])){
          $td_attr["onmousedown"] = "SCC(\"cell\",this)";
          $td_attr["rel"]         = 'ac';
        }


        if($data['cell_extra']!='' && is_array($data['cell_extra'])){
          foreach($data['cell_extra'] as $attribute=>$attr_value){
            preg_match_all('/\[\![a-zA-Z0-9_]*\!\]/',$attr_value,$matches);
            if(is_array($matches)){
              $searches = null;
              $replace  = null;
              foreach($matches[0] as $section){
                $sct        = preg_replace("/[^a-zA-Z0-9_]*/","",$section);
                $searches[] = $section;
                $replace[]  = $r[$sct];
              }
            }
            $rep_str = str_replace($searches, $replace, $attr_value);
            if(trim($rep_str!='')){
              $td_attr[$attribute] = $rep_str;
            }
          }
        }

        echo "\n\t<td ".DatagridListFilter::concat_attributes($td_attr).">\n\t\t";

        if($r['colspan']!='' && $cols_skipped<$r['colspan']){
          $cols_to_skip    = $r['colspan'];
          $cols_skipped    = 1;
          $already_skipped = true;
        }

        //If a custom display was to be used for the field, then show it, otherwise, show the field's value.
        if($data['field_custom']!='') {
          preg_match_all('/\[\![a-zA-Z0-9_]*\!\]/',$data['field_custom'],$matches);
          if(is_array($matches)){
            $searches=null;
            $replace=null;
            foreach($matches[0] as $section){
              $sct        = preg_replace("/[^a-zA-Z0-9_]*/","",$section);
              $searches[] = $section;
              $replace[]  = $r[$sct];
            }
          }

          $href = str_replace($searches,$replace,$data['field_custom']);

          /**
           *Replace any icons that match the cell's contents
           **/
          preg_match_all('/\{\![a-zA-Z0-9_]*\!\}/',$data['field_custom'],$icons);
          if(is_array($icons)){
            $searches=null;
            $replace=null;
            foreach($icons[0] as $icon){
              $sct=preg_replace("/[^a-zA-Z0-9_]*/","",$icon);
              $searches[]=$icon;

              //Find any tokens that were embedded as part of the $icon_href argument, replace the tokens with the record's value
              preg_match_all('/\[\![a-zA-Z0-9_]*\!\]/',$this->datagrid_formatted_icons[$sct]['icon_href'],$icon_matches);
              if(is_array($icon_matches)){
                foreach($icon_matches[0] as $icon_section){
                  $s=preg_replace("/[^a-zA-Z0-9_]*/","",$icon_section);
                  $icon_searches[]=$icon_section;
                  $icon_replace[]=$r[$s];
                }
              }
              $icon_href=str_replace($icon_searches,$icon_replace,$this->datagrid_formatted_icons[$sct]['icon_href']);

              if($this->datagrid_formatted_icons[$sct]['icon_is_delete']==true){
                $icon_href = 'javascript:if(confirm("Are you sure you want to delete this record?\nThis can not be undone.")){window.location="'.$icon_href.'";}';
              }
              preg_match_all('/\[\![a-zA-Z0-9_]*\!\]/',$this->datagrid_formatted_icons[$sct]['icon_display'],$icon_title_matches);
              if(is_array($icon_title_matches)){
                foreach($icon_title_matches[0] as $icon_section){
                  $s=preg_replace("/[^a-zA-Z0-9_]*/","",$icon_section);
                  $icon_title_searches[]=$icon_section;
                  $icon_title_replace[]=$r[$s];
                }
              }
              $title = str_replace("'","#&39;",str_replace($icon_title_searches,$icon_title_replace,$this->datagrid_formatted_icons[$sct]['icon_display']));
              $new_page = $this->datagrid_formatted_icons[$sct]['icon_new_page'];

              $replace[]="\n<a ".DatagridListFilter::concat_attributes(array("href"=>$icon_href,"target"=>($new_page?"_blank":"_self")))." style='margin:1px;margin-right:0px;' title='{$title}'><img src='{$this->datagrid_formatted_icons[$sct]['icon_src']}' alt='{$title}' style='border:0' /></a>";
            }
          }

          if($data['field_nowrap']){
            $href = str_replace($searches,$replace,($data['field_eval']==true?"?><span style='white-space:nowrap'><?":"<span style='white-space:nowrap'>").$href.($data['field_eval']==true?"?></span><?":"</span>"));
          }
          else{
            $href = str_replace($searches,$replace,$href);
          }

          if($data['field_eval']==true){
            $href=eval($href);
          }
          else{
            echo $href;
          }
        }
        else{
          //This now allows a column that has a mapped value of `table`.`field` to only display the
          //field section.
          $fields     = explode(".",str_replace("`","",$data['field_name']));
          $field_name = $fields[count($fields)-1];
          $value      = $r[$field_name];

          //Check if there was any search text entered in the box. If there was
          //create highlight blocks so the user knows what matched exactly.

          if($this->search_filter_criteria!='' && $gv['dghilite']==true){
            if(stristr($this->search_filter_criteria,"::AND::")){
              $strings = explode("::AND::",$this->search_filter_criteria);
            }
            else{
              $strings[] = $this->search_filter_criteria;
            }
            foreach($strings as $search_txt){
              $search_txt = trim($search_txt);
              $pos = stripos($value,$search_txt);
              if($pos!==false){
                $value = substr($value,0,$pos)."<span style=\"background:yellow;\" rel=\"ac\">".substr($value,$pos,strlen($search_txt))."</span>".substr($value,$pos+strlen($search_txt));
              }
            }
          }
          echo $value==''?"&nbsp;":$value;
          //echo $r[$field_name];
        }
        echo "</td>";
      }
    }
    echo "</tr>";
    $this->count++;
  }

  /**
     * Removes all datagrid variables from the querystring leaving only non-dg variables intact
     *
     */
  public function clean_query_string($query_string = null){
    $dgpage       = $this->dgpage;
    $gv           = $this->fetch_grid_values();

    $query_string  = gpc_clean_array($_GET);//$_SERVER['QUERY_STRING'];
    if(is_array($query_string)){
      unset($query_string['dgdefault']);
      unset($query_string['dgdir']);
      unset($query_string['dgfiltersubmit']);
      unset($query_string['dgfilter']);
      unset($query_string['dghilite']);
      unset($query_string['dgpage']);
      unset($query_string['dgprint']);
      unset($query_string['dgqsval']);
      unset($query_string['dgrest']);
      unset($query_string['dgreset']);
      unset($query_string['dgrpp']);
      unset($query_string['dgsort']);
      unset($query_string['dgrss']);
      unset($query_string['dgcsv']);
      unset($query_string['search']);

      if($gv['dgsort']!=''){
        $query_string['dgdir']  = ($gv['dgdir']=='d'?"d":"a");
        $query_string['dgsort'] = $gv['dgsort'];
      }

      $parts = array();
      foreach ($query_string as $parameter=>$value){
        $parts[] = $this->encodeQuerystringParameter($parameter,$value,false);
      }

      //Except for the dgdirection and sort column, the rest are blank
      $query_string = implode("&",$parts);
    }
    return $query_string;
  }


  /**
     * Create the bottom section of the datagrid - used for displaying pagination
     *
     */
  public function create_footer(){
    global $form;
    $USE_FORM           = $form instanceof Form ? true : false;
    $query_string       = $this->clean_query_string();
    $clean_query_string = $this->clean_query_string();

    $gv = $this->fetch_grid_values();

    $dgpage = $gv['dgpage'];

    echo "
		<tr style='background-color:{$this->datagrid_header_background}'>
		<td class='dgth' colspan='100%' style='font-weight:normal;font-size:$this->datagrid_row_size;'>
		<table style=\"width:100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size:$this->datagrid_row_size\">
		<tr>
		<td style='width:25%'>
		";

    if($this->datagrid_total_records>0){
      $current_page	=	$dgpage+1;
      $total_page		=	ceil($this->datagrid_total_records/$this->datagrid_rpp);
      $start			=	$dgpage*$this->datagrid_rpp+1;
      $end			=	$start+$this->datagrid_page_records-1;
      echo "<ul style='margin:0;display:inline;'><li style='padding-right:100px;display:inline;color:$this->datagrid_header_color'><span style='white-space:nowrap'>$start - $end of $this->datagrid_total_records</span></li></ul>";
      echo "</td>";
      echo "<td style='width:50%'><div class='pagination' style='text-align:center;'><ul>";

      if($current_page==1){
        echo "<li class='disablepage'>&#171;First</li>";
      }else{
        $query_string = $clean_query_string."&dgpage=1";
        echo "<li class='nextpage'>";
        if($USE_FORM){
          echo $form->a("?$query_string","&#171;First");
        }else{
          echo "<a href='?$query_string'>&#171;First</a>";
        }
        echo "</li>";
      }

      if($current_page==1){
        echo "<li class='disablepage'>&#171;Previous</li>";
      }else{
        $query_string = $clean_query_string."&dgpage=".($current_page-1);
        echo "<li class='nextpage'>";
        if($USE_FORM){
          echo $form->a("?".str_replace("&amp;","&",$query_string),"&#171;Previous");
        }else{
          echo "<a href='?$query_string'>&#171;Previous</a>";
        }
        echo "</li>";
      }

      if($current_page<5){
        $start_num=1;
      }elseif($total_page-$current_page<=0){
        $start_num=$current_page-4;
      }else{
        $start_num=$current_page-3;
      }

      for($i=$start_num;$i<$start_num+7;$i++){
        if($i<=$total_page){
          if($i==$current_page){
            echo "<li class='currentpage'>$i</li>";
          }else{
            $query_string = $clean_query_string."&dgpage=$i";
            echo "<li>";
            if($USE_FORM){
              echo $form->a("?$query_string",$i);
            }else{
              echo "<a href='?$query_string'>$i</a>";
            }
            echo "</li>";
          }
        }else{
          break;

        }
      }

      $start_last_page=$total_page-2;

      /**
             * If the last page number shown is higher than the supposed iteration of the last page numbers
             * then set the start of the last page numbers to the next higher page number.
             *
             * if there are 20 pages total, and the iteration will start at 17, and the current page is 18
             * then make the iteration start at 19
             *
             */
      if($start_last_page<$i){
        $start_last_page=$i;
      }

      /**
             * If the number of pages in the iteration is less than 3, then show last number of pages remaining
             */
      if(($total_page-2) < $start_last_page){
        $start_last_page=$total_page-($total_page-$start_last_page);
      }

      if($start_last_page>$i){
        echo "...";
        for($i=$start_last_page;$i<=$total_page;$i++){
          $query_string=$clean_query_string.($clean_query_string!=""?"&":"")."dgpage=$i";
          echo "<li>";
          if($USE_FORM){
            echo $form->a("?$query_string",$i);
          }else{
            echo "<a href='?$query_string'>$i</a>";
          }
          echo "</li>";
        }
      }
      if($current_page==$total_page){
        echo "<li class='disablepage'>Next&#187;</li>";
      }else{
        $query_string=$clean_query_string.($clean_query_string!=""?"&":"")."dgpage=".($current_page+1);
        echo "<li class='nextpage'>";
        if($USE_FORM){
          echo $form->a("?$query_string","Next&#187;");
        }else{
          echo "<a href='?$query_string'>Next&#187</a>";
        }
        #<a href='?$query_string'>Next&#187;</a>
        echo "</li>";
      }

      if($current_page==$total_page){
        echo "<li class='disablepage'>Last&#187;</li>";
      }else{
        $query_string=$clean_query_string.($clean_query_string!=""?"&":"")."dgpage=".($total_page);
        echo "<li class='nextpage'>";
        if($USE_FORM){
          echo $form->a("?$query_string","Last&#187;");
        }else{
          echo "<a href='?$query_string'>Last&#187;</a>";
        }

        #<a href='?$query_string'>Last&#187;</a>
        echo "</li>";
      }
      echo "</ul></div></td>";
      echo "<td style='width:25%' align='right'>";
      if(!$USE_FORM){
        echo "<span style='white-space: nowrap'>$this->datagrid_rpp records per page</span>";
      }
      else{
        $gv = $this->fetch_grid_values();
        $query_string = $this->query_string;
        echo "<span style='white-space: nowrap'>Show ".
        $form->select("dgrpp",
        $this->datagrid_rpp,
        array(20=>20,50=>50,100=>100,250=>250,500=>500,1000=>1000,2000=>2000),
        array("style"=>"font-size:$this->datagrid_row_size;",
        "nodojo"=>"nodojo",
        "onchange"=>"window.location='?{$query_string}&dgrpp='+this.value"))." per page</span>";
      }
      $this->csv_query_string = $query_string;
    }else{
      echo "No Records";
      $this->csv_query_string = $query_string;
    }
    echo "</td></tr>";
    echo "</table></td></tr>";
  }

  /**
     * Create the bottom section of the datagrid - used for displaying pagination
     *
     */
  public function create_top_pagination(){
    global $form;
    $USE_FORM           = $form instanceof Form ? true : false;
    $query_string       = $this->clean_query_string();
    $clean_query_string = $this->clean_query_string();
    $gv                 = $this->fetch_grid_values();
    $dgpage             = $gv['dgpage'];

    echo "\t\t<tr style='background-color:{$this->datagrid_header_background}'>\n";
    echo "\t\t\t<td class='dgth' colspan='100%' style='font-weight:normal;font-size:$this->datagrid_row_size;'>\n";
    echo "\t\t\t\t<table style=\"width:100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size:$this->datagrid_row_size\">\n";
    echo "\t\t\t\t\t<tr>\n<td style=\"width:215px\" valign=\"bottom\">\n\n";

    if($this->datagrid_total_records>0) {
      $current_page	=	$dgpage+1;
      $total_page		=	ceil($this->datagrid_total_records/$this->datagrid_rpp);
      $start			=	$dgpage*$this->datagrid_rpp+1;
      $end			=	$start+$this->datagrid_page_records-1;

      echo "<div style=\"color:$this->datagrid_header_color\">\n";
      echo "<span style='white-space:nowrap'><span style='font-weight:bold;'>$this->datagrid_total_records</span> records &nbsp;&nbsp;&nbsp; </span>";
      echo "<form method=\"post\" action=\"?$query_string\" style=\"display:inline;padding:0;margin:0;\">";

      //Create the previous/first links
      if($current_page!=1){
        $query_string = $clean_query_string.($clean_query_string!=""?"&":"")."dgpage=1";
        echo "<a href=\"?$query_string\" title=\"Go To First Page\" style=\"font-size:12px;\">&#171;&nbsp;</a>";
        $query_string = $clean_query_string.($clean_query_string!=""?"&amp;":"")."dgpage=".($current_page-1);
        echo "<a href=\"?$query_string\" title=\"Go To Previous Page\" style=\"font-size:12px;\">&#8249;&nbsp;</a>";
      }

      //Add the textbox with page number
      echo "<input type=\"text\" name=\"dgpage\" value=\"$current_page\" size=\"".strlen($current_page)."\" style=\"border:1px solid gray;background-color:white;font-size:10px;\" onfocus=\"this.select()\" title=\"Go To Page Number\" />/$total_page&nbsp;";

      //Create the next/last links
      if($current_page<$total_page){
        $query_string=$clean_query_string.($clean_query_string!=""?"&":"")."dgpage=".($current_page+1);
        echo "<a href='?$query_string' title='Go To Next Page' style='font-size:12px;'>&#8250;</a>&nbsp;";
        $query_string=$clean_query_string.($clean_query_string!=""?"&":"")."dgpage=".($total_page);
        echo "<a href='?$query_string' title='Go To Last Page' style='font-size:12px;'>&#187;</a>";
      }
      echo "<input type='submit' style='visibility:hidden;width:0;height:0;display:inline;' value='' />";
      echo "</form></div></td>";
      echo "<td style='width:25%' align='right'>";

      //If the page class hasn't been loaded, the display text only.
      if(!$USE_FORM){
        echo "<span style='white-space: nowrap'>$this->datagrid_rpp records per page</span>";
      }
      else{
        $gv                = $this->fetch_grid_values();
        $query_string      = $this->clean_query_string()."&dgrpp=";
        echo "<span style='white-space: nowrap'>Show ".$form->select("dgrpp",$this->datagrid_rpp,array(20=>20,50=>50,100=>100,250=>250,500=>500,1000=>1000,2000=>2000),array("style"=>"font-size:$this->datagrid_row_size;","nodojo"=>"nodojo","onchange"=>"window.location='?$query_string'+this.value"))." per page</span>";
      }
    }else{
      echo "No Records";
    }
    echo "</td></tr>";
    echo "</table></td></tr>";
  }
  /**
     * Generate a table row containing a list of the icons that have been registered.
     *
     */
  public function create_icons(){

    echo "<tr>
            <td colspan='100%' style='text-align:right;background-color:$this->datagrid_header_background;font-family:$this->datagrid_header_family;font-size:$this->datagrid_header_size;'>";
    $footer_buttons = array();
    if ( $this->display_csv){
      $footer_buttons[] = array("?".$this->clean_query_string()."&dgcsv=1","Export To CSV",false);
    }

    if ($this->display_print){
      $footer_buttons[] = array("?".$this->clean_query_string()."&dgprint=1","Print Page",true);
    }
    if($this->display_rss){
      global $page;
      $rss_location = ($_SERVER['PHP_SELF']=='/admin/index.php'?"rss.php":$_SERVER['PHP_SELF'])."?".$this->clean_query_string()."{$this->rss_query_string}&dgrss=1";
      $footer_buttons[] = array($rss_location,"Subscribe to RSS",true);
      $page->content('<link rel="alternate" type="application/rss+xml"  href="'.$rss_location.'" title="Subscribe to RSS">',"page_head");
    }

    $footer_buttons = array_merge($footer_buttons, $this->footer_buttons);
    foreach($footer_buttons as $button){
      echo  "<a href='{$button[0]}' ".($button[2]==true?"target='_blank'":"")." style='float:left;border:1px solid $this->datagrid_title_background;padding:5px;color:$this->datagrid_title_background;text-decoration:none;margin-right:3px;'>{$button[1]}</a>";
    }

    if($this->datagrid_formatted_icons!=false){
      foreach($this->datagrid_formatted_icons as $name=>$icon){
        if($icon['icon_src']!=''){
          echo "<img src='{$icon['icon_src']}' /> - ";
        }
        preg_match_all('/\[\![a-zA-Z1-9_]*\!\]/',$icon['icon_display'],$icon_matches);
        if(is_array($icon_matches)){
          foreach($icon_matches[0] as $icon_section){
            $s=preg_replace("/[^a-zA-Z0-9_]*/","",$icon_section);
            $icon_searches[]=$icon_section;
            $icon_replace[]="";
          }
        }
        echo str_replace($icon_searches,$icon_replace,$icon['icon_display'])."&nbsp;";
      }
      echo "</td></tr>";
    }
  }

  public function create_printable($sql){
    global $page;

    $count = count($this->datagrid_header_rows)-1;
    if(strtolower($this->datagrid_header_rows[$count]['field_display'])=="actions"){
      unset($this->datagrid_header_rows[$count]);
    }

    while(ob_get_length()>0){ob_clean();}
    ob_start();
    $page->Page("blank",TPL_ROOT);
    $this->query($sql);
    $this->datagrid_page_records = $this->affected;
    $this->datagrid_table_border = 1;
    $this->datagrid_row_background_1 = '#fff';
    $this->datagrid_row_background_2 = '#fff';
    $this->display_print = false;
    $this->display_csv   = false;
    $this->datagrid_highlight_rows = false;
    $this->datagrid_highlight_default_on = false;
    $this->display_highlight_checkbox = false;
    $this->datagrid_header_background = '#ececec';
    $this->datagrid_title_background  = '#ececec';
    $this->datagrid_title_color       = '#000';

    echo "<h2 class='entry-title'>".$page->page_content['page_title']."</h2>";
    echo $this->datagrid_printable_summary_text;
    $this->create_printable_header();
    if($this->affected<=0){
      echo "<tr><td colspan='100%' align='center'>$this->datagrid_message_norecords</td></tr>";
    }
    else{
      while ($r=$this->fetch_assoc()) {
        $this->create_row($r);
      }
    }
    if ($this->datagrid_paginate==true) {
      $this->create_footer();
    }

    //$this->create_icons();
    echo "</table>";

    if($this->error){
      if($this->saved_err_message!=''){
        echo $this->saved_err_message;
      }else{
        $err_msg = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "," - Syntax error near <b>",$this->error."</b>");
        $substr = str_replace("You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ","",$this->error);
        notify("<b>There was an error in creating the Datagrid</b><br /> $err_msg <hr /><pre style='text-size:6px;font-family:courier;'>".str_replace($substr,"<b>$substr</b>",$sql)."</pre>",NOTIFY_DISPLAY_ERROR);
      }
    }
    $page->content(ob_get_clean(),"page_content");
    $page->display_page();
    exit;
  }

  public function create_rss($sql) {

    global $page;
    while(ob_get_length()>0){
      ob_clean();
    }
    $rss_title_field = $this->rss_title_field =='' ? $this->datagrid_header_rows[0]['field_name'] : $this->rss_title_field;
    if($this->rss_link_icon   ==''){
      for($i=0;$i<count($this->datagrid_icons);$i++){
        if($this->datagrid_icons[$i]['icon_href']!=''){
          $rss_link_icon = $this->datagrid_icons[$i]['icon_name'];
          break;
        }
      }
    }
    else {
      $rss_link_icon = $this->rss_link_icon;
    }

    $rss_pub_date           = $this->rss_published_field;
    $rss_description_field  = $this->rss_description_field =='' ? $this->datagrid_header_rows[1]['field_name'] : $this->rss_description_field;
    $rss_channel_title      = $this->rss_feed_title==''?htmlentities($page->page_content['page_title']):htmlentities($this->rss_feed_title);

    $rs = $this->query($sql);
    ob_start();
    $this->build_printable_filters_box();
    $this->create_footer();
    ob_clean();
    $description  = strip_tags($this->printable_search_text);
    $description .= " ".strip_tags($this->printable_narrowed_text);
    if($this->datagrid_sort_field){
      $gv = $this->fetch_grid_values();
      $description.= " Sorted By \"".$this->datagrid_header_rows[$this->datagrid_sort_field]['field_display']."\"".($this->datagrid_sort_direction==1 || $this->datagrid_sort_direction==null?" Ascending":" Descending");
    }

    $file_name = $_SERVER['PHP_SELF']=='/admin/rss.php' ? "/admin/index.php" : $_SERVER['PHP_SELF'];

    $field_names = array();
    $grouping_categories = explode(",",$this->rss_category_field);

    foreach ($this->datagrid_header_rows as $key=>$array){
      if($array['field_name']!=''){
        $field_names[$array['field_name']]=$array['field_display'];
      }
      if(in_array($array['field_name'],$grouping_categories)){
        $rss_category_label[$array['field_name']] = $array['field_display'];
      }
    }

    for($i=0;$i<mysql_num_fields($rs);$i++){
      $resultset_fields[mysql_field_name($rs,$i)] = mysql_field_type($rs,$i);
    }


    header("content-type: text/xml");
    echo '<?xml version="1.0" encoding="utf-8" ?>
<rss version="2.0" xmlns:cf="http://www.microsoft.com/schemas/rss/core/2005" xmlns:inexo="uri:inexo:datagrid:rss"><channel>';
    echo "
<cf:treatAs xmlns:cf=\"http://www.microsoft.com/schemas/rss/core/2005\">list</cf:treatAs>
<cf:listinfo>";
    foreach($field_names as $name=>$display){
      switch ($resultset_fields[$name]) {
        case "int":
        case "real":
          $field_type = "number";
          break;
        case "date":
        case "time":
        case "datetime":
          $field_type = "date";
          break;
        default:
          $field_type = "text";
      }

      echo "<cf:sort ns=\"uri:inexo:datagrid:rss\" element=\"$name\" data-type=\"$field_type\" label=\"$display\" />";
    }
    foreach($grouping_categories as $group_name){
      echo "<cf:group ns=\"uri:inexo:datagrid:rss\" label=\"".(array_key_exists($group_name,$rss_category_label)?$rss_category_label[$group_name]:$group_name)."\" element=\"dggrp_{$group_name}\"></cf:group>";
    }

    echo "
</cf:listinfo>
   		<title>".str_replace("&nbsp;","&#160;","$rss_channel_title - $description")."</title>
   		<description>";
    echo str_replace("&nbsp;","&#160;",$description);
    echo "</description>
		<link>http://{$_SERVER['HTTP_HOST']}{$file_name}?".htmlentities($this->clean_query_string()).$this->rss_query_string."</link>
		<guid isPermaLink='false'>".md5($$link_href)."</guid>

		<generator>Inexo Datagrid Service</generator>
		<lastBuildDate>".date("r")."</lastBuildDate>
		";
    #http://{$_SERVER['HTTP_HOST']}{$file_name}?".htmlentities($this->clean_query_string()).$this->rss_query_string."
    $this->reformat_icons();
    while($r=$this->fetch_assoc()){
      if($rss_link_icon!==""){
        $icon_searches = array();
        $icon_replace  = array();
        preg_match_all('/\[\![a-zA-Z1-9_]*\!\]/',$this->datagrid_formatted_icons[$rss_link_icon]['icon_href'],$icon_matches);
        if(is_array($icon_matches)){
          foreach($icon_matches[0] as $icon_section){
            $s=preg_replace("/[^a-zA-Z0-9_]*/","",$icon_section);
            $icon_searches[]=$icon_section;
            $icon_replace[]=$r[$s];
          }
        }
        $link_href  = htmlentities(str_replace($icon_searches,$icon_replace,$this->datagrid_formatted_icons[$rss_link_icon]['icon_href']));
        if(substr($link_href,0,1)=='?'){
          $link_href = "http://".$_SERVER['HTTP_HOST'].$file_name.$link_href;
        }
      }else{
        $link_href = "http://{$_SERVER['HTTP_HOST']}{$file_name}?".str_replace("&","&amp;",$this->csv_query_string).$this->rss_query_string;
      }
      echo "
		<item>
			<title>".htmlentities($r[$rss_title_field])."</title>
			<link>{$link_href}</link>".($rss_pub_date!=''?"
			<pubDate>".date("r",strtotime($r[$rss_pub_date]))."</pubDate>":"
			<pubDate>".date("r",strtotime("-1 hour"))."</pubDate>").
			"<description><![CDATA[".$r[$rss_description_field]."]]></description>";
			foreach ($field_names as $name=>$display){
			  echo "\n<inexo:$name><![CDATA[{$r[$name]}]]></inexo:$name>";
			}
			foreach($grouping_categories as $group_name){
			  echo "<inexo:dggrp_{$group_name}><![CDATA[{$r[$group_name]}]]></inexo:dggrp_{$group_name}>";
			}
			echo "
    	   <guid isPermalink='false'>".md5($link_href)."</guid>
		</item>";
    }


    echo "</channel></rss>";
    exit;

  }

  #4.0 Data Support Functions
  /**
     * Provides basic filtering and escaping of data types
     *
     * @param mixed $mixed Can be various types
     * @param string $type Optional. Can be one of bool, integer, float, numeric, string, null
     * @param boolean $quotes If TRUE wraps in double quotes, if FALSE (default)
     *                        returns without double quotes
     * @return mixed
     */
  public function escape($mixed, $type = null, $quotes = true) {
    $type = (!empty($type)) ? strtolower(trim($type)) : null;
    $type = (in_array($type, array('bool','integer','float','numeric','string','null'))) ? $type : null;
    switch (true) {
      case is_object($mixed):
        foreach ($mixed as $key => $value) {
          $mixed->$key = $this->escape($value, $type, $quotes);
        }
        break;
      case is_array($mixed):
        foreach ($mixed as $key => $value) {
          $mixed[$key] = $this->escape($value, $type, $quotes);
        }
        break;
      case $type=='identifier':
        $mixed = preg_replace('/[a-zA-Z0-9_.]+/', '', (string) $mixed);
        $mixed = ($quotes) ? '`'.str_replace('.', '`.`', $mixed).'`' : $mixed;
        break;
      case $type=='bool':
        $mixed = (!empty($mixed)) ? 1 : 0;
        break;
      case $type=='integer':
        $mixed = intval(preg_replace('/\D+/', '', $mixed));
        break;
      case $type=='float':
        $mixed = floatval(preg_replace('/[\D.,+-]+/', '', $mixed));
        break;
      case $type=='numeric':
        $mixed = (preg_match('/^[+-]?(([0-9]+)|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*)|(([0-9]+|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*))[eE][+-]?[0-9]+))$/', $mixed)!=false)
        ? floatval(preg_replace('/[\D.,+-]+/', '', $mixed))
        : intval(preg_replace('/\D+/', '', $mixed));
        break;
      case $type=='string':
        $mixed = mysql_real_escape_string((string)$mixed, $this->mysql);
        if ($quotes===true) {
          $mixed = '"'.$mixed.'"';
        }
        break;
      case $type=='null':
        $mixed = 'NULL';
        break;
      case is_bool($mixed):
        $mixed = ($mixed) ? 1 : 0;
        break;
      case is_int($mixed):
        $mixed = intval($mixed);
        break;
      case is_float($mixed):
        $mixed = floatval($mixed);
        break;
      case is_numeric($mixed):
        $mixed = (preg_match('/^[+-]?(([0-9]+)|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*)|(([0-9]+|([0-9]*\.[0-9]+|[0-9]+\.[0-9]*))[eE][+-]?[0-9]+))$/', $mixed)!=false)
        ? floatval($mixed)
        : intval($mixed);
        break;
      case is_string($mixed):
        $mixed = mysql_real_escape_string($mixed, $this->mysql);
        if ($quotes===true) {
          $mixed = '"'.$mixed.'"';
        }
        break;
      case is_null($mixed):
        $mixed = 'NULL';
        break;
    }
    return $mixed;
  }
  /**
     * Removes the LIMIT condition of a SQL query
     *
     * @param string $sql
     * @return string $sql
     */
  public function sanitize_sql($sql){
    $sql=preg_replace("/ LIMIT\s[0-9]*\s*,?\s*[0-9]+ /i","",$sql);
    return $sql;
  }
  /**
     * Reformat the icon data so it can be used in datagrid
     *
     * @return boolean
     */
  public function reformat_icons(){
    if(is_array($this->datagrid_icons)){
      foreach($this->datagrid_icons as $icon){
        $icons[$icon["icon_name"]]=$icon;
      }
      $this->datagrid_formatted_icons=$icons;
      unset($icons);
      return true;
    }else{
      $this->datagrid_formatted_icons=false;
      return false;
    }
  }

  /**
   * Takes a multi-dimensional array and creates a urlencoded querystring.
   *
   * @param unknown_type $parameter
   * @param unknown_type $value
   * @return unknown
   */
  public function encodeQuerystringParameter($parameter,$value,$asHiddenField = false){
    $string = array();
    if(is_array($value)){
      foreach($value as $subparam=>$subval){
        $string[] = $this->encodeQuerystringParameter("{$parameter}[$subparam]",$subval,$asHiddenField);
      }
    }
    else{
      if($asHiddenField){
        $form = new Form(FORM_RETURN_VALUE);
        $string[] = $form->hiddenbox($parameter,$value);
      }
      else{
        $string[] = $parameter."=".urlencode($value);
      }
    }
    return implode(($asHiddenField ? "": "&"),$string);
  }
  ################
  #CSV PROCESSING#
  ################
  /**
     * Registers an array containing field/label pairs to use in a CSV diagram.
     *
     * @param array $array Use the key as the field name, and the value as the display to set the Column Title
     */
  public function register_csv_field($array){
    foreach($array as $field=>$display){
      $this->datagrid_csv_additional_fields[$field] = $display;
    }
  }
  /**
     * Set the fields that should be used for CSV
     *
     * @param array $array
     */
  public function set_csv_fields($array){
    foreach($array as $field=>$display){
      $this->datagrid_csv_field_map[$field] = $display;
    }
  }
  /**
     * Create a header for for the CSV sheet
     *
     * @return string
     */
  public function create_csv_header(){
    if(count($this->datagrid_csv_field_map) == 0){
      foreach($this->datagrid_header_rows as $id=>$data){
        $fields[]=$data['field_display'];
        $csv_fields[] = $data['field_name']==''? $data['field_custom'] : $data['field_name'];
      }
    }else{
      foreach($this->datagrid_csv_field_map as $field=>$display){
        if(is_integer($display) && is_integer($field)){
          $fields[] = $this->datagrid_header_rows[$display]['field_display'];
          $csv_fields[] = $display;
        }else{
          $fields[]     = $display;
          $csv_fields[] = $field;
        }
      }
    }

    if(count($this->datagrid_csv_additional_fields)>0){
      foreach($this->datagrid_csv_additional_fields as $field=>$display){
        if(is_integer($display) && is_integer($field)){
          $fields[] = $this->datagrid_header_rows[$display]['field_display'];
          $csv_fields[] = $display;
        } else {
          $fields[] = $display;
          $csv_fields[] = $field;
        }
      }
    }


    foreach($fields as $display){
      $csv_text[]='"'.str_replace('"','""',$display).'"';
    }
    $this->datagrid_csv_fields = $csv_fields;
    return implode(",",$csv_text)."\n";
  }
  /**
     * Create a csv row with the fields defined in Datagrid::datagird_header_rows
     * @param array $r
     */
  public function create_csv_row($r){
    $csv_cell=array();
    foreach($this->datagrid_csv_fields as $id){
      if(is_integer($id)){
        $data = $this->datagrid_header_rows[$id];
      }else{
        $data['field_name'] = $id;
      }

      if($data['field_custom']!=''){
        /**
         * Replace all tokens
         */
        preg_match_all('/\[\![a-zA-Z1-9\_\-]*\!\]/',$data['field_custom'],$matches);
        if(is_array($matches)){
          $searches=null;
          $replace=null;
          foreach($matches[0] as $section){
            $sct=preg_replace("/[^a-zA-Z0-9_\-]*/","",$section);
            $searches[]=$section;
            $replace[]=$r[$sct];
          }
        }
        $href=str_replace($searches,$replace,$data['field_custom']);


        /**
         * Repace all the icon holders with blanks.
         **/
        preg_match_all('/\{\![a-zA-Z1-9_]*\!\}/',$data['field_custom'],$icons);
        if(is_array($icons)){
          $searches=null;
          $replace=null;
          foreach($icons[0] as $icon){
            $sct=preg_replace("/[^a-zA-Z0-9_]*/","",$icon);
            $searches[]=$icon;

            preg_match_all('/\[\![a-zA-Z1-9_]*\!\]/',$this->datagrid_formatted_icons[$sct]['icon_href'],$icon_matches);
            if(is_array($icon_matches)){
              foreach($icon_matches[0] as $icon_section){
                $s=preg_replace("/[^a-zA-Z0-9_]*/","",$icon_section);
                $icon_searches[]=$icon_section;
                $icon_replace[]="";
              }
            }
            $replace[]="";
          }
        }
        $href=str_replace($searches,$replace,$href);

        /**
         * If we have to eval. do it
         */
        if($data['field_eval']==true){
          ob_start();
          eval($href);
          $csv_cell[]='"'.strip_tags(str_replace("<br />","\r\n",str_replace('"','""',ob_get_clean()))).'"';
        }
        else{
          $csv_cell[]='"'.strip_tags(str_replace("<br />","\r\n",str_replace('"','""',$href))).'"';
        }
      }
      else{
        $csv_cell[]='"'.strip_tags(str_replace("<br />","\r\n",str_replace('"','""',$r[$data['field_name']]))).'"';
      }
    }
    return implode(",",$csv_cell)."\n";
  }
  /**
     * Creates a CSV export of the datagrid, pass the same headers and sql to
     * easily generate a CSV sheet.
     *
     * @param string $sql
     * @param array $headers
     * @param string $file_name
     */
  public function create_csv($sql,$headers=null,$file_name=null){
    if(!is_array($headers) && !is_array($this->datagrid_header_rows)){
      return false;
    }elseif($headers!=null){
      foreach($headers as $header){
        $this->datagrid_header_rows[]=$header;
      }
    }
    while(ob_get_length()>0){
      ob_clean();
    }
    $csv_output.=$this->create_csv_header();
    $this->datagrid_sql=$sql;
    $this->query($sql);
    while($r=$this->fetch_assoc()){
      $csv_output.=$this->create_csv_row($r);
    }

    $size_in_bytes = strlen($csv_output);
    header("content-type:text/csv");
    header("Content-disposition:  attachment; filename=".($file_name==null?"export_".date("Y-m-d H:i:s").".csv":$file_name)."; size=$size_in_bytes");
    echo $csv_output;
    exit;
  }
}

class OptionsList {
  public $sql;
  public $keys;
  public $values;
  public $options;
  /**
     * Creates an object to store data so that that can be modified by the database class.
     *
     * @param string $sql
     * @param string $keys
     * @param string $values
     * @param boolean $add_all_option
     * @param string $foreign_key_name
     */
  public function __construct($sql, $keys, $values,$add_all_option=true,$foreign_key_name=null){
    $db = DatabaseFactory::getDb();
    $this->sql     = $sql;
    $this->keys    = $keys;
    $this->values  = $values;
    $this->foreign_key_name = $foreign_key_name;
    $this->add_all_option = $add_all_option;
    $this->options = ($add_all_option ? array("dgall"=>"All") : array()) + $db->create_array_values($sql,$keys,$values);
  }
}

class OptionsNodeList {
  function __construct($id=null, $add_all_option=true, $foreign_key_name=null, $active=true, $field = "guid", $parent_field = "parent_guid", $table = "type"){
    $db = DatabaseFactory::getDb();
    $this->sql     = $sql;
    $this->keys    = $keys;
    $this->values  = $values;
    $this->foreign_key_name = $foreign_key_name;
    $this->add_all_option = $add_all_option;

    if($add_all_option){
      $this->options = array('dgall'=>array("guid"=>"dgall","name"=>"All","level"=>1)) + $db->fetch_node_tree($id,$active,$field,$parent_field,$table);
    }
    else {
      $this->options = $db->fetch_node_tree($id,$active,$field,$parent_field,$table);
    }
  }
}
