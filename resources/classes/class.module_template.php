<?php
define("TEMPLATE_ALLOW_EDIT", 0 );
define("TEMPLATE_READ_ONLY" , 1);

/**
 * Adds functionality to page class by customizing the functionality for
 * use with module templates.
 * @author Brian Castellanos
 * @version 1.1
 * @since  v1.1 2011-12-02 Adds type_select method
 * @since  v1.0 2009-07-02 Initial Version
 */
class ModuleTemplate extends Page {
  public $version = "1.1";
  /**
   * Sets the current state of the template, either as read-only, editable
   *
   * @var integer
   */
  public $state;

  /**
     * Form object used to create elements.
     * @var Form
     */
  public $form_obj;

  /**
   * Creates a new Module Template object
   *
   * @param string $template
   * @param string $directory
   * @param integer $state
   */
  public function __construct($state=TEMPLATE_ALLOW_EDIT,$template='',$directory=null){
    $this->state = $state;
    parent::__construct($template,$directory);
    include_class("form");
    $this->form_obj = new Form();
  }

  /**
   * Changes which template will be loaded.
   *
   * @param string $template
   * @param string $path
   */
  public function set_template($template,$path=null){
    $this->page_template      = $template;
    $this->page_template_path = $path;
  }

  /**
   * Read the template file and return its contents.
   *
   * @param string $template
   * @param string $path
   * @return mixed
   */
  public function load_template($template=null,$path=null){
    if($path==null){
      $bt    = array_reverse(debug_backtrace());
      $index = basename($bt[0]['file'])=='dispatcher.php' ? 1 : 0;
      $path  = dirname($bt[$index]['file']).DIR_TKN;
    }
    $template .= $template!='' ?".":null;
    $file      = $path.$template."html.php";
    if(!file_exists($file)|| !is_readable($file)){
      if(!file_exists($file)){
        echo "$file doesn't exist<br />";
      }
      if(!is_readable($file)){
        echo "$file is not readable<br />";
      }
      return false;
    }else{
      ob_start();
      include($file);
      return ob_get_clean();
    }
  }

  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
  public function textbox($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->text($element_name,$element_value,$attributes) : $element_value;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
  public function button($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->button($element_name,$element_value,$attributes) : null;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
public function checkbox($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->checkbox($element_name,$element_value,$attributes) : $element_value;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
  public function hiddenbox($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->hiddenbox($element_name,$element_value,$attributes) : $element_value;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
  public function password($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->password($element_name,$element_value,$attributes) : $element_value;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $attributes
   * @param boolean $always_show
   */
  public function submit($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->submit($element_name,$element_value,$attributes) : null;
    $this->content($content,$token_name);
  }
  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param array $options
   * @param array $attributes
   * @param boolean $always_show
   */
  public function select($token_name,$element_name,$element_value,$options,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->select($element_name,$element_value,$options,$attributes) : $options[$element_value];
    $this->content($content,$token_name);
  }


  public function type_select($token_name,$element_name,$element_value,$options,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->type_select($element_name,$element_value,$options,$attributes) : $options[$element_value];
    $this->content($content,$token_name);
  }

  /**
   * Creates an HTML element or displays text depending on the state
   *
   * @param string $tag_name
   * @param string $element_name
   * @param string $element_value
   * @param object $attributes
   * @param boolean $always_show
   */
  public function textarea($token_name,$element_name,$element_value,$attributes=null,$always_show=false){
    $content = ($this->state==TEMPLATE_ALLOW_EDIT || $always_show==true) ? $this->form_obj->textarea($element_name,$element_value,$attributes) : $element_value;
    $this->content($content,$token_name);
  }

  /**
   * Parses the Module's template, rendering the HTML, and saves it to page's token to be rendered to the browser
   *
   * @param unknown_type $token_name
   */
  public function save_to_page($token_name="page_content",$insert_mode = PAGE_CONTENT_INSERT_AFTER){
    global $page;
    ob_start();
    $this->display_page();
    $page->content(ob_get_clean(),$token_name,$insert_mode);
  }
}