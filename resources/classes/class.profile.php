<?php
  // Profile usage
  define("PROFILE_USAGE_GUID", "{DEF11DC9-43D6-8DE8-E201-EAE86C2D7B88}");

  // Default profile type (if one is not supplied in the data during ADD)
  define("PROFILE_DEFAULT_PROFILE_TYPE_GUID", "{D9631CB7-631A-404E-8A66-B6914F9CD192}"); // Default

  // Default address type (if one is not supplied in the data during ADD)
  define("PROFILE_DEFAULT_ADDRESS_TYPE_GUID", "{D07CB5B3-B3A0-6AF5-3D8D-EBA6964CF6AC}"); // mailing

  // Default profile_attribute type (if one is not supplied in the data during ADD)
  define("PROFILE_DEFAULT_ATTRIBUTE_TYPE_GUID", ""); // to-be-defined (?)

  // Default contact_point type (if one is not supplied in the data during ADD)
  define("PROFILE_DEFAULT_CONTACT_POINT_TYPE_GUID", "{D8EADFF0-11B5-5764-5FB6-FBCF7B668305}"); // home

  // Contact point usage
  define("PROFILE_CONTACT_POINT_USAGE_GUID", "{2009D273-A376-A8B0-8762-C7A392415C94}");

  // Address types (child of PROFILE_CONTACT_POINT_USAGE_GUID)
  define("PROFILE_ADDRESS_TYPE_GUID", "{7D28FE54-F6F7-8651-8FEC-4360B6C3375A}");

  // Phone types (child of PROFILE_CONTACT_POINT_USAGE_GUID)
  define("PROFILE_PHONE_TYPE_GUID", "{BDC54A1B-EB31-BAED-4942-D0EEAD751BAE}");

  class Profile {
    public $error = null;
    public $errors = null;

    public $address_types = null;
    public $phone_types = null;

    // to-be-defined
    public $internet_types = null;
    public $messenger_types = null;
    public $social_media_types = null;

    public $state_provinces = null;
    public $countries = null;

    /**
     * Create a new Profile object to interact with the data
     *
     * @param Database $db object
     */
    public function __construct() {
      global $db;

      if (!$db instanceof Database) {
        throw new Exception("The Database handler \$db is not an instance of class Database", E_ERROR);

        return false;
      }

      $this->address_types = new OptionsList("SELECT `guid`, `name` FROM `type` WHERE `parent_guid` = '".PROFILE_ADDRESS_TYPE_GUID."' AND `status` = 'active' ORDER BY `display_order`", "guid", "name", false);
      $this->phone_types = new OptionsList("SELECT `guid`, `name` FROM `type` WHERE `parent_guid` = '".PROFILE_PHONE_TYPE_GUID."' AND `status` = 'active' ORDER BY `display_order`", "guid", "name", false);

      $this->state_provinces = new OptionsList("SELECT `id`, `abbreviation` FROM `state_province` ORDER BY `abbreviation`", "id", "abbreviation", false);
      $this->countries = new OptionsList("SELECT `id`, `name` FROM `country` ORDER BY `id`", "id", "name", false);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a profile record
     *
     * @param mixedarray $data
     * @param Database $db object
     * @return mixed - ID of profile record if successful, otherwise false
     */
    public function addProfile($data = null) {
      global $db;

      if ($data) {
        unset($data["id"]);

        if (!$data["profile_type_guid"]) $data["profile_type_guid"] = PROFILE_DEFAULT_PROFILE_TYPE_GUID;

        if (!$data["affiliated"]) $data["affiliated"] = "no";

        if (!$data["show_email"]) $data["show_email"] = "no";

        if (!$data["status"]) $data["status"] = "pending";

        $data["created_on"] = "DB_TIMESTAMP";

        if ($retval = $db->mk_sql(addslashes_array($data), "profile", "insert")) {
          return $retval;
        }
        else {
          $this->addError("addProfile() - An error occurred during add of profile data (ID = n/a):");
          $this->addError("addProfile() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Delete a profile
     *
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfile($profile_id = null) {
      global $db;

      // delete profile addresses
      if ($this->deleteProfileAddresses($profile_id)) {
        // delete profile contact points
        if ($this->deleteProfileContactPoints($profile_id)) {
          // delete the profile
          $db->Query("UPDATE `profile` SET `status` = 'deleted' WHERE `id` = '{$profile_id}'");

          return true;
        }
        else {
          $this->addError("deleteProfile() - An error occurred during profile contact point deletion (ID = {$profile_id}).");
        }
      }
      else {
        $this->addError("deleteProfile() - An error occurred during profile address deletion (ID = {$profile_id}).");
      }

      return false;
    }

    /**
     * Update an identified profile's record
     *
     * @param mixedarray $data
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function updateProfile($data = null, $profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_id)) {

        $data["id"] = $profile_id;

        unset($data["created_on"]);

        if ($db->mk_sql(addslashes_array($data), "profile", "update", "id")) {
          return true;
        }
        else {
          $this->addError("updateProfile() - An error occurred during add of profile data (ID = {$profile_id}):");
          $this->addError("updateProfile() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a profile address record set for an identified profile
     *
     * @param mixed $data
     * @param integer $profile_id
     * @param Database $db object
     * @return mixed - ID of profile_address record if successful, otherwise false
     */
    public function addProfileAddress($data = null, $profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_id)) {
        unset($data["id"]);

        if (!$data["type_guid"]) $data["type_guid"] = PROFILE_DEFAULT_ADDRESS_TYPE_GUID;

        if (!$data["status"]) $data["status"] = "active";

        $data["created_on"] = "DB_TIMESTAMP";

        if ($retval = $db->mk_sql(addslashes_array($data), "address", "insert")) {
          $data = array("profile_id" => $profile_id,
                        "address_id" => $retval,
                        "created_on" => "DB_TIMESTAMP");

          if ($retval = $db->mk_sql(addslashes_array($data), "profile_address", "insert")) {
            return $retval;
          }
          else {
            $this->addError("addProfileAddress() - An error occurred during add of profile address data (ID = {$retval}):");
            $this->addError("addProfileAddress() - DATA = ".print_r($data, true));
          }
        }
        else {
          $this->addError("addProfileAddress() - An error occurred during add of the address data (ID = n/a):");
          $this->addError("addProfileAddress() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Delete an identified profile address record
     *
     * @param integer $profile_address_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileAddress($profile_address_id = null) {
      global $db;

      $adata = $db->query_fetch_assoc("SELECT `address_id` FROM `profile_address` WHERE `id` = '{$profile_address_id}'", false);

      $db->Query("UPDATE `address` SET `status` = 'deleted' WHERE `id` = '{$adata["address_id"]}'");

      return true;
    }

    /**
     * Update an identified profile address record
     *
     * @param mixedarray $data
     * @param integer $profile_address_id
     * @param Database $db object
     * @return boolean
     */
    public function updateProfileAddress($data = null, $profile_address_id = null) {
      global $db;

      if ($data && is_numeric($profile_address_id)) {
        $adata = $db->query_fetch_assoc("SELECT `address_id` FROM `profile_address` WHERE `id` = '{$profile_address_id}'", false);

        $data["id"] = $adata["address_id"];

        unset($data["created_on"]);

        if ($db->mk_sql(addslashes_array($data), "address", "update", "id")) {
          return true;
        }
        else {
          $this->addError("updateProfileAddress() - An error occurred during update of profile address data (ID = {$profile_address_id}):");
          $this->addError("updateProfileAddress() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Deletes an identified profile's addresses
     *
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileAddresses($profile_id = null) {
      global $db;

      $addresses = $db->query_fetch_assoc("SELECT `id` FROM `profile_address` WHERE `profile_id` = '{$profile_id}'");

      foreach ($addresses as $address) {
        if (!$this->deleteProfileAddress($address["id"])) {
          $this->addError("deleteProfileAddresses() - An error occurred during profile address deletion (ID = {$profile_id}).");

          return false;
        }
      }

      return true;
    }

    /**
     * Returns an array of profile address IDs for an identified profile
     *
     * @param integer $profile_id
     * @return array
     */
    public function getProfileAddresses($profile_id = null) {
      global $db;

      if (is_numeric($profile_id)) {
        if ($addresses = $db->query_fetch_assoc("SELECT `id` FROM `profile_address` WHERE `profile_id` = '{$profile_id}'", true)) {
          return $addresses;
        }
        else {
          $this->addError("getProfileAddresses() - An error occurred during retrieval of the profile address data (ID = {$profile_id}):");
        }
      }

      return false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a profile attribute record for an identified profile
     *
     * @param mixed $data
     * @param integer $profile_id
     * @param Database $db object
     * @return mixed - ID of profile_attribute record if successful, otherwise false
     */
    public function addProfileAttribute($data = null, $profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_id)) {
        unset($data["id"]);

        $data["profile_id"] = $profile_id;

        if (!$data["type_guid"]) $data["type_guid"] = PROFILE_DEFAULT_ATTRIBUTE_TYPE_GUID;

        if (!$data["status"]) $data["status"] = "active";

        $data["created_on"] = "DB_TIMESTAMP";

        if ($retval = $db->mk_sql(addslashes_array($data), "profile_attribute", "insert")) {
          return $retval;
        }
        else {
          $this->addError("addProfileAttribute() - An error occurred during add of the profile attribute data (ID = n/a):");
          $this->addError("addProfileAttribute() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Delete an identified profile attribute
     *
     * @param integer $profile_attribute_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileAttribute($profile_attribute_id = null) {
      global $db;

      $db->Query("UPDATE `profile_attribute` SET `status` = 'deleted' WHERE `id` = '{$profile_attribute_id}'");

      return true;
    }

    /**
     * Update an identified profile attribute record
     *
     * @param mixedarray $data
     * @param integer $profile_attribute_id
     * @param Database $db object
     * @return boolean
     */
    public function updateProfileAttribute($data = null, $profile_attribute_id = null) {
      global $db;

      if ($data && is_numeric($profile_attribute_id)) {
        $data["id"] = $profile_attribute_id;

        unset($data["created_on"]);

        if ($db->mk_sql(addslashes_array($data), "profile_attribute", "update", "id")) {
          return true;
        }
        else {
          $this->addError("updateProfileAttribute() - An error occurred during update of profile attribute data (ID = {$profile_attribute_id}):");
          $this->addError("updateProfileAttribute() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Deletes an identified profile's attributes
     *
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileAttributes($profile_id = null) {
      global $db;

      $attributes = $db->query_fetch_assoc("SELECT `id` FROM `profile_attribute` WHERE `profile_id` = '{$profile_id}'");

      foreach ($attributes as $attribute) {
        if (!$this->deleteProfileAttribute($attribute["id"])) {
          $this->addError("deleteProfileAttributes() - An error occurred during profile attribute deletion (ID = {$profile_id}).");

          return false;
        }
      }

      return true;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a profile contact point record set for an identified profile
     *
     * @param mixed $data
     * @param integer $profile_id
     * @param Database $db object
     * @return mixed - ID of profile_contact_point record if successful, otherwise false
     */
    public function addProfileContactPoint($data = null, $profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_id)) {
        unset($data["id"]);

        if (!$data["type_guid"]) $data["type_guid"] = PROFILE_DEFAULT_CONTACT_POINT_TYPE_GUID;

        if (!$data["status"]) $data["status"] = "active";

        $data["created_on"] = "DB_TIMESTAMP";

        if ($retval = $db->mk_sql(addslashes_array($data), "contact_point", "insert")) {
          $data = array("profile_id"       => $profile_id,
                        "contact_point_id" => $retval,
                        "created_on"       => "DB_TIMESTAMP");

          if ($retval = $db->mk_sql(addslashes_array($data), "profile_contact_point", "insert")) {
            return $retval;
          }
          else {
            $this->addError("addProfileContactPoint() - An error occurred during add of profile contact point data (ID = {$retval}):");
            $this->addError("addProfileContactPoint() - DATA = ".print_r($data, true));
          }
        }
        else {
          $this->addError("addProfileContactPoint() - An error occurred during add of the contact point data (ID = n/a):");
          $this->addError("addProfileContactPoint() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Delete an identified profile contact point
     *
     * @param integer $profile_contact_point_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileContactPoint($profile_contact_point_id = null) {
      global $db;

      $pcpdata = $db->query_fetch_assoc("SELECT `contact_point_id` FROM `profile_contact_point` WHERE `id` = '{$profile_contact_point_id}'", false);

      $db->Query("UPDATE `contact_point` SET `status` = 'deleted' WHERE `id` = '{$pcpdata["contact_point_id"]}'");

      return true;
    }

    /**
     * Update an identified profile contact point record
     *
     * @param mixedarray $data
     * @param integer $profile_contact_point_id
     * @param Database $db object
     * @return boolean
     */
    public function updateProfileContactPoint($data = null, $profile_contact_point_id = null) {
      global $db;

      if ($data && is_numeric($profile_contact_point_id)) {
        $pcpdata = $db->query_fetch_assoc("SELECT `contact_point_id` FROM `profile_contact_point` WHERE `id` = '{$profile_contact_point_id}'", false);

        $data["id"] = $pcpdata["contact_point_id"];

        unset($data["created_on"]);

        if ($db->mk_sql(addslashes_array($data), "contact_point", "update", "id")) {
          return true;
        }
        else {
          $this->addError("updateProfileContactPoint() - An error occurred during update of profile contact point data (ID = {$profile_contact_point_id}):");
          $this->addError("updateProfileContactPoint() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Deletes an identified profile's contact points
     *
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileContactPoints($profile_id = null) {
      global $db;

      $contact_points = $db->query_fetch_assoc("SELECT `contact_point_id` FROM `profile_contact_point` WHERE `profile_id` = '{$profile_id}'");

      foreach ($contact_points as $contact_point) {
        if (!$this->deleteProfileContactPoint($contact_point["id"])) {
          $this->addError("deleteProfileContactPoints() - An error occurred during profile contact point deletion (ID = {$profile_id}).");

          return false;
        }
      }

      return true;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Add a profile contact profile record set for an identified profile
     *
     * @param mixed $data
     * @param integer $profile_id
     * @param Database $db object
     * @return mixed - ID of profile_contact_profile record if successful, otherwise false
     */
    public function addProfileContactProfile($data = null, $profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_id)) {
        $contact_profile_id = $this->addProfile($data);

        if (is_numeric($contact_profile_id)) {
          $data = array("profile_id"         => $profile_id,
                        "contact_profile_id" => $contact_profile_id,
                        "created_on"         => "DB_TIMESTAMP");

          if ($retval = $db->mk_sql(addslashes_array($data), "profile_contact_profile", "insert")) {
            return $retval;
          }
          else {
            $this->addError("addProfileContactProfile() - An error occurred during add of profile contact profile data (ID = {$contact_profile_id}):");
            $this->addError("addProfileContactProfile() - DATA = ".print_r($data, true));
          }
        }
        else {
          $this->addError("addProfileContactProfile() - An error occurred during add of the contact profile data (ID = n/a):");
          $this->addError("addProfileContactProfile() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Delete an identified profile contact profile
     *
     * @param integer $profile_contact_profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileContactProfile($profile_contact_profile_id = null) {
      global $db;

      $pcpdata = $db->query_fetch_assoc("SELECT `contact_profile_id` FROM `profile_contact_profile` WHERE `id` = '{$profile_contact_profile_id}'", false);

      $this->deleteProfile($pcpdata["contact_profile_id"]);

      return true;
    }

    /**
     * Returns the profile ID of an identified profile contact profile
     *
     * @param integer $profile_contact_profile_id
     * @return boolean
     */
    public function getContactProfileID($profile_contact_profile_id = null) {
      global $db;

      if (is_numeric($profile_contact_profile_id)) {
        if ($pcpdata = $db->query_fetch_assoc("SELECT `contact_profile_id` FROM `profile_contact_profile` WHERE `id` = '{$profile_contact_profile_id}'", false)) {
          return $pcpdata["contact_profile_id"];
        }
        else {
          $this->addError("getContactProfileID() - An error occurred during retrieval of the profile contact profile data (ID = {$profile_contact_profile_id}):");
        }
      }

      return false;
    }

    /**
     * Returns an array of profile contact profile IDs for an identified profile
     *
     * @param integer $profile_id
     * @return array
     */
    public function getProfileContactProfiles($profile_id = null) {
      global $db;

      if (is_numeric($profile_id)) {
        if ($pcpdata = $db->query_fetch_assoc("SELECT `id` FROM `profile_contact_profile` WHERE `profile_id` = '{$profile_id}'", true)) {
          return $pcpdata;
        }
        else {
          $this->addError("getProfileContactProfiles() - An error occurred during retrieval of the profile contact profile data (ID = {$profile_id}):");
        }
      }

      return false;
    }

    /**
     * Update an identified profile contact profile record
     *
     * @param mixedarray $data
     * @param integer $profile_contact_profile_id
     * @param Database $db object
     * @return boolean
     */
    public function updateProfileContactProfile($data = null, $profile_contact_profile_id = null) {
      global $db;

      if ($data && is_numeric($profile_contact_profile_id)) {
        $pcpdata = $db->query_fetch_assoc("SELECT `contact_profile_id` FROM `profile_contact_profile` WHERE `id` = '{$profile_contact_profile_id}'", false);

        if ($this->updateProfile($data, $pcpdata["contact_profile_id"])) {
          return true;
        }
        else {
          $this->addError("updateProfileContactProfile() - An error occurred during update of contact profile data (ID = {$profile_contact_profile_id}):");
          $this->addError("updateProfileContactProfile() - DATA = ".print_r($data, true));
        }
      }

      return false;
    }

    /**
     * Deletes an identified profile's contact profiles
     *
     * @param integer $profile_id
     * @param Database $db object
     * @return boolean
     */
    public function deleteProfileContactProfiles($profile_id = null) {
      global $db;

      $contact_profiles = $db->query_fetch_assoc("SELECT `contact_profile_id` FROM `profile_contact_profile` WHERE `profile_id` = '{$profile_id}'");

      foreach ($contact_profiles as $contact_profile) {
        if (!$this->deleteProfile($contact_profile["id"])) {
          $this->addError("deleteProfileContactProfiles() - An error occurred during contact profile deletion (ID = {$profile_id}).");

          return false;
        }
      }

      return true;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Adds an error message to the local error message queue, setting the error flag
     *
     * @param string $emessage
     */
    private function addError($emessage) {
      $this->errors[] = $emessage;
      $this->error = true;
    }
  }
?>