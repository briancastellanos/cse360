<?php
define("PAGE_CONTENT_INSERT_AFTER"  , 0);
define("PAGE_CONTENT_INSERT_BEFORE" , 1);
define("PAGE_CONTENT_INSERT_REPLACE", 2);

/**
 * Create an object that can read an html template and assign content into those tokens.
 * Renders a page when complete.
 * @author  Brian Castellanos, Inexo
 * @version 1.1
 * @since   2011-10-24 Renders tokens with hyphens
 * @since   2009-07-01 Inital Version
 * @package templating
 */
class Page {
    public $version = "1.1";

    /**
     * The name of the current template being used
     *
     * @var string
     */
    public $page_template;
    /**
     * An array containing the values that will replace the token placeholders.
     *
     * @var array
     */
    public $page_content;
    /**
     * An array containing the limiters and delimiters of token names.
     *
     * @var array
     */
    public $page_tokens=array("limiter"=>"[!","delimiter"=>"!]");
    /**
     * The absolute path to the template being used
     *
     * @var string
     */
    public $page_template_path;

    /**
     * Create a new page object
     *
     * @param string name portion of the file tpl.name.php
     * @param string the absolute path to the file, uses TPL_ROOT if null
     */
    public function __construct($default_template="blank",$path=null){
        $this->page_template      = $default_template;
        $this->page_template_path = $path;
    }

    /**
     * Alias for __construct, added in for support functionality.
     *
     * @param string $default_template
     * @param string $path
s     */
    public function Page($default_template,$path){
        $this->__construct($default_template,$path);
    }

    /**
     * Read the template file and return its contents.
     *
     * @param string $template
     * @param string $path
     * @return mixed
     */
    public function load_template($template,$path=null){
        if($path==null){
            $path=TPL_ROOT;
        }
        $file=$path."tpl.$template.php";
        if(!file_exists($file)|| !is_readable($file)){
            if(!file_exists($file)){
                echo "File doesn't exist";
            }
            if(!is_readable($file)){
                echo "File is not readable";
            }
            return false;
        }else{
            ob_start();
            include($file);
            return ob_get_clean();
        }
    }
    /**
     * Load the tokens from within the template content
     *
     * @param string $subject
     * @param string $pattern
     * @return mixed
     */
    public function fetch_tokens($subject,$pattern='/\[\![a-zA-Z0-9_\-]*\!\]/'){
        preg_match_all($pattern,$subject,$matches);
        if(is_array($matches)){
            return $matches[0];
        }
        else{
            return false;
        }
    }
    /**
     * Render the page to the browser, if template text is supplied, use that as the template instead of an actual template file.
     *
     * @param string $text
     */
    public function display_page($text=null){
        if($text==null){
            $text = $this->load_template($this->page_template,$this->page_template_path) or die("$this->page_template is not a valid template in {$this->page_template_path}tpl.$this->page_template.php");
        }
        $page_sections = $this->fetch_tokens($text);

        foreach($page_sections as $section){
            $sct  = preg_replace("/[^a-zA-Z0-9_\-]*/","",$section);
            $text = str_replace("$section",$this->page_content["$sct"],$text);
        }
        echo $text;
    }
    /**
     * Replace a tokens position with text
     *
     * @param string $content
     * @param string $section
     * @param integer $insert
     */
    public function content($content,$token_name="page_content",$insert=null){
        if($insert=="before" || $insert==PAGE_CONTENT_INSERT_BEFORE){
            $this->page_content[$token_name]=$content.$this->page_content[$token_name];
        }
        else if($insert=="replace" || $insert == PAGE_CONTENT_INSERT_REPLACE){
            $this->page_content[$token_name]=$content;
        }
        else{
            $this->page_content[$token_name].=$content;
        }
    }
    /**
     * Recursively add content to an array containing token/content pairs
     *
     * @param array $sections
     */
    public function multi_content($sections){
        foreach($sections as $section=>$parts){
            if(!is_array($parts)){$parts=array($parts);}
            $this->content($parts[0],$section,$parts[1],$parts[2]);
        }
    }
    /**
     * Create a string of links based on an array containing href/display pairs
     *
     * @param array $page_links
     * @return string
     */
    public function glue_links($page_links=array()){
        foreach($page_links as $pg=>$href){
            if(!is_array($href)){
                $links.="<a href='$href'>$pg</a>";
            }
            else{
                $links.="<a href='{$href['href']}' target='{$href['target']}'>$pg</a>";
            }
        }
        return $links;
    }
}