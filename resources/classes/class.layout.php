<?php
/**
 * Creates a select box with colored and indented teir levels and child/parent support.
 * @author Joe Colburn, Inexo
 * @package Multi-level select box with child/parent relationships
 * @version 1.0
 * @since 2010-08-18 Initial version notes:
 * Because teir headers can be seleected by the user (and shouldn't be),
 * an error should be thrown when processing if the selected value
 * is equal to '---'.
 *
 * To color code list items and headers, add two css classes for each
 * level similar to what is shown below:
 *
 * <style type="text/css">
 * option.head1 {background-color:black;color:white;}
 * option.item1 {background-color:white;color:black;}
 * option.head2 {background-color:#333333;color:white;}
 * option.item2 {background-color:white;color:black;}
 * option.head3 {background-color:#555555;color:white;}
 * option.item3 {background-color:white;color:black;}
 * option.head4 {background-color:#777777;color:white;}
 * option.item4 {background-color:white;color:black;}
 * </style>
*/
  class layout {
    public $version = "1.0";
    public function __construct() {
    }

    public function format_for_id($id) {
      $id = $this->hentities($id);
      $id = str_replace("[", "_", $id);
      $id = str_replace("]", "", $id);
      return ($id);
    }

    /**
     * @desc Will properly display hentities by first decoding entities, then re-encoding them.
     * May have issues if the $string is not encoded already(meaning a a literal &amp; will need to be entered as &amp;amp;,
     * but It's no different than a person typing &amp; into a search box and having the same value echoed'.
     * */
    private function hentities($string){
      return htmlentities($string,ENT_QUOTES);
    }

    /**
     * Returns the concatenation of a series of defined attributes passed as a key->value array
     *
     * @param array $attributes - defined array of attribute key=>value pairs
     * @param string $type - type of attributes to be concatenated ("element" or "ckeditor"; default = "element")
     * @return string - concatenated string of attributes
     */
    private function concat_attributes($attributes, $type = "element") {
      if (!is_array($attributes)) return false;

      switch ($type) {
        case "ckeditor": // format attributes for ckeditor config settings
          foreach ($attributes as $att => $value){
            if ($strAttributes) $strAttributes .= ", ";

            if (substr($value, 0, 1) != "[" && substr($value, strlen($value) - 1, 1) != "]") {
              if (is_numeric($value) || in_array($value, array("true", "false")) || substr($value, 0, 8) == "CKEDITOR") {
                $strAttributes .= $this->hentities($att).":".$value;
              }
              else {
                $strAttributes .= $this->hentities($att).":'".$value."'";
              }
            }
            else {
              $strAttributes .= $this->hentities($att).":[".$value."]";
            }
          }
          break;
        case "element": // format attributes for a form element
          // break intentionally omitted
        default:
          foreach ($attributes as $att => $value){
            $strAttributes .= $this->hentities($att).'="'.$value.'" ';
          }
      }

      return $strAttributes;
    }

    /**
     * Build and return HTML form dropdown select element
     *
     * @param  string $formName - name/id of the element
     * @param  array  $array - array of key->value pairs representing selectable dropdown values
     * @param  string $default - default selected value to be selected
     * @param  array  $options - key->value pair array of element attributes
     * @return string - HTML code for form element
     */
    public function form_dropdown($formName, $array, $default, $options=array()) {
        $optionso = $options;
        if ($optionso["readonly"] == "extended") {
          unset($optionso["readonly"]);
          $optionso["onfocus"] = "javascript: this_index = this.selectedIndex;";
          $optionso["onchange"] = "javascript: this.selectedIndex = this_index;";
        }
        if ($optionso["tooltips"]) {
          $tt_form = $optionso["tooltips"]["form"];
          $tt_tips = $optionso["tooltips"]["tips"];
          unset($optionso["tooltips"]);
        }
#        if (!security::can("update", "element")) $optionso["disabled"] = "disabled";
        $optionso = $this->concat_attributes($optionso);
	      $out = "<select id=\"".$this->format_for_id($formName)."\" name=\"".$this->hentities($formName)."\" $optionso>";
	      $out .= $this->form_dropdown_traverse($array, $default, 1);
	      $out .= "</select>";

	      if (is_array($tt_tips)) {
          $out .= "<script>";

	        foreach ($tt_tips as $key => $value) {
            $out .= "document.{$tt_form}.".$this->format_for_id($formName).".options[{$key}].setAttribute(\"title\", \"{$value}\");";
	        }

          $out .= "</script>";
	      }

        return ($out);
    }

    /**
     * Traverses dropdown key->value array recursively to build dropdown selection
     * values and option groups
     *
     * @param  array  $array - array of key->value pairs representing selectable dropdown values
     * @param  string $default - default selected value to be selected
     * @param  array  $options - key->value pair array of element attributes
     * @return string - HTML code for form element
     */
    private function form_dropdown_traverse_old($array=array(), $default=null, $options=array()) {

      while (list($key, $val) = each($array)) {
        if (is_array($val)) {
          $out .= "<optgroup label=\"".$this->hentities($key)."\">";
          $out .= $this->form_dropdown_traverse($val, $default, $options);
          $out .= "</optgroup>\n";
        }
        else {
          $style = "";
          if ($options["ostyle"]) $style = "style='{$options["ostyle"]}'";
          if ($options["readonly"] == "extended") {
            if ($key != $default) {
              $style = " style='color: #CCCCCC;'";
            }
          }
		      $out .= "<option value=\"".$this->hentities($key)."\"$style";
 		      if ($key == $default) $out .= " selected";
	        $out .= ">".$this->hentities($val)."</option>\n";
        }
      }
      return ($out);
    }

    /**
     * Traverses dropdown key->value array recursively to build dropdown selection
     * values and option groups
     *
     * @param  array  $array - array of key->value pairs representing selectable dropdown values
     * @param  string $default - default selected value to be selected
     * @param  integer $level - teir level
     * @return string - HTML code for form element
     */
    private function form_dropdown_traverse_old2($array=array(), $default=null, $level=1) {

    	$level = intval($level);
    	$padding = '';
    	$x = $level - 1;
    	while ($x > 0) {
    		$padding .= '&nbsp;&nbsp;&nbsp;&nbsp;';
    		$x--;
    	}

      while (list($key, $val) = each($array)) {
        if (is_array($val)) {
          $out .= "<option class=\"head".$level."\" value=\"---\">".$padding.$this->hentities($key)."</option>\n";
          $out .= $this->form_dropdown_traverse($val, $default, $level+1);
        } else {
		      $out .= "<option value=\"".$this->hentities($key)."\" class=\"item".$level."\"";
 		      if ($key == $default) $out .= " selected";
	        $out .= ">".$padding.$this->hentities($val)."</option>\n";
        }
      }
      return ($out);
    }

    /**
     * Traverses dropdown key->value array recursively to build dropdown selection
     * values and option groups
     *
     * @param  array  $array - array of key->value pairs representing selectable dropdown values
     * @param  string $default - default selected value to be selected
     * @param  integer $level - teir level
     * @return string - HTML code for form element
     */
    private function form_dropdown_traverse($array=array(), $default=null, $level=1) {

    	$level = intval($level);
    	$padding = '';
    	$x = $level - 1;
    	while ($x > 0) {
    		$padding .= '&nbsp;&nbsp;&nbsp;&nbsp;';
    		$x--;
    	}

      while (list($key, $val) = each($array)) {
        if (count($val) > 1) {
          $out .= "<option class=\"head".$level."\" value=\"---\">".$padding.$this->hentities($val['name'])."</option>\n";
          unset($val['name']);
          $out .= $this->form_dropdown_traverse($val, $default, $level+1);
        } else {
		      $out .= "<option value=\"".$this->hentities($key)."\" class=\"item".$level."\"";
 		      if ($key == $default) $out .= " selected";
	        $out .= ">".$padding.$this->hentities($val['name'])."</option>\n";
        }
      }
      return ($out);
    }

    /**
     * Traverses a table to build an array of parent and child elements
     *
     * @param  string $table - name of the table to traverse
     * @param  string $id_field - name of the elements id field
     * @param  string $parent_field - name of the parent element identifying field
     * @param  string $name_field - name of field that stores the child name
     * @param  integer $start - the starting parent ID ... will pull all children and on down
     */
    public function parent_child_array_old($table=null, $id_field=null, $parent_field=null, $name_field=null, $start=null) {
    	global $db;

    	$parent = $start;
			$array = $db->create_array_values("SELECT {$id_field}, {$name_field} FROM {$table} WHERE {$parent_field}='{$parent}' AND `status` = 'active' ORDER BY `display_order`", $id_field, $name_field);

			// Loop through the child elements to find grandchild elements
    	while (list($key, $val) = each($array)) {
    		$tmp = $this->parent_child_array($table, $id_field, $parent_field, $name_field, $key);
    		if (count($tmp) > 0) {
    			unset($array[$key]);
    			$array[$val] = $tmp;
    		}
    	}
      return ($array);
    }

    /**
     * Traverses a table to build an array of parent and child elements
     *
     * @param  string $table - name of the table to traverse
     * @param  string $id_field - name of the elements id field
     * @param  string $parent_field - name of the parent element identifying field
     * @param  string $name_field - name of field that stores the child name
     * @param  integer $start - the starting parent ID ... will pull all children and on down
     * @param  boolean $active - if TRUE, only select records with active status (default=TRUE)
     */
    public function parent_child_array($table=null, $id_field=null, $parent_field=null, $name_field=null, $start=null, $active=true){
      global $db;

    	$parent = $start;

      $sql="SELECT {$id_field}, {$name_field} FROM {$table} WHERE {$parent_field}='{$parent}' ";

      if ($active) {
        $sql .= "AND status = 'active'";
      }

      $sql .= "ORDER BY `display_order`";

      $db->Query($sql,false);
      while($r=$db->fetch_assoc()){
          $child[$r["{$id_field}"]]["name"] = $r["{$name_field}"];
          $c = $this->parent_child_array($table, $id_field, $parent_field, $name_field, $r["{$id_field}"]);
          if(is_array($c)){
              $child[$r["{$id_field}"]]+=$c;
              unset($c);
          }
      }
      $db->free_result();

      return $child;
    }
  }
?>