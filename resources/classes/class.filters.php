<?php
/**
 * Data validation via emulation of the Filter functions available with the filters extension.
 * @author Brian Castellanos
 * @since 2009-07-01
 * @version 1.1
 * @package validation
 */
class Filters{
    public $version = "1.1";
    /**
     * True if the extenstion is loaded on the server
     *
     * @var boolean
     */
    public $filter_extension_loaded;
    /**
     * List of the avalible filter types in the class.
     *
     * @var array
     */
    public $filter_list;
    /**
     * List of the vailable flag types available
     *
     * @var array
     */
    public $filter_flags;
    /**
     * Inputs taken from the superglobals
     *
     * @var array
     */
    public $filter_input;
    /**
     * Errors from the last executed instance of filter_array
     *
     * @var mixed
     */
    public $filter_errors;

    /**
     * Create a new validation filter
     */
    public function __construct(){
        $this->filter_extension_loaded=false; //extension_loaded("filter"); set to false manually
        if(!$this->filter_extension_loaded){
            $this->filter_list  = array(
            'int'=>1,
            'boolean'=>2,
            'float'=>3,
            'validate_regexp'=>4,
            'validate_url'=>5,
            'validate_email'=>6,
            'validate_ip'=>7,
            'string'=>8,
            'stripped'=>9,
            'encoded'=>10,
            'special_chars'=>11,
            'unsafe_raw'=>12,
            'email'=>13,
            'url'=>14,
            'number_int'=>15,
            'number_float'=>16,
            'magic_quotes'=>17,
            'callback'=>18,
            );

            $this->filter_input=array(
            "post"=>1,
            "get"=>2,
            "cookie"=>3,
            "env"=>4,
            "server"=>5,
            "session"=>6,
            "request"=>7,
            );

            $this->filter_flags=array(
            "search"=>99
            );

            DEFINE('FILTER_VALIDATE_INT'         ,$this->filter_list["int"]);
            DEFINE('FILTER_VALIDATE_BOOLEAN'     ,$this->filter_list["boolean"]);
            DEFINE('FILTER_VALIDATE_FLOAT'       ,$this->filter_list["float"]);
            DEFINE('FILTER_VALIDATE_REGEXP'      ,$this->filter_list["validate_regexp"]);
            DEFINE('FILTER_VALIDATE_URL'         ,$this->filter_list["validate_url"]);
            DEFINE('FILTER_VALIDATE_EMAIL'       ,$this->filter_list["validate_email"]);
            DEFINE('FILTER_VALIDATE_IP'          ,$this->filter_list["validate_ip"]);
            DEFINE('FILTER_SANITIZE_STRING'      ,$this->filter_list["string"]);
            DEFINE('FILTER_SANITIZE_STRIPPED'    ,$this->filter_list["stripped"]);
            DEFINE('FILTER_SANITIZE_ENCODED'     ,$this->filter_list["encoded"]);
            DEFINE('FILTER_SANITIZE_EMAIL'       ,$this->filter_list["email"]);
            DEFINE('FILTER_SANITIZE_URL'         ,$this->filter_list["url"]);
            DEFINE('FILTER_SANITIZE_INT'         ,$this->filter_list["number_int"]);
            DEFINE('FILTER_SANITIZE_FLOAT'       ,$this->filter_list["number_float"]);
            DEFINE('FILTER_SANITIZE_MAGIC_QUOTES',$this->filter_list["magic_quotes"]);
            DEFINE('FILTER_CALLBACK'             ,$this->filter_list["callback"]);

            DEFINE('INPUT_POST'                  ,$this->filter_input["post"]);
            DEFINE('INPUT_GET'                   ,$this->filter_input["get"]);
            DEFINE('INPUT_COOKIE'                ,$this->filter_input["cookie"]);
            DEFINE('INPUT_ENV'                   ,$this->filter_input["env"]);
            DEFINE('INPUT_SERVER'                ,$this->filter_input["server"]);
            DEFINE('INPUT_SESSION'               ,$this->filter_input["session"]);
            DEFINE('INPUT_REQUEST'               ,$this->filter_input["request"]);

            DEFINE('FILTER_FLAG_SEARCH_READY'    ,$this->filter_flags["search"]);
        }
        else{
            $this->filter_list=filter_list();
        }
    }
    /**
     * Checks if variable of specified type exists
     * @param integer $type
     * @param string $variable_name string
     * @return boolean
     */
    public function filter_has_var($type,$variable_name){
        if($this->filter_extension_loaded){
            return filter_has_var($type,$variable_name);
        }
        else{
            switch($type){
                case 1:
                    return (array_key_exists($variable_name,$_POST))?true:false;
                    break;
                case 2:
                    return (array_key_exists($variable_name,$_GET))?true:false;
                    break;
                case 3:
                    return (array_key_exists($variable_name,$_COOKIE))?true:false;
                    break;
                case 4:
                    return (array_key_exists($variable_name,$_ENV))?true:false;
                    break;
                case 5:
                    return (array_key_exists($variable_name,$_SERVER))?true:false;
                    break;
                case 6:
                    return (array_key_exists($variable_name,$_SESSION))?true:false;
                    break;
                case 7:
                    return (array_key_exists($variable_name,$_REQUEST))?true:false;
                    break;
                default:
                    return false;
                    break;
            }
        }
    }
    /**
     * Checks if a superglobal has an entry named variable_name
     *
     * @param integer $type
     * @param string $variable_name
     * @return boolean
     */
    public function filter_has_val($type,$variable_name){
        if($this->filter_extension_loaded){
            return filter_has_var($type,$variable_name);
        }
        else{
            switch($type){
                case 1:
                    return $_POST["$variable_name"];
                    break;
                case 2:
                    return $_GET["$variable_name"];
                    break;
                case 3:
                    return $_COOKIE["$variable_name"];
                    break;
                case 4:
                    return $_ENV["$variable_name"];
                    break;
                case 5:
                    return $_SERVER["$variable_name"];
                    break;
                case 6:
                    return $_SESSION["$variable_name"];
                    break;
                case 7:
                    return $_REQUEST["$variable_name"];
                    break;
                default:
                    return false;
                    break;
            }
        }
    }
    /**
     * Returns the filter ID belonging to a named filter
     * @param string $filtername string
     * @return integer mixed
     */
    public function filter_id($filtername){
        if($this->filter_extension_loaded){
            return filter_id($filtername);
        }
        else{
            return $this->filter_list["$filtername"];
        }
    }
    /**
     * Gets multiple variables from outside PHP and optionally filiters them
     * @param integer $type
     * @param mixed $definition
     * @return mixed
     */
    public function filter_input_array($type,$definition){
        if($this->filter_extension_loaded){
            return filter_input_array($type,$definition);
        }
        else{
            switch($type){
                case 1:
                    $array  = $_POST;
                    break;
                case 2:
                    $array  = $_GET;
                    break;
                case 3:
                    $array  = $_COOKIE;
                    break;
                case 4:
                    $array  = $_ENV;
                    break;
                case 5:
                    $array  = $_SERVER;
                    break;
                case 6:
                    $array  = $_SESSION;
                    break;
                case 7:
                    $array  = $_REQUEST;
                    break;
                default:
                    return false;
                    break;
            }

            foreach($array as $key=>$value){
                if(is_array($definition)){
                    $filter  = $definition["$key"]["filter"];
                    $options = $definition["$key"]["options"];
                    $flags   = $definition["$key"]["flags"];
                }
                else{
                    $filter  = $definition;
                }

                if($this->filter_has_var($type,$key)){
                    $filter_data = $this->filter_input($type,$key,$filter,array("options"=>$options,"flags"=>$flags));
                    $filtered[]  = $filter_data;
                }
                else{
                    $filtered[]  = false;
                }
            }
            return $filtered;
        }
    }
    /**
	 * Gets variable from outside PHP and optionally filters it
	 * @param integer $type
	 * @param string $variable_name
	 * @param integer $filter
	 * @param mixed $options
	 * @return mixed
	 */
    public function filter_input($type,$variable_name,$filter=null,$options=null){
        if($this->filter_extension_loaded){
            return filter_input($type,$variable_name,$filter,$options);
        }
        else{
            if($this->filter_has_var($type,$variable_name)){
                return $this->filter_var($this->filter_has_val($type,$variable_name),$filter,$options);
            }
            else{
                return null;
            }
        }
    }
    /**
    * Uses the filter to validate variable with a specified type
    * @param string $variable
    * @param integer $filter
    * @param array  $options
    * @return mixed
    */
    public function filter_var($variable,$filter=null,$options=null){
        if($this->filter_extension_loaded){
            return filter_var($variable,$filter,$options);
        }
        else{
            if(is_array($variable)){
                foreach($variable as $id=>$var){
                    $variable[$id]=$this->filter_var($var,$filter,$options);
                }
                return $variable;
            }
            switch($filter){
                case FILTER_VALIDATE_INT:
                case 1:
                    if(is_int($variable)){

                    }elseif(!preg_match('/^[0-9]+$/',$variable)){
                        return false;
                    }

                    if(is_array($options)){
                        if($options["options"]["min_range"]!=''){
                            if(!is_integer($options["options"]["min_range"])){
                                trigger_error("max_range is not a valid integer",E_USER_ERROR);
                                return false;
                            }
                            elseif($variable<$options["options"]["min_range"]){
                                return false;
                            }
                        }
                        if($options["options"]["max_range"]!=''){
                            if(!is_integer($options["options"]["max_range"])){
                                trigger_error("max_range is not a valid integer",E_USER_ERROR);
                                return false;
                            }
                            elseif($variable>$options["options"]["max_range"]){
                                return false;
                            }
                        }
                    }
                    return (integer)$variable;
                    break;
                case FILTER_VALIDATE_BOOLEAN:
                case 2:
                    if($variable===true){
                        return true;
                    }
                    if($variable===1){
                        return true;
                    }
                    if(gettype($variable)=='string'){
                        if(in_array(strtoupper($variable),array("1","YES","TRUE","ON"))){
                            return true;
                        }
                    }
                    return false;
                    break;
                case FILTER_VALIDATE_FLOAT:
                case 3:
                    if(!preg_match('/^[0-9]+\.?[0-9]*$/',$variable)){
                        return false;
                    }
                    else{
                        return $variable;
                    }
                    break;
                case FILTER_VALIDATE_REGEXP:
                case 4:
                    $pattern=$options["options"]["regexp"];
                    if($pattern==''){
                        return false;
                    }
                    if(preg_match($pattern,$variable)){
                        return $variable;
                    }
                    else{
                        return false;
                    }
                    break;
                    #'validate_url'=>5,
                case FILTER_VALIDATE_EMAIL:
                case 6:
                    if(!preg_match('/^([0-9a-zA-Z_\-.]+)+@([0-9a-zA-Z_\-\.]+)+\.([a-zA-Z]{2,4})$/',$variable)){
                        return false;
                    }
                    else{
                        return $variable;
                    }
                    break;

                case FILTER_VALIDATE_IP:
                case 7:
                    if(!preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/',$variable)){
                        return false;
                    }
                    else{
                        return $variable;
                    }
                    break;
                case FILTER_SANATIZE_STRING:
                case 8:
                    if(get_magic_quotes_gpc()==1){
                        $variable=stripslashes($variable) ;
                    }

                    $variable = strip_tags($variable);
                    switch($options){
                        #THESE ARE WHERE THE FLAGS WILL GO..HAVEN'T BUILT THEM YET.
                        case FILTER_FLAG_SEARCH_READY:
                        case 99:
                            $variable=ereg_replace("[^[:space:]A-Za-z0-9_-]","",$variable);
                            break;
                        default:
                    }
                    return $variable;
                    break;
                case 9: //stripped9,
                case 10://'encoded'=>10,
                case 11://'special_chars'=>11,
                case 12://'unsafe_raw'=>12,
                case 13://'email'=>13,
                case 14://'url'=>14,
                case 15://'number_int'=>15,
                case 16://'number_float'=>16,
                case 17://'magic_quotes'=>17,
                case 18://'callback'=>18,*/
                default:
                    return $variable;
            }
        }
    }
    /**
     * Gets multiple variables and optionally filters them.
     * This function is useful for retrieving many values without repetitively calling filter_var(), data - an array with string keys contining the data to filter.
     * @param array $data An array defining the arguments. A valid key is a string containing a variable name and a valid value is either a filter type, or an array optionally specifying the filter, flags and options. If the value is an array, valid keys are filter which specifies the filter type, flags which specifies any flags that apply to the filter, and options which specifies any options that apply to the filter.
     * @param mixed $definition this parameter can be an integer holding a filter constant. All values in the input array are filtered by this filter.
     * @return mixed
     */
    public function filter_var_array($data,$definition){
        if(!is_array($data)){
            return false;
        }
        else{
            foreach ($data as $id=>$value) {
                if(is_array($definition)){
                    $filter = $definition[$id];
                    if(is_array($definition[$id])){
                        $filter  = $definition[$id]['filter'];
                        $options = $definition[$id]['options'];
                        $flags   = $definition[$id]['flags'];
                    }
                    else{
                        $options = null;
                        $flags   = null;
                    }
                }
                else{
                    $filter  = $definition;
                    $options = null;
                    $flags   = null;
                }
                $values[$id] = $this->filter_var($value,$filter,(is_array($flags) || is_array($options) ? array("options"=>$options,"flags"=>$flags) : $options));
            }
            return $values;
        }
    }
    /**
     * Validates elements of a data array against a validation array. Handles error lableing as well.
     * @param array $form_data
     * @param array $validation_data
     * @return $val mixed
     * @example $form_data = array("test_email"=>"test@me.com"); $validation_data = array("test_email" => array("display_name"=>"Email Address", "data_type"=>"email","max_length"=>"100"));
     */
    public function filter_array($form_data=null,$validation_data=null){
        if(!is_array($form_data)){
            $form_data=array();
        }
        if(!is_array($validation_data)){
            return false;
        }
        else{
            foreach($validation_data as $element=>$constraints){
                $strErrValidation = null;
                $strErrLength     = null;
                $value            = $form_data["$element"];

                #Simple validation on field length;
                $intLength=$this->check_length($value,array("min_length"=>$constraints["'min_length'"],"max_length"=>$constraints["'max_length'"],"ext_length"=>$constraints["'ext_length'"]));
                if($intLength!==true){
                    switch($intLength){
                        case -1:
                            $strErrLength = "This field requires a minimum length of {$constraints["'min_length'"]} characters.";
                            break;
                        case -2:
                            $strErrLength = "This field has a maximum length of {$constraints["'min_length'"]} characters.";
                            break;
                        case -3:
                            $strErrLength = "This field must be between {$constraints["'min_length'"]} and {$constraints["'max_length'"]} characters.";
                        case -4:
                        case -5:
                        case -6:
                        case -7:
                            $strErrLength = "This field must be exactly {$constraints["'ext_length'"]} characters";
                    }
                }

                #Validate Data Type
                $strFieldDisplay=$constraints["'display_name'"]==""?$element:$constraints["'display_name'"];
                $filter=$constraints["filter"];
                if($filter==''){
                    $filter=strtoupper($constraints["'data_type'"]);
                }


                switch($filter){
                    case FILTER_VALIDATE_EMAIL:
                        $filtered_val=$this->filter_var($value,FILTER_VALIDATE_EMAIL,$constraints["flag"]);
                        if($filtered_val===false){
                            $strErrValidation="$value is not a valid email address";
                        }
                        break;
                    case FILTER_VALIDATE_INT:
                        $filtered_val=$this->filter_var($value,FILTER_VALIDATE_INT,$constraints["flag"]);
                        if($filtered_val===false){
                            $strErrValidation="This Field is not an integer";
                        }
                        break;
                    case FILTER_VALIDATE_FLOAT:
                        $filtered_val=$this->filter_var($value,FILTER_VALIDATE_FLOAT);
                        if($filtered_val===false){
                            $strErrValidation="This Field is not a float";
                        }
                        break;
                    case "DATE":
                        switch($constraints["'date_format'"]){
                            case "d-m-y":
                            case "m-d-y":
                                $strPattern='/^[0-9]{1,2}(\-|\/)[0-9]{1,2}(\-|\/)[0-9]{2}$/';
                                break;
                            case "Y-m-d":
                                $constraints["'date_format'"]='yyyy-mm-dd';
                                $strPattern='/^[0-9]{4}(\-|\/)[0-9]{1,2}(\-|\/)[0-9]{1,2}$/';
                                break;
                            default:
                                $constraints["'date_format'"]='mm-dd-yyyy';
                                $strPattern='/^[0-9]{1,2}(\-|\/)[0-9]{1,2}(\-|\/)[0-9]{4}$/';
                                break;
                        }
                        if(!preg_match($strPattern,$value)){
                            $strErrValidation="This Field is not in the proper date format ({$constraints["'date_format'"]}";
                        }
                    case FILTER_VALIDATE_BOOLEAN:
                        $filtered_val=$this->filter_var($value,FILTER_VALIDATE_BOOLEAN,$constraints["flag"]);
                        if($filtered_val===false){
                            $strErrValidation="The field must be a boolean value (true/false, yes/no)";
                        }
                        break;
                }
                if($strErrValidation!='' || $strErrLength){
                    $errors[]="$strFieldDisplay: $strErrValidation $strErrLength";
                }
                $values["$element"]=$filtered_val;
            }

            if(is_array($errors)){
                $this->filter_errors=$errors;#=implode("<br />",$errors);
            }
            else{
                $this->filter_errors=null;
            }
            return $values;
        }
    }
    /**
     *  Displays errors from the last instance of filter_array;
     *
     */
    public function filter_errors(){
        if(!is_array($this->filter_errors)){
            $this->filter_errors=array();
        }
        return implode("<br />",$this->filter_errors);
    }
    /**
     * Checks to see if the string is within max/min limits required.
     *
     * @param string $str
     * @param array $args
     * @return mixed
     */
    public function check_length($str,$args=null){
        $min   = $args["'min_length'"];
        $max   = $args["'max_length'"];
        $exact = $args["'ext_length'"];

        if($min!=null){
            if(strlen($str)<$min){
                $err-=1;
            }
        }
        if($max!=null){
            if(strlen($str)>$max){
                $err-=2;
            }
        }
        if($exact!=null){
            if(strlen($str)!=$exact){
                $err-=4;
            }
        }

        if($err<0){
            return $err;
        }
        else{
            return true;
        }
    }
}