<?php
/**
 * @version 1.3.1
 * @since   v1.3.1 2011-11-18 Filters display 100 Characters
 * @since   v1.3 2010-04-27 Added support for datagrid 4.4
 * @since   v1.2 2009-12-28 added support for optiongroups
 *
 *
 */

define("FILTER_CONJUNCTION_OR",0);
define("FILTER_CONJUNCTION_AND",1);

define("FILTER_COMPARITIVE_EQUAL_TO",0);
define("FILTER_COMPARITIVE_NOT_EQUAL_TO",1);
define("FILTER_COMPARITIVE_EQ_GREATER_THAN",2);
define("FILTER_COMPARITIVE_GREATER_THAN",3);
define("FILTER_COMPARITIVE_LESS_THAN",4);
define("FILTER_COMPARITIVE_LESS_THAN_EQ_TO",9);
define("FILTER_COMPARITIVE_LIKE_BEFORE",5);
define("FILTER_COMPARITIVE_LIKE_AFTER",6);
define("FILTER_COMPARITIVE_LIKE",7);
define("FILTER_COMPARITIVE_IS",8);
define("FILTER_COMPARITIVE_IS_NOT",10);

/**
 * The Datgrid Filter class creates a new text box or drop down list based
 * filter that is associated to a datagrid to create a filter WHERE condition for a
 * query
 *
 */
abstract class AbstractDatagridFilter {

  /**
     * Set the linked field name, or use "`table`.`field` syntax
     * @var string
     */
  public $db_field_name;
  public $db_hash;
  public $value;
  public $comparison;
  /**
     * The conjution defines what type of conjunction should be used - FILTER_CONJUNCTION_OR or FILTER_CONJUNCTION_AND
     *
     * @var int
     */
  public $conjunction;
  /**
     * Set weather or not add quotes around the value (e.g. '$value') when building the statement.
     *
     * @var bool
     */
  public $quote_value;
  public $add_slashes;
  /**
     * Set wheather to skip this field in the dump if there is no value supplied.
     *
     * @var unknown_type
     */
  public $skip_on_null=true;

  /**
     * Instantiate a new datagrid filter. $field_name is the name of the field in the query that you would like to add a filter to
     * $conjuction is the
     *
     * @param string $field_name
     * @param int $conjuction
     * @param int $comparison_operator
     */
  public function __construct($field_name,$conjuction,$comparison_operator,$options=null){
    $f=explode(".",$field_name);
    if(count($f)==1){
      $field=str_replace("`","",$f[0]);
      $table=null;
    }else{
      $field=str_replace("`","",$f[1]);
      $table=str_replace("`","",$f[0]);
    }
    $this->conjunction=$conjuction;
    $this->setDbField($field,$table);
    $this->comparison=$comparison_operator;
    $this->quote_value=true;
    $this->add_slashes=true;
    $this->db_hash=calcPassphrase($this->db_field_name);


    if(isset($_REQUEST['dgfilter'][$this->db_hash])){
      $this->value=(get_magic_quotes_gpc()?stripslashes($_REQUEST['dgfilter'][$this->db_hash]):$_REQUEST['dgfilter'][$this->db_hash]);
    }else{
      if($_SESSION['dgfilter'][$this->db_hash]==''){
        $this->value='';
      }else{
        $this->value=$_SESSION['dgfilter'][$this->db_hash];
      }
    }

    $_SESSION['dgfilter'][$this->db_hash]=$this->value;
    #    $this->value=(get_magic_quotes_gpc()?stripslashes($_REQUEST['dgfilter'][$this->db_hash]):$_REQUEST['dgfilter'][$this->db_hash]);
  }

  public function setDbField($field,$table=null){
    if($field==null){
      $bt = debug_backtrace();

        if(!stristr($bt[0]['file'],basename(__FILE__))){
          $bt = $bt[0];
        }else{
          $bt = $bt[1];
        }

        $file     = str_replace(DOC_ROOT,"",$bt['file']);
        $line     = $bt['line'];
        $function = $bt['function'];
      user_error("DatagridFilter::{$function}() call failed on line {$line} in {$file}. A field name must be provided for the filter. The value provided was: <pre style='display:inline;'>".var_export($field,true)."</pre>",E_USER_WARNING);

    }
    $field=($table!=null?"`$table`.":"")."`$field`";
    $this->db_field_name=$field;
  }

  function concat_attributes($attributes){
    if(!is_array($attributes)){
      return false;
    }
    foreach($attributes as $att=>$value){
      $strAttributes .=htmlentities($att,ENT_QUOTES).'="'.htmlentities($value,ENT_QUOTES).'" ';
    }
    return $strAttributes;
  }

}
/**
 * Create a new text box filter
 *
 */
class DatagridTextFilter extends AbstractDatagridFilter{
  public function __construct($field_name,$comparison_operator=FILTER_COMPARITIVE_EQUAL_TO,$conjunction=FILTER_CONJUNCTION_OR){
    parent::__construct($field_name,$conjunction,$comparison_operator);
  }

  public function createFilter($attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $txtarea='<input type="text" name="dgfilter['.htmlentities($this->db_hash,ENT_QUOTES).']" value="'.htmlentities($this->value,ENT_QUOTES).'" '.$strAttributes.' />';
    return $txtarea;
  }
}

/**
 * Creates a drop down box filter
 *
 */
class DatagridListFilter extends AbstractDatagridFilter{
  public function __construct($field_name,$comparison_operator=FILTER_COMPARITIVE_EQUAL_TO,$conjunction=FILTER_CONJUNCTION_OR){
    parent::__construct($field_name,$conjunction,$comparison_operator);
  }

  public function createFilter($options,$attributes=null,$isNodeList = false){

    $strAttributes=$this->concat_attributes($attributes);
    if(!is_array($options))$options=array();
    if(!is_array($attributes))$attributes=array();
    #$attributes+=array("dojoType"=>"dijit.form.FilteringSelect",'autoComplete'=>"true");
    global $form;

    if(!$form instanceof Form ){
      include_class("form");
      $form = new Form(FORM_RETURN_VALUE);
    }
    $form->max_display = 100;


    if($isNodeList){
      $cbo = $form->type_select('dgfilter['.htmlentities($this->db_hash,ENT_QUOTES).']',$this->value,$options,$attributes);
    }else{
      $cbo = $form->select('dgfilter['.htmlentities($this->db_hash,ENT_QUOTES).']',$this->value,$options,$attributes);
    }

    /*        //$strAttributes=$this->concat_attributes($attributes);
    $cbo = '<select name="dgfilter['.htmlentities($this->db_hash,ENT_QUOTES).']" '.$strAttributes.'>';
    foreach($options as $val=>$dsp){
    $cbo.='<option value="'.htmlentities($val,ENT_QUOTES).'"'.($val==$this->value?' selected="selected"':null).'>'.htmlentities($dsp,ENT_QUOTES).'</option>';
    }
    $cbo.='</select>';*/

    if($this->return_mode=="echo"){
      echo $cbo;
    }
    else{
      return $cbo;
    }
  }
}



$__DATAGRID_FILTER_LEVELS=array();

class DatagridFilterLevel{
  public $family;
  public $name;
  public $datagrid_conjunction;
  public $value;

  /**
     * Create a new filter condition level to create a WHERE (condition AND condition OR (condition AND condition)))
     * relationship
     *
     * @param object $filter_obj
     * @param object $parent
     * @param int $conjunction
     */
  public function __construct(&$filter_obj,$parent=null){
    #,$conjunction=FILTER_CONJUNCTION_OR
    global $__DATAGRID_FILTER_LEVELS;
    #$this->datagrid_conjunction=$conjunction;
    if($parent!==null){
      if(!is_object($parent)){
        throw new Exception("The parent does not exist");
      }else{
        $parent->attachChild($this,$filter_obj, $this->family);
      }
    }else{
      $filter_index=count($__DATAGRID_FILTER_LEVELS);
      $__DATAGRID_FILTER_LEVELS[$filter_index]=array('filter'=>$filter_obj,"conjunction"=>$conjunction,'children'=>array());
      $this->family =& $__DATAGRID_FILTER_LEVELS[$filter_index];
    }
  }

  /**
     * Attach a new child level to the current level
     *
     * @param object $obj
     * @param object $filter_obj
     * @param array $new_family
     */
  public function attachChild($obj,&$filter_obj,&$new_family){
    $child_index=count($this->family['children']);
    $this->family['children'][$child_index]=array('filter'=>$filter_obj,'conjunction'=>$obj->datagrid_conjunction,'children'=>array());
    $new_family =& $this->family['children'][$child_index];
  }

  public function buildStatement(){
    global $__DATAGRID_FILTER_LEVELS;
    foreach($__DATAGRID_FILTER_LEVELS as $child){
      $clause.=buildClause($child);
    }
    return $clause;
  }
}


function buildClause($child){

  switch($child['filter']->conjunction){
    case FILTER_CONJUNCTION_AND:
      $clause=" AND ";
      break;
    case FILTER_CONJUNCTION_OR:
      $clause=" OR ";
      break;
  }

  $clause.=" ({$child['filter']->db_field_name}";
  $value=($child['filter']->quote_value && ($child['filter']->comparison < 4) ?"'":"").
  ($child['filter']->add_slashes?addslashes($child['filter']->value):$child['filter']->value).
  ($child['filter']->quote_value && ($child['filter']->comparison < 4)?"'":"");



  switch($child['filter']->comparison){
    case FILTER_COMPARITIVE_EQUAL_TO:
      $clause.=" = $value";
      break;
    case FILTER_COMPARITIVE_GREATER_THAN:
      $clause.=" > $value";
      break;
    case FILTER_COMPARITIVE_EQ_GREATER_THAN:
      $clause.=" >= $value";
      break;
    case FILTER_COMPARITIVE_LESS_THAN:
      $clause.=" < $value";
      break;
    case FILTER_COMPARITIVE_LESS_THAN_EQ_TO:
      $clause.=" <= $value";
      break;
    case FILTER_COMPARITIVE_NOT_EQUAL_TO:
      $clause.=" <> $value";
      break;
    case FILTER_COMPARITIVE_LIKE:
      $clause.=" LIKE '%$value%'";
      break;
    case FILTER_COMPARITIVE_LIKE_BEFORE:
      $clause.=" LIKE '%$value'";
      break;
    case FILTER_COMPARITIVE_LIKE_AFTER:
      $clause.=" LIKE '$value%'";
      break;
    case FILTER_COMPARITIVE_IS:
      $clause.=" IS $value";
      break;
    case FILTER_COMPARITIVE_IS_NOT:
      $clause.=" IS NOT $value";
      break;
    default:
      $clause .=" $value";
      break;
  }

  if($child['filter']->value=='' && $child['filter']->skip_on_null){
    $clause='';
    $ending_parenthesis=false;
  }

  if(count($child['children'])>0){
    foreach($child['children'] as $grand_child){
      $clause.=buildClause($grand_child);
    }
  }
  if($ending_parenthesis!==false){
    $clause.=" )";
  }
  return $clause;
}
function calcPassphrase($string) {
  return strtoupper(str_replace("=","",md5($string)));
}
