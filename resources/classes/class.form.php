<?php
define("DT_NON_NULL","/.+/");
define("DT_INTEGER","/[0-9]*/");

define("FORM_RETURN_VALUE" ,0);
define("FORM_ECHO_VALUE"   ,1);
/**
 * Creates form, links, and images elements in xhtml standards
 * @version 1.5.1
 * @since  v1.5.1 2012-02-08 Checkbox "checked" attribute makes standards-compliant attribute.
 * @since  v1.5   2010-04-27 Adds type_select to iterate through an array from type table
 * @since  v1.4   2009-12-28 Truncate select box options to 50 characters.
 * @since  v1.3   2009-08-17 Initial Tracked Version
 * @author Brian Castellanos
 * @example To create Input Box with name "foo" value "bar; Form::textbox("foo","bar");
 */
class Form{
  public $version = "1.5.1";
  /**
     * Controls whether the text should be rendered to the browser or sent as a string
     *
     * @var integer
     */
  public $return_mode;

  /**
     * Maximum number of characters to display in the options list
     *
     * @var unknown_type
     */
  public $max_display = 30;

  public $type_select_css = array(
	"item1" => "background-color:white;color:black;",
	"item2" => "background-color:white;color:black;",
	"item3" => "background-color:white;color:black;",
	"item4" => "background-color:white;color:black;",
	"item5" => "background-color:white;color:black;",
	"item6" => "background-color:white;color:black;",

	"head1" => "background-color:black;color:white;",
	"head2" => "background-color:#333333;color:white;",
	"head3" => "background-color:#555555;color:white;",
	"head4" => "background-color:#777777;color:white;",
	"head5" => "background-color:#999999;color:white;",
	"head6" => "background-color:#BBBBBB;color:white;",
  );

  /**
	 * Instantiate a new Form object
	 * @param $return_mode int
	 * @return object
	 */
  public function __construct($return_mode=FORM_RETURN_VALUE){
    $this->return_mode=$return_mode;
  }
  /**
	 * Create an xhtml compliant button
	 *
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @return mixed
	 */
  public function button($name,$value,$attributes=null){
    if($attributes['class'] == ''){
      $attributes["class"] = "button";
    }
    $strAttributes=$this->concat_attributes($attributes);
    $btn = '<input type="button" name="'.hentities($name).'" value="'.hentities($value).'" '.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $btn;
      unset($btn);
    }else{
      return $btn;
    }
  }
  /**
	 * Create an xhtml compliant checkbox
	 *
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @param boolean $checked
	 * @return mixed
	 */
  public function checkbox($name,$value=null,$attributes=null,$checked=false){
    $strAttributes = $this->concat_attributes($attributes);
    $strChecked = ($checked)?' checked="checked"':'';
    $strCkBx='<input type="checkbox" name="'.hentities($name).'" value="'.hentities($value).'"'.$strAttributes.$strChecked.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $strCkBx;
    }
    else{
      return $strCkBx;
    }
  }
  /**
	 * Create an xhtml compliant hiddenbox
	 *
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @return mixed
	 */
  public function hiddenbox($name,$value=null,$attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $txtarea='<input type="hidden" name="'.hentities($name).'"  value="'.hentities($value).'" '.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $txtarea;
    }
    else{
      return $txtarea;
    }
  }
  /**
	 * Create an xhtml compliant file input box
	 *
	 * @param string $name
	 * @param string $value
	 * @param integer $MAX_FILE_SIZE
	 * @param array $attributes
	 * @return mixed
	 */
  public function file($name,$value=null,$MAX_FILE_SIZE='10000',$attributes=null){
    if($attributes['class'] == ''){
      $attributes["class"] = "textbox";
    }
    $strAttributes=$this->concat_attributes($attributes);
    $txtarea = '<input type="hidden" name="MAX_FILE_SIZE" value="'.$MAX_FILE_SIZE.'" />
					<input type="file" name="'.$name.'" value="'.$value.'" '.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $txtarea;
    }
    else{
      return $txtarea;
    }
  }
  /**
	 * Create an xhtml compliant password box
	 *
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @return mixed
	 */
  public function password($name,$value=null,$attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $txtarea = '<input type="password" name="'.hentities($name).'" value="'.hentities($value).'" '.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $txtarea;
    }
    else{
      return $txtarea;
    }
  }
  /**
	 * Create an xhtml compliant radio button
	 *
	 * @param string $name
	 * @param string $default
	 * @param array $options
	 * @param array $attributes
	 * @param string $orient
	 * @return mixed
	 */
  public function radio($name,$default,$options,$attributes=null){
    if(!is_array($options))$options=array();
    foreach($options as $val=>$dsp){
      $cbo.='<input type="radio" name="'.hentities($name).'" value="'.hentities($val).'" '.(strtolower($default)==strtolower($val)?' checked="checked"':null)." ".$this->concat_attributes($attributes)." /> $dsp";
    }
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $cbo;
    }
    else{
      return $cbo;
    }
  }
  /**
	 * Create an xhtml compliant select box
	 *
	 * @param string $name
	 * @param string $default
	 * @param array $options
	 * @param array $attributes
	 * @return mixed
	 */
  public function select($name,$default,$options,$attributes=null){
    if(!is_array($options))$options=array();
    if(!is_array($attributes))$attributes=array();

    if(array_key_exists("nodojo",$attributes)){
      unset($attributes['dojoType']);
      unset($attributes['autoComplete']);
      unset($attributes['nodojo']);
    }

    if(array_key_exists("encode",$attributes)){
      unset($attributes['encode']);
      $encode_value = false;
    }else{
      $encode_value = true;
    }

    $attributes['onchange'] = 'is_dirty=true;'.$attributes['onchange'];
    $strAttributes=$this->concat_attributes($attributes);
    $cbo = '<select name="'.hentities($name).'" '.$strAttributes.'>';
    $cbo.= $this->create_options($options,$default,$encode_value);
    $cbo.='</select>';
    if($this->return_mode==="echo" || $this->return_mode===FORM_ECHO_VALUE){
      echo $cbo;
    }
    else{
      return $cbo;
    }
  }

  /**
   * Creates a drop down list with highlighted rows for display
   *
   * @param unknown_type $name
   * @param unknown_type $default
   * @param unknown_type $options
   * @param unknown_type $attributes
   * @return unknown
   */
  public function type_select($name,$default,$options,$attributes = null){
    if(!is_array($options))$options=array();
    if(!is_array($attributes))$attributes=array();

    if(array_key_exists("nodojo",$attributes)){
      unset($attributes['dojoType']);
      unset($attributes['autoComplete']);
      unset($attributes['nodojo']);
    }

    if(array_key_exists("encode",$attributes)){
      unset($attributes['encode']);
      $encode_value = false;
    }else{
      $encode_value = true;
    }

    $attributes['onchange'] = 'is_dirty=true;'.$attributes['onchange'];
    $strAttributes=$this->concat_attributes($attributes);
    $cbo = '<select name="'.hentities($name).'" '.$strAttributes.'>';
    foreach($options as $guid=>$array){
      if(!is_array($array)){
        $cbo.='<option value="'.hentities($guid).'"'.($default==$guid?' selected="selected"':null).'>'.hentities((strlen($array)>$this->max_display ? substr($array,0,$this->max_display-3)."..." : $array)).'</option>';
      }else{
        $cbo.='<option style="'.$this->type_select_css[$array['class']].'" value="'.hentities($array['guid']).'"'.($default==$array['guid']?' selected="selected"':null).'>'.str_repeat('&nbsp;',($array['level']-1)*4).hentities((strlen($array['name'])>$this->max_display ? substr($array['name'],0,$this->max_display-3)."..." : $array['name'])).'</option>';
      }
    }
    $cbo.='</select>';
    if($this->return_mode==="echo" || $this->return_mode===FORM_ECHO_VALUE){
      echo $cbo;
    }
    else{
      return $cbo;
    }
  }


  /**
     * Create options or option groups -
     * To create options, set the option value as $key and display text as $value;
     * to create groups, set the key as the display text, and the value as an array containing $key=>$value pairs.
     *
     * @param array $options
     */
  function create_options($options,$default,$encode){
    $text = "";
    foreach($options as $val=>$dsp){
      if(is_array($dsp)){
        $text .="<optgroup label=\"".hentities($val)."\">";
        $text .= $this->create_options($dsp,$default,$encode);
        $text .="</optgroup>";
      }
      else{
        $text.='<option value="'.hentities($val,$encode).'"'.($default==$val?' selected="selected"':null).'>'.hentities((strlen($dsp)>$this->max_display ? substr($dsp,0,$this->max_display-3)."..." : $dsp) ,$encode).'</option>';
      }
    }
    return $text;
  }
  /**
	 * Create an xhtml compliant submit button
	 *
	 * @param $name string
	 * @param $value string
	 * @param $attributes array
	 * @return mixed
	 */
  public function submit($name=null,$value=null,$attributes=null){
    if($attributes['class'] == ''){
      $attributes["class"] = "button";
    }
    if($value!='' && $value!=null){$value='value="'.hentities($value).'" ';}
    if($name!='' && $name!=null){$name=' name="'.hentities($name).'" ';}
    $strAttributes=$this->concat_attributes($attributes);
    $btn='<input type="submit" '.$name.$value.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $btn;
    }
    else{
      return $btn;
    }
  }
  /**
	 * Create an xhtml compliant link tag
	 *
	 * @param string $href
	 * @param string $display
	 * @param array $attributes
	 * @return mixed
	 */
  public function a($href,$display,$attributes=null){
    return $this->link($href,$display,$attributes=null);
  }
  /**
	 * Create an xhtml compliant link tag
	 *
	 * @param string $href
	 * @param string $display
	 * @param array $attributes
	 * @return mixed
	 */
  public function link($href,$display,$attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $link='<a href="'.hentities($href).'" '.$strAttributes.'>'.(substr($display,0,4)=='<img'?$display:$display)."</a>";
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $link;
    }else{
      return $link;
    }
  }
  /**
	 * Create an xhtml compliant anchor tag
	 *
	 * @param string $href
	 * @param string $display
	 * @param array $attributes
	 * @return mixed
	 */
  public function anchor($name,$display,$attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $anchor='<a name="'.hentities($name).'" '.$strAttributes.'>'.(substr($display,0,4)=='<img'?$display:$display)."</a>";
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $anchor;
    }else{
      return $anchor;
    }
  }
  /**
     * Create an xhtml compliant image tag
     *
     * @param string $src
     * @param string $alt
     * @param string $width
     * @param string $height
     * @param array $attributes
     * @return mixed
     */
  public function image($src,$alt=null,$width=null,$height=null,$attributes=null){
    $strAttributes=$this->concat_attributes($attributes);
    $img='<img src="'.hentities($src).'" alt="'.hentities($alt).'" '.($width!=''?' width="'.hentities($width).'"':"").($height!=''?' height="'.hentities($height).'" ':"").$strAttributes.'/>';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $img;
    }else{
      return $img;
    }
  }
  /**
     * Create an xhtml compliant text area
     *
     * @param $name string
     * @param $value string
     * @param $attributes array
     * @return mixed
     **/
  public function textarea($name,$value=null,$attributes=null){
    if($attributes['class'] == ''){
      $attributes["class"] = "textbox";
    }
    if($attributes['cols']==''){$attributes['cols']=30;}
    if($attributes['rows']==''){$attributes['rows']=10;}

    $strAttributes=$this->concat_attributes($attributes);
    $txtarea = '<textarea name="'.hentities($name).'" '.$strAttributes.'>'.hentities($value).'</textarea>';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      echo $txtarea;
    }else{
      return $txtarea;
    }
  }
  /**
     * Create an xhtml textbox
     *
     * @param $name string
     * @param $value string
     * @param $attributes array
     * @return mixed
     */
  public function text($name,$value=null,$attributes=null){
    return 	$this->textbox($name,$value,$attributes);
  }
  /**
     * Create an xhtml textbox
     *
     * @param $name string
     * @param $value string
     * @param $attributes array
     * @param $label_text string
     * @return mixed
     */
  public function textbox($name,$value=null,$attributes=null,$label_text=null){
    if($attributes['class'] == ''){
      $attributes["class"] = "textbox";
    }
    if ($label_text==null) {
      $keyup = '';
    } else {
      if (($value==null) || ($value=='')) {
        $value = $label_text;
      }
      $keyup =  ' onkeydown="if (this.value==\''.$label_text.'\'){this.value=\'\';}"';
      $keyup .= ' onkeyup="if (this.value==\'\'){this.value=\''.$label_text.'\';}"';
    }
    $strAttributes=$this->concat_attributes($attributes);
    $txtarea='<input type="text" name="'.hentities($name).'" value="'.hentities($value).'"'.$keyup.' '.$strAttributes.' />';
    if($this->return_mode==='echo' || $this->return_mode===FORM_ECHO_VALUE){
      var_dump($this->return_mode === 'echo');
      echo $txtarea;
    }else{
      return $txtarea;
    }
  }
  /**
     * Implode an array containing name-value pairs to generate a string of attirubtes
     *
     * @param array $attributes
     * @return mixed
     */
  public function concat_attributes($attributes){
    if(!is_array($attributes)){
      return false;
    }
    foreach($attributes as $att=>$value){
      $strAttributes .=hentities($att).'="'.hentities($value).'" ';
    }
    return $strAttributes;
  }
}
/**
 * Will properly display hentities by first decoding entities, then re-encoding them.
 * May have issues if the $string is not encoded already(meaning a a literal &amp; will need to be entered as &amp;amp;,
 * but It's no different than a person typing &amp; into a search box and having the same value echoed'.
 * @param string $string
 * @return string
 */
function hentities($string){
  return htmlentities($string,ENT_QUOTES);
}