 	<?php
 	/**
 	 * @package Menu
 	 * @desc    Generates CSS to create navigation menu for websites.
 	 * @author  Brian Castellanos
 	 * @version 2.4.1
 	 * @since   2011-11-08 v2.5			Added support for IOS and Android and small variable usage fix (JLC)
 	 * @since   2011-11-08 v2.4.1		Added a property to turn on display of menu dividers and a property to set the dividers class name (JLC)
 	 * @since   2011-09-08 v2.4			Added optional build method for accordion-style menus using jQuery (ALA)
 	 * @since   2010-06-10 v2.3			Added optional parameters for buildMenu method to specify an id attribute for the wrapper div and the menu ul.
 	 */
 	
 	class Menu{
 	  public $version							= "2.4.1";
 	  public $submenu_text;
 	  public $criteria;
 	  public $submenu_location		= "topbar";//sidebar or topbar
 	  public $add_spacers					= false;
 	  public $menu_dividers				= false;
 	  public $menu_divider_class;
 	  public $max_levels					= 0;

 	  /**
     * Create an array containing the structure of the all menu items. First level items separated by a blank element.
     *
     * @param int $menu_id
     * @return array
     */
 	  public function arrange_items($menu_id){
 	    global $db;
 	    $child = array();

 	    $sql="SELECT * FROM `menu` WHERE parent_id='$menu_id' AND status='active' ORDER BY display_order";
 	    $db->Query($sql,false);

 	    while($r=$db->fetch_assoc()){
 	      $child[$r['id']]["attributes"] = $r;
 	      $c = $this->arrange_items($r['id']);

 	      if(count($c)>0){
 	        $child[$r['id']] += $c;
 	        unset($c);
 	      }

 	      //If the menu item belongs to the first level,
 	      //then add a spacer element after it.
 	      if($menu_id==0){
 	        $child["S_".count($child)] = "spacer";
 	      }
 	    }
 	    $db->free_result();

 	    //Remove the last spacer element.
 	    if($menu_id==0){
 	      array_pop($child);
 	    }
 	    return $child;
 	  }

 	  function build_arrangement($array,$nested=false){
 	    if (is_array($array)) {
 	      //Reset the counters.
 	      $num_items = count($array);
 	      $item_num  = 0;

 	      foreach($array as $menu_id => $data){
 	        $item_num++;
 	        if($data == 'spacer' && $this->add_spacers == true){
 	          if($skip_spacer){
 	            //If the previous menu item has been set to not display, then
 	            //The spacer item for that menu item should be hidden as well.
 	            $skip_spacer=false;
 	          }
 	          else{
 	            echo "<li class='separator'></li>";
 	          }
 	        }
 	        else {
 	        	if (is_array($data["attributes"])) {
	 	        	if (($data["attributes"]["parent_id"]==0) && ($this->menu_dividers===true)) {
	 	        		if (isset($first_divider_skipped)) {
				 	        echo "<li class='{$this->menu_divider_class}'></li>";
	 	        		} else {
	 	        			$first_divider_skipped = true;
	 	        		}
	 	        	}
 	        	}
 	          $item = $data['attributes'];
 	          /**
 	           * Remove the border from the last item.
 	           */
 	          if ($item_num==$num_items) {
 	            $li_border = ' style="border-right:0px;"';
 	            $a_border = 'border-right:0px;';
 	          }
 	          else {
 	            $li_border = '';
 	          }

 	          if ($item['status']=='active') {
 	            if($item['visibility']=='always' || $item['visibility']==''){
 	              $display_menu_item = true;
 	            }
 	            else if($item['visibility']=='logged out' && $_SESSION['UserLoggedIn']!=true){
 	              $display_menu_item = true;
 	            }
 	            else if($item['visibility']=='logged in' && $_SESSION['UserLoggedIn']==true){
 	              $display_menu_item = true;
 	            }
 	            else{
 	              $display_menu_item = false;
 	            }

 	            if ($display_menu_item==false) {
 	              $skip_spacer = true;
 	              continue;
 	            }
 	            else{
 	              unset($data['attributes']);

 	              $parent_id    = $item['parent_id'];
 	              $menu_id      = $item['id'];
 	              $has_children = count($data) > 0 ? true : false;
 	              $menu_href    = $item['clickable']=='yes' ? " href='".($item['url_type']=='content' ? "http://{$_SERVER['HTTP_HOST']}/": "")."{$item['url']}'" : false;
 	              $menu_target  = $menu_href!==false ? ($item['new_window']=='yes'?" target='_blank'":"") : false;
 	              $drop_down    = ($has_children && $parent_id==0 ? " rel='nav_sub_menu_{$menu_id}'" : "");
 	              $hide_icon    = $item['icon_hide']=='yes' && $_SESSION['UserLoggedIn'] ? true : false;
 	              $offset       = $parent_id == 0 ? "margin-top:5px;margin-left:10px;" :"";
 	              $icon_float   = $item['icon_placement']=='right' ? "vertical-align:middle;align:right;right:0;position:absolute;{$offset}": "";
 	              $icon_txt     = "<img src='/img/sm_icon/icon_padlock.gif' style='width:30;height:30;border:0px;{$icon_float}' alt='Secure'  />";
 	              $lft_icon     = $item['icon_placement']=='left'  && $hide_icon==false ? $icon_txt."&nbsp;" : null;
 	              $rgt_icon     = $item['icon_placement']=='right' && $hide_icon==false ? "&nbsp;&nbsp;&nbsp;".$icon_txt : null;
 	              $display_text = $lft_icon."{$item['title']}".$rgt_icon;

 	              $text         = "<li".$li_border.">";
 	              $text        .= "<a{$menu_href}{$menu_target}{$drop_down} style='position:relative;".$a_border."'>{$display_text}</a>";

 	              if ($parent_id==0) {
 	                echo $text;
 	              }
 	              else{
 	                $this->submenu_text .= $text;
 	              }

 	              if ($has_children) {
 	                $this->submenu_text.="<ul".($nested==false ?" id='nav_sub_menu_{$menu_id}' class='ddsubmenustyle'":"").">\n";
 	                $this->build_arrangement($data, true);
 	                $this->submenu_text.="</ul>";
 	              }
 	              $text = "</li>";
 	              if($parent_id==0){
 	                echo $text;
 	              }
 	              else{
 	                $this->submenu_text.=$text;
 	              }
 	            }
 	          }
 	        }
 	      }
 	    }
 	  }

 	  function build_accordion($array, $level = 0){
      $output = "";

 	    if (is_array($array)) {
 	      //Reset the counters.
 	      $num_items = count($array);
 	      $item_num  = 0;

 	      foreach($array as $menu_id => $data){
 	        $item_num++;

 	        if($data == 'spacer' && $this->add_spacers == true){
 	          if($skip_spacer){
 	            //If the previous menu item has been set to not display, then
 	            //The spacer item for that menu item should be hidden as well.
 	            $skip_spacer = false;
 	          }
 	          else{
 	            $output .= "<div class='acc-separator acc-separator-{$level}'></div>";
 	          }
 	        }
 	        else {
 	          $item = $data['attributes'];

 	          if ($item['status'] == 'active') {
 	            if($item['visibility'] == 'always' || $item['visibility'] == ''){
 	              $display_menu_item = true;
 	            }
 	            else if($item['visibility'] == 'logged out' && $_SESSION['UserLoggedIn'] != true){
 	              $display_menu_item = true;
 	            }
 	            else if($item['visibility'] == 'logged in' && $_SESSION['UserLoggedIn'] == true){
 	              $display_menu_item = true;
 	            }
 	            else{
 	              $display_menu_item = false;
 	            }

 	            if ($display_menu_item == false) {
 	              $skip_spacer = true;
 	              continue;
 	            }
 	            else{
 	              unset($data['attributes']);

 	              $parent_id    = $item['parent_id'];
 	              $has_children = count($data) > 0 ? true : false;
 	              $menu_href    = $item['clickable'] == 'yes' ? " href='".($item['url_type'] == 'content' ? "http://{$_SERVER['HTTP_HOST']}/": "")."{$item['url']}'" : false;
 	              $menu_target  = $menu_href !== false ? ($item['new_window'] == 'yes' ? " target='_blank'" : "") : false;
 	              $hide_icon    = $item['icon_hide'] == 'yes' && $_SESSION['UserLoggedIn'] ? true : false;

 	              $icon_float   = $item['icon_placement'] == 'right' ? "vertical-align:middle;float:right;" : "";

 	              $icon         = "<img src='/img/sm_icon/icon_padlock.gif' style='width:16;height:16;border:0px;{$icon_float}' alt='Secure'  />";

 	              $lft_icon     = $item['icon_placement'] == 'left'  && $hide_icon == false ? $icon."&nbsp;" : null;
 	              $rgt_icon     = $item['icon_placement'] == 'right' && $hide_icon == false ? $icon : null;

 	              $display_text = $lft_icon;

 	              if ($has_children) {
 	                $display_text .= "<a class='acc-header-link'>{$item['title']}</a>";
 	              }
 	              else {
 	                if ($item['url'] == "/") {
 	                  $link_url = "home";
 	                }
 	                else {
 	                  $link_url = str_replace("/", "", $item['url']);
 	                }

 	                $display_text .= "<a class='acc-content-link' id='acc-content-link-{$link_url}'{$menu_href}{$menu_target}>{$item['title']}</a>";
 	              }

 	              $display_text .= $rgt_icon;

 	              if ($has_children) {
 	                array_push($_SESSION["accordion-menu"]["ids"], $menu_id);

             	    if ($level > $this->max_levels) $this->max_levels = $level;

                  $output .= "<div class='acc-header acc-header-{$level}' rel='level-{$level}' id='acc-id-{$menu_id}'>{$display_text}</div>";
                  $output .= "<div class='acc-content acc-content-{$level}'>";
 	                $output .= $this->build_accordion($data, ($level + 1));
 	                $output .= "</div>";

 	                array_pop($_SESSION["accordion-menu"]["ids"]);
 	              }
 	              else {
 	                if ($item['url'] == "/") {
 	                  $link_url = "home";
 	                }
 	                else {
 	                  $link_url = str_replace("/", "", $item['url']);
 	                }

                  if ($_SESSION["accordion-menu"]["urls"] == "") {
                    $_SESSION["accordion-menu"]["urls"] = $link_url.":";
                  }
                  else {
                    $_SESSION["accordion-menu"]["urls"] .= "|".$link_url.":";
                  }

                  if (count($_SESSION["accordion-menu"]["ids"])) {
                    $_SESSION["accordion-menu"]["urls"] .= implode(",", $_SESSION["accordion-menu"]["ids"]);
                  }

 	                $output .= "<div class='acc-content acc-link-{$level}'>"; // id='acc-id-{$menu_id}'
 	                $output .= $display_text;
 	                $output .= "</div>";
 	              }
 	            }
 	          }
 	        }
 	      }
 	    }

      return $output;
 	  }

 	  /**
 	     * Create the menu's html
 	     *
 	     * @param string $nav_div_id Optional. Specifies an html id attribute for the menu wrapper div
 	     * @param string $nav_a_id Optional. Specifies an html id attribute for the menu ul
 	     */
 	  function buildMenu( $nav_div_id = "main_navigation", $nav_ul_id = "nav_top" ) {
 	    echo "<div id='$nav_div_id'><ul id='$nav_ul_id'>";    /* class="mattblackmenu" */
 	    $this->build_arrangement($this->arrange_items(0));
 	    echo '</ul></div>';
 	    echo $this->submenu_text;
 	    echo '<script type="text/javascript">ddlevelsmenu.setup("'.$nav_div_id.'", "'.$this->submenu_location.'")</script>';
 	    //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
 	  }

 	  /**
 	   * Create and return accordion menu html
 	   * Based on original recipe: http://www.stemkoski.com/stupid-simple-jquery-accordion-menu/
 	   *
 	   * @param string $nav_div_id
 	   * @param integer $width - maximum width of accordion menu (in pixels)
 	   * @param integer $indent - indentation amount for each indented level (in pixels)
 	   * @return string
 	   */
 	  function buildAccordionMenu($nav_div_id = "acc-wrapper", $width = 200, $indent = 0) {
      session_start();

      $_SESSION["accordion-menu"]["ids"] = array();

      unset($_SESSION["accordion-menu"]["urls"]);

 	    $menu_output = $this->build_accordion($this->arrange_items(0));

      unset($_SESSION["accordion-menu"]["ids"]);

 	    echo "<style>";

 	    $level_width = $width;

 	    for ($i=0; $i <= $this->max_levels; $i++) {
 	      echo ".acc-header-{$i} {
              	width: {$level_width}px;
              	float: left;
              	cursor: default;
              }

              .acc-link-{$i} {
              	width: {$level_width}px;
              	float: left;
              	cursor: default;
              }

              .acc-separator-{$i} {
                width: {$level_width}px;
                float: left;
              }

              ";

 	      $level_width = $level_width - $indent;

 	      echo ".acc-content-{$i} {
 	            	width: {$level_width}px;
              	float: left;
         	      display: none;
         	      padding-left: {$indent}px;
              }

              ";
 	    }

      echo ".acc-link-{$i} {
          	width: {$level_width}px;
          	float: left;
          	cursor: default;
          }

        </style>";

      echo "<div id='{$nav_div_id}'>";

      echo $menu_output;

 	    echo "</div>";

      if ($_REQUEST["url"] == "/") {
        $selected_url = "home";
      }
      else {
        $selected_url = str_replace("/", "", $_REQUEST["url"]);
      }

 	    echo "<script type=\"text/javascript\">
 	            // Leave the following for future debug purposes
 	            //alert('{$_SESSION["accordion-menu"]["urls"]}');
 	            //alert('{$selected_url}');

              jQuery.noConflict();

              var last_menu_id = [];

              var selected_url = '{$selected_url}';

              var menus_to_open = '{$_SESSION["accordion-menu"]["urls"]}';
              menus_to_open = menus_to_open.split('|');

              var num_levels = {$this->max_levels};

              jQuery(document).ready(function() {
                // Set click action for accordion div levels 0-n
                for (var n=0; n <= num_levels; n++) {
                	jQuery('div.acc-header-' + n).click(function() {
                		var rel = jQuery(this).attr('rel');
                		var parts = rel.split('-');
                		var level = parts[1];

                		var menu_id = jQuery(this).attr('id');
                		var parts = menu_id.split('-');
                		var menu_id = parts[2];

                		if (menu_id != last_menu_id[level]) {
                 		  for (var n=level; n <= num_levels; n++) {
                 		    jQuery('div.acc-content-' + n).slideUp('normal');
                 		  }

                		  if (jQuery(this).next().is(':visible')) {
                		    jQuery(this).next().slideUp('normal');
                		  }
                		  else {
                        var ua = jQuery.browser;

                		    if (ua.msie && ua.version.slice(0,3) == '7.0') {
                		      // ie7 compatibility mode only
                          jQuery(this).next().show('normal', function() {
                            this.style.removeAttribute('filter');
                          });
                        }
                        else {
                		      jQuery(this).next().slideDown('normal');
                        }
                		  }
                		}
                		else {
                		  if (jQuery(this).next().is(':visible')) {
                  		  for (var n=level; n <= num_levels; n++) {
                  		    jQuery('div.acc-content-' + n).slideUp('normal');
                  		  }
                		  }
                		  else {
                		    jQuery(this).next().slideToggle('normal');
                		  }
                		}

               		  last_menu_id[level] = menu_id;
                	});

                	jQuery(this).next().hide();
                }

                // Show accordion divs based on selected page loaded (url:id,id,id)
                for (var m=0; m < menus_to_open.length; m++) {
                  var check_menu_to_open = menus_to_open[m];
                  check_menu_to_open = check_menu_to_open.split(':');

                  if (check_menu_to_open[0] == selected_url) {
                    jQuery('#acc-content-link-' + selected_url).attr('className', 'acc-content-link acc-content-selected-link');

                    var ids_to_open = check_menu_to_open[1].split(',');

                    for (var n=0; n < ids_to_open.length; n++) {
                      jQuery('#acc-id-' + ids_to_open[n]).next().show();
                    }
                  }
                }
              });
 	          </script>";
 	  }
 	}