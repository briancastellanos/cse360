<?php
class Address {
  private $addressOne;
  private $addressTwo;
  private $city;
  private $state;
  private $zip;
  private $id;
  private $status;
  private $personId;
  
  public function __construct($person_id=null){
    if($person_id!=""){
      $db = DatabaseFactory::getDb();
      $data = $db->query_fetch_assoc("SELECT * FROM address WHERE person_id='$person_id'",false);
      $this->addressOne  = $data['address_one'];
      $this->addressTwo  = $data['address_two'];
      $this->city        = $data['city'];
      $this->state       = $data['state'];
      $this->zip         = $data['zip'];
      $this->id          = $data['id'];
      $this->personId    = $data['person_id'];
    }
  }
  
  
  public function save() {
    $db=DatabaseFactory::getDb();
    $data=array(
      "person_id"=>$this->personId,
      "state"=>$this->state,
      "address_one"=>$this->addressOne,
      "address_two"=>$this->addressTwo,
      "city"=>$this->city,
      "zip"=>$this->zip,
      "status"=>"active"
    );
    
    if ($this->id == "") {
      $data['created_on']="DB_TIMESTAMP";
      $method="insert";
    } else {
      $data['person_id']=$this->personId;
      $method='update';
    }
    
    $tmp = $db->mk_sql(addslashes_array($data), "address", $method, "person_id");
    if($method=="insert"){
      $this->id = $tmp;
    }
    
    return $this->id;
  }
  public function __get($what) {
    return $this->$what;
  }
  public function __set($what, $value) {
    $this->$what=$value;
  }
}