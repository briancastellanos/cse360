<?php

/**
 * @author Brian Castellanos
 * @version 2.2
 * @since 2011-11-04 v2.2 - Adds rendering of template tokens
 * @since 2011-06-02 v2.1 - Adds back the function for verifying page security and redirects if necessary.
 * @since 2010-09-30 v2.0 - heuristic fix to evaluating php code.
 */

if(is_array($pg)){

  /**
   * Validate the user is logged in.
   */
  if($pg['secure']=='yes' && $_SESSION['UserLoggedIn']!==true){
    header("location: {$pg['redirect_url']}");
  }

  /**
     * Check to see if the page is past the permalink date or 
     * if the page location is not the permalink location.
     */

  if($pg['permalink_date']!='') {
    $permalink_time = strtotime($pg['permalink_date']);
    $todays_time    = mktime(0,0,0,date('m'),date('d'),date('Y'));
    $permalink      = getPermalink($pg);

    mktime($pg['permalink']);
    if( ($permalink_time < $todays_time && $_SERVER['REQUEST_URI']!=$permalink)){ // || $_SERVER['REQUEST_URI']!=$permalink
      header("location: $permalink");
    }
  }
  else  {
    if($_REQUEST['x_permalink']=='yes' && $_SERVER['REQUEST_URI']!=$permalink){
      header("location: $permalink");
    }
  }

  $page_id=$pg['page_id'];
  $page->Page(str_replace("tpl.","",str_replace(".php","",$pg['page_template'])),TPL_ROOT);
  $db->Query("SELECT * FROM content_page_property WHERE page_id='$page_id'");
  while($r=$db->fetch_assoc()){
    $search  = array();
    $replace = array();
    $content = $r['content'];
    $page->content($content,$r['token_name']);
  }

  $db->Query("SELECT * FROM content_template_property WHERE template_name='{$pg['page_template']}'");
  while($r=$db->fetch_assoc()){
    $search  = array();
    $replace = array();
    $content = $r['content'];
    $page->content($content,$r['token_name']);
  }


  $db->Query("(SELECT content,token_name FROM content_current WHERE page_id='$page_id') UNION (SELECT content,token_name FROM content_template_current WHERE template_name='{$pg['page_template']}')");
  #Fetch all of the records for content.

  while($tpl_mgr_content_record=$db->fetch_assoc()){
    $search=array();
    $replace=array();
    $content=$tpl_mgr_content_record['content'];
    $content=str_replace("<p>~~~</p>","~~~",$content);
    $snippets=null;

    #Now check the content block to see if anything has to be parsed.
    preg_match_all('/~{3}([^~]*)~{3}/',$content,$snippets);

    #There is stuff to parse
    if(is_array($snippets)){
      $i=0;
      foreach($snippets[0] as $snippet){
        $content=str_replace($snippet,'{'.$i.'}',$content);
        $search[]='{'.$i.'}';
        $i++;
        #preg_match_all("/\? >(.*)(\? >(php)?)?/i",$snippet,$php_matches);
        preg_match_all("/\?>(.*)/i",$snippet,$php_matches);

        $snippet=str_replace("~~~","",strip_tags($snippet));
        $snippet= html_entity_decode(str_replace("&nbsp;"," ",$snippet),ENT_QUOTES);
        ob_start();
        try{
          eval($snippet);
        }catch(Exception $e){
          echo $e->getMessage();
        }
        $replace[]=ob_get_clean();
      }
      $content=str_replace($search,$replace,$content);
    }
    $page->content($content,$tpl_mgr_content_record['token_name']);
  }
  $page->display_page();
}

function getPermalink($page_data){
  if(file_exists(MOD_ROOT."liveeditor".DIR_TKN."preferences.inc.php") && $page_data['issue_date']!=''){
    include(MOD_ROOT."liveeditor".DIR_TKN."preferences.inc.php");
    $date_segment = strtolower(date($date_format,strtotime($page_data['issue_date'])))."/";
  }else{
    $date_segment = "";
  }


  $clean_url = preg_replace("[^a-zA-Z0-9\/\-]","",str_replace(" ","-",strtolower($page_data['page_title'])));
  $permalink = "/archive/{$date_segment}{$clean_url}-{$page_data['page_id']}";

  return $permalink;
}