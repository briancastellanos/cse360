<?php
@include("vars.inc.php");
require_once(dirname(__FILE__)."/resources/init.php");
$page->content("<link rel='stylesheet' type='text/css' href='/style/datagrid.css' />","page_head");
$page->content("<script type='text/javascript' src='/javascript/datagrid.js'></script>","page_head");
$page->Page("admin",TPL_ROOT);

if(!in_array($_REQUEST['mod'],array('login','logout')) && $_SESSION['UserDestination']==''){
  $_SESSION['UserDestination']=$_SERVER['QUERY_STRING'];
}

ob_start();
$module=($_REQUEST['mod']!='')?$_REQUEST['mod']:"home";
if($_SESSION['LoggedIn']){
  $module=($_REQUEST['mod']!='')?$_REQUEST['mod']:"home";
}
elseif(in_array($_REQUEST['mod'],array("login","forgotpw"))){
  $module=$_REQUEST['mod'];
}
else{
  header("location: /index.php?mod=login");
  exit;
}

$tab    = trim(strtolower($_REQUEST['tab']));
$button = trim(strtolower($_REQUEST['button']));
$action = trim(strtolower($_REQUEST['action']));

if(check_security_key($module)==false){
  notify("You don't have permission to use that module",NOTIFY_DISPLAY_ERROR);
  $module="home";
}else{
  if(check_security_key($action)==false){
    notify("You don't have permission to perform that action",NOTIFY_DISPLAY_ERROR);
    $action="";
  }
}

define("TAB",$tab);
define("MODULE",$module);
define("BUTTON",$button);

$CFG['mod']=$module;
$CFG['tab']=$tab;
$CFG['button']=$button;

$db->datagrid_table_border=false;
$db->datagrid_table_width='100%';

$path=DOC_ROOT."modules/".MODULE.DIR_TKN."index.php";
if(!file_exists($path)){
  $path=DOC_ROOT."modules/home/index.php";
  $module="home";
}
include("menu.inc.php");
$base_path=DOC_ROOT."modules/".MODULE.DIR_TKN;
if(is_readable($base_path."tabs.inc.php")){
  include($base_path."tabs.inc.php");

}
if(is_readable($base_path."buttons.inc.php")){
  include($base_path."buttons.inc.php");
}
include_once($path);
include("navigation.inc.php");

$page->content($title,"page_section_title");
$page->content(ob_get_clean(),"page_content");
$page->content("../","page_style_path");
$page->content("$CLIENT_NAME Administration","header_message");
$page->content("$CLIENT_NAME","client_name");
$page->content("<img src='$LOGO_URL' alt='logo' />","logo");

if($_SESSION['AdminLoggedIn']){
  $page->content("Welcome, {$_SESSION['AdminFirstName']}","welcome_message");
}
if(file_exists($base_path."info.plist")){
  include $base_path."info.plist";
  $page->content("<span style='color:#fff;font-size:11px'>v".$PLIST['version'].($PLIST['build']!=''?" (build {$PLIST['build']})</span>":""),"page_content");
}


$page->display_page();

function check_security_key($key_name){
 return true;
}