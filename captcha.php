<?php
/**
 * @version 1.0
 * @since   2009-07-10
 * @author  Brian Castellanos
 */
include("resources/classes/class.captcha.php");

$captcha = new Captcha();
$captcha->create_captcha_image();
