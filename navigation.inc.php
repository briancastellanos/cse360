<?php 
if(is_array($menu_tabs)){
  ksort($menu_tabs);
  foreach($menu_tabs as $menu_tab_item){
    if ( ($CFG['tab']!="" && $CFG['tab']==$menu_tab_item["tab_href"]) || ($CFG['tab']!="" && $CFG['tab'] == $menu_tab_item["tab_name"]) ) {
      $tabClass = " class='active_tab' ";
    }else{
      $tabClass="";
    }
    $page->content("<li{$tabClass}><a href='?mod=".MODULE."&amp;tab={$menu_tab_item['tab_href']}'>{$menu_tab_item['text']}</a></li>","page_tabs");
  }
}

if(is_array($menu_buttons)){
  if(key_exists($CFG['tab'],$menu_buttons)){
    $menu_buttons=$menu_buttons[$CFG['tab']];
  }

  ksort($menu_buttons);
  foreach($menu_buttons as $key=>$menu_button_item){
    if(is_numeric($key)){
      if ( ($CFG['button']!="" && $CFG['button']==$menu_button_item["button_href"]) || ($CFG['button']!="" && $CFG['button'] == $menu_button_item["button_name"]) ) {
        $buttonClass = " class='active' ";
      }else{
        $buttonClass="";
      }
    }
    $page->content("<li{$buttonClass}><a href='".(substr($menu_button_item["button_href"],0,11)=="javascript:"?"":"?mod=".MODULE."&amp;tab={$CFG['tab']}&amp;button=").$menu_button_item['button_href']."'>{$menu_button_item['text']}</a></li>","page_buttons");
  }
}