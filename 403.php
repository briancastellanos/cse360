<?php
include_once("./resources/init.php");
$page->Page("blank",TPL_ROOT);
$page->content("
<div style='margin:0 auto;width:800px;height:100%;padding:20px;border:1px solid grey; background-color:#ececec;color:grey;'>
<span style='font-size:20px;'>Sorry, This Page Cannot Be Displayed</span><br /><br /><br />
<div style='padding:10px;background-color:white;'>
<span style='font-size:16px;font-weight;bold;'>
The page you are trying to view is protected. You must Log In to view this page.
</span>
<br /><br />
<hr />
<span style='font-weight:bold'>Please try one of the following:</span>
<ul>
    <li>If you have registered a username and password, please <a style='color:#336666'href='/user'>log in</a>.</li>
    <li>If you have not registered, <a href='/user' style='color:#336666'>Complete Registration</a>.</li>
</ul>
<br />
<span style='font-weight:bold'>What next?</span>
<ul>
    <li><a href='/members' style='color:#336666'>Go To Members Section</a></li>
    <li><a href='javascript:history.back()' style='color:#336666'>Go Back</a></li>
    <li><a href='/' style='color:#336666'>Return To Main Page</a></li>
</ul>


</div>
</div>
","page_content");
$page->content("403: Not Authorized","page_title",PAGE_CONTENT_INSERT_REPLACE);
$page->display_page();