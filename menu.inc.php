<?php
if ($_SESSION['LoggedIn']!==true) {
  $menu_items[] = array("text" => "Login", "menu_href" => "login");
}
else{
  include("menu_items.inc.php");
  foreach($menu_items as $key=>$row){
    $temp[$row['text']] = $row;
  }
  ksort($temp);
  $menu_items   = $temp;
  $menu_items[] = array("menu_href"=>"logout","text"=>"Logout");
}

$page->content("<ul>","page_navigation");
foreach ($menu_items as $item){
    $page->content("<li ".(MODULE==$item['menu_href'] || MODULE==$item['menu_name']?" class='active'":"")."><a ".(substr($item["menu_href"], 0, 1) == "/" || substr($item["menu_href"], 0, 5) == "http:"?"
		target='_blank' href=\"{$item['menu_href']}\"
		":"href='?mod={$item["menu_href"]}'").">{$item['text']}</a></li>","page_navigation");
}
$page->content("</ul>","page_navigation");
