<?php
include_once("./resources/init.php");
$page->Page("blank",TPL_ROOT);
$page->content("
<div style='margin:0 auto;width:800px;height:100%;padding:20px;border:1px solid grey; background-color:#ececec;color:grey;'>
<span style='font-size:20px;'>Sorry, we couldn't find what you were looking for</span><br /><br /><br />
<div style='padding:10px;background-color:white;'>
<span style='font-size:16px;font-weight;bold;'>
The page you are looking could not be found. It may have moved or is no longer available.
</span>
<br /><br />
<hr />
<span style='font-weight:bold'>Please try one of the following:</span>
<ul>
    <li>If you are accessing this page by typing in the address, check your spelling to make sure it is correct.</li>
    <li>If you are accessing this page by a saved bookmark or link, the page may have been moved or deleted.</li>
</ul>
<br />
<span style='font-weight:bold'>What next?</span>
<ul>
    <li><a href='javascript:history.back()' style='color:#336666'>Go Back</a></li>
    <li><a href='/' style='color:#336666'>Return To Main Page</a></li>
</ul>


</div>
</div>
","page_content");
$page->content("404: Not Found","page_title",PAGE_CONTENT_INSERT_REPLACE);
$page->display_page();